<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

App::after(function($request, $response)
{
    $response->headers->set('Cache-Control','nocache, no-store, max-age=0, must-revalidate');
    $response->headers->set('Pragma','no-cache');
    $response->headers->set('Expires','Fri, 01 Jan 1990 00:00:00 GMT');
});


#Start of Mobile Service
Route::post('resetPassword', 'AppController@resetEmail');
Route::post('getCity', 'AppController@getCityList');
Route::post('searchStore', 'AppController@searchStore');
Route::post('getStore', 'AppController@getStore');
Route::post('getProduct', 'AppController@getProduct');
Route::post('editPassword', 'AppController@editPassword');
Route::post('addUserAddress', 'AppController@addAddress');
Route::post('getUserAddress', 'AppController@getAddress');
Route::post('updateUserAddress', 'AppController@getAddress');
Route::post('setUserDefaultAddress', 'AppController@setUserDefaultAddress');
Route::post('getCategory', 'AppController@getCategory');

#End Mobile Service

Route::get('/', 'PublicController@index');
Route::get('/1', 'PublicController@index1');
Route::get('/logme', 'PublicController@logme');
Route::get('/home', 'PublicController@home');
Route::get('/search','PublicController@search');
Route::get('activate/{id}', 'PublicController@activate');
Route::post('resetEmail', 'PublicController@resetEmail');
Route::get('reset/{id}', 'PublicController@reset');
Route::post('reset/updatePassword', 'PublicController@updatePassword');

#Start of Stripe
Route::post('addCard', 'StripeController@addCard');
Route::get('deleteCard/{customer}/{card}', 'StripeController@deleteCard');
Route::post('listCard', 'StripeController@listCard');
Route::get('deleteCard/{customer}/{card}', 'StripeController@deleteCard');
Route::post('deleteAllCard', 'StripeController@deleteAllCard');
Route::post('deleteCard', 'StripeController@deleteCards');


#End of Stripe


Route::post('likeStore', 'PublicController@likeStore');

Route::post('updateProfileImage', 'PublicController@updateProfileImage');
Route::get('store/{id}', 'PublicController@store');
Route::get('cart', 'PublicController@cart');
Route::post('deleteAddress', 'PublicController@deleteAddress');

Route::get('getCity/{id}', 'PublicController@getCity');
Route::get('getProfileData/{id}', 'PublicController@getProfileData');

Route::post('updateAddress/', 'PublicController@updateAddress');
Route::post('updateAddressEdit/', 'PublicController@updateAddressEdit');


Route::post('updateImg/', 'PublicController@updateImg');
Route::post('fillAddress/', 'PublicController@fillAddress');
Route::post('changepasswordprocess', 'PublicController@changePasswordProcess');


Route::post('updateProfile/', 'PublicController@updateProfile');

Route::post('updateCart', 'PublicController@updateCart');

Route::post('checkFBLogin', 'PublicController@checkFBLogin');
Route::post('checkFBStatus', 'PublicController@checkFBStatus');

Route::post('checkGoogleLogin', 'PublicController@checkGoogleLogin');



##Service
Route::post('loginUser', 'PublicController@loginUser');
Route::post('store/loginUser', 'PublicController@loginUser');
Route::post('registerUser', 'PublicController@registerUser');
Route::get('logout', 'PublicController@logout');


#Admin
Route::get('/admin', 'PublicController@adminLogin');
Route::get('/admin/', 'PublicController@adminLogin');
Route::post('admin/adminAuth', 'PublicController@adminAuth');


Route::post('updateDefault', 'AdminController@updateDefault');


Route::group(array('before' => 'auth'), function()
{


Route::get('profile', 'PublicController@profile');
Route::get('profile1', 'PublicController@profile1');
Route::get('changepassword', 'PublicController@changepassword');

Route::get('managecard', 'PublicController@managecard');

Route::get('managecategory', 'PublicController@managecategory');

Route::post('updateUserCategory', 'PublicController@updateUserCategory');



Route::get('/admin/dashboard', 'AdminController@dashBoard');
#Store
Route::get('/admin/addstore', 'AdminController@addStore');
Route::post('/admin/addStoreDetails', 'AdminController@addStoreDetails');
Route::get('/admin/liststore', 'AdminController@listStore');
Route::get('/admin/liststore/{id}', 'AdminController@listStoreDetails');
Route::post('/admin/updateStoreDetails', 'AdminController@updateStoreDetails');
Route::get('/admin/delStore/{id}', 'AdminController@delStore');
Route::get('/admin/editstore/{id}', 'AdminController@editStore');

#Product
##Cat
Route::get('/admin/product/{id}', 'AdminController@addProduct');
Route::post('/admin/addCat', 'AdminController@addCat');
Route::post('/admin/checkCat', 'AdminController@checkCat');
Route::post('/admin/checkJuice', 'AdminController@checkJuice');
Route::post('/admin/getCat', 'AdminController@getCat');
Route::post('/admin/getJuice', 'AdminController@getJuice');
Route::post('/admin/getCatDetails', 'AdminController@getCatDetails');
Route::post('/admin/deleteCat', 'AdminController@deleteCat');
Route::post('/admin/updateCat', 'AdminController@updateCat');
##Juice
Route::post('/admin/addjuice', 'AdminController@addJuice');
Route::post('/admin/getJuiceDetails', 'AdminController@getJuiceDetails');
Route::post('/admin/updateJuiceDetails', 'AdminController@updateJuiceDetails');
Route::post('/admin/deleteJuice', 'AdminController@deleteJuice');
#Tip of Day
Route::get('/admin/addtipofday', 'AdminController@addTipOfDay');
Route::post('/admin/addTip', 'AdminController@addTip');
Route::get('/admin/edittip/{id}', 'AdminController@editTip');
Route::get('/admin/listtipofday', 'AdminController@listTipOfDay');
Route::post('/admin/updateTip', 'AdminController@updateTip');
Route::get('/admin/delTip/{id}', 'AdminController@delTip');
#Store Type
Route::get('/admin/addtype', 'AdminController@addType');
Route::post('/admin/addTypeDetails', 'AdminController@addTypeDetails');
Route::get('/admin/edittype/{id}', 'AdminController@editType');
Route::get('/admin/listtype', 'AdminController@listType');
Route::post('/admin/updateType', 'AdminController@updateType');
Route::get('/admin/delType/{id}', 'AdminController@delType');
#City
Route::get('/admin/addcity', 'AdminController@addCity');
Route::post('/admin/addCityData', 'AdminController@addCityData');
Route::get('/admin/editcity/{id}', 'AdminController@editCity');
Route::get('/admin/listcity', 'AdminController@listCity');
Route::post('/admin/updateCity', 'AdminController@updateCity');
Route::get('/admin/delCity/{id}', 'AdminController@delCity');
#user
Route::get('/admin/listuser', 'AdminController@listUser');
Route::get('/admin/viewuser/{id}', 'AdminController@viewUser');
#profile
Route::get('/admin/profile', 'AdminController@profileAdmin');
Route::post('/admin/profileUpdate', 'AdminController@profileUpdate');
Route::get('/admin/changepassword', 'AdminController@changepassword');
Route::post('/admin/changepasswordprocess', 'AdminController@changePasswordProcess');



Route::post('/admin/googleplace', 'AdminController@googlePlace');



#Shop
Route::get('/admin/addshop', 'AdminController@addShop');
Route::post('/admin/addShopDetails', 'AdminController@addShopDetails');
Route::get('/admin/listshop', 'AdminController@listShop');
Route::get('/admin/listshop/{id}', 'AdminController@listShopDetails');
Route::post('/admin/updateShopDetails', 'AdminController@updateShopDetails');
#Category
Route::get('/admin/addcategory', 'AdminController@addCategory');
Route::post('/admin/addCategoryDetails', 'AdminController@addCategoryDetails');
Route::get('/admin/listcategory', 'AdminController@listCategory');
Route::get('/admin/listcategory/{id}', 'AdminController@listcategoryDetails');
Route::post('/admin/updateCategoryDetails', 'AdminController@updateCategoryDetails');
#Juice
// Route::get('/admin/addjuice', 'AdminController@addJuice');
// Route::post('/admin/addJuiceDetails', 'AdminController@addJuiceDetails');
// Route::get('/admin/listjuice', 'AdminController@listJuice');
// Route::get('/admin/listjuice/{id}', 'AdminController@listJuiceDetails');
// Route::post('/admin/updateJuiceDetails', 'AdminController@updateJuiceDetails');
#Users
Route::get('/admin/listusers', 'AdminController@listUser');





Route::post('/admin/imageUpload', 'AdminController@imageUpload');


//Route::get('home', 'HomeController@index');

});
Route::get('/admin/logout', 'PublicController@Logouts');

Route::get('/admin/createAdmin/{email}', 'PublicController@createAdmin');