<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Tip extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tipofday';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	

		protected $fillable = ['content', 'date', 'created_by', 'image'];

    public static $tipAdd = array(
        'content' => array('required','unique:tipofday'), 
        'date' => array('required'), 
        'image' => array('required'), 
			);
    public static $tipData = array(
        'content' => array('required','unique:tipofday'), 
        'date' => array('required'), 
        'image' => array('required'), 
			);


}
