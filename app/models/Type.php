<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Type extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'type';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	

		protected $fillable = ['store_id', 'category_id','status'];

    public static $tipData = array(
        'name' => array('required','unique:type'), 
        'image' => array('required'), 
        'status' => array('required'), 
			);

    public static $typeUpdate = array(
        'name' => array('required'), 
        'image' => array('required'), 
        
			);


}
