<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Juice extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'juice';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	

		protected $fillable = ['store_id', 'category_id', 'name', 'image', 'description', 'price', 'status'];

    public static $juiceAdd = array(
        'name' => array('required'), 
        'image' => array('required'), 
        'description' => array('required'), 
        'price' => array('required'), 
        
        'status' => array('required'), 

        
			);


}
