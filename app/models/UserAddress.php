<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class UserAddress extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user_address';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	protected $fillable = ['user_id', 'fullname', 'address', 'city', 'pincode', 'state', 'country'];

	public static $loginUser = array(
        'email' => array('required','email'), 
        'password' => 'required', 
		);

	public static $registerUser = array(
        'email' => array('required','unique:users'), 
        'password' => 'required', 
		);

}
