<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	protected $fillable = ['stripe_id', 'username', 'email', 'password', 'active_link'];

		public function setpasswordAttribute($value)
    {
        $this->attributes['password'] = \Hash::make($value);
    }
    
	public static $loginUser = array(
        'email' => array('required','email'), 
        'password' => 'required', 
		);

	public static $registerUser = array(
        'username' => 'required', 
        'email' => array('required','email','unique:users'), 
        'password' => 'required', 
		);

	public static $changePassword = array(
        'oldPassword' => 'required', 
        'newPassword' => array('required'), 
		);

	

}
