<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class City extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'city';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	

		protected $fillable = [ 'name',   'status'];

    public static $cityData = array(
        'name' => array('required', 'unique:city'), 
        'status' => array('required'), 
        );
        
    public static $cityAdd = array(
        'name' => array('required', 'unique:city'), 
        'status' => array('required'), 
			);

    public static $cityUpdate = array(
        'name' => array('required',), 
        'status' => array('required'), 
			);


}
