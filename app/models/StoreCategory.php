<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class StoreCategory extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'store_category';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	

		protected $fillable = ['name', 'address', 'profile_image', 'type','cover_image', 'pincode', 'city','email', 'phone_code', 'phone', 'mobile_code', 'mobile', 'website', 'status', 'latitude', 'longitude', 'open_time', 'close_time'];

    public static $storeAdd = array(
        'name' => array('required'), 
        'address' => array('required'), 
        'profile_image' => array('required'), 
        'cover_image' => array('required'), 
        'pincode' => array('required'), 
        'email' => array('required', 'unique:store',), 
        'phone' => array('required', 'unique:store', 'min:10', 'integer'), 
        'mobile' => array('required', 'unique:store', 'min:10', 'integer'), 
        'status' => array('required'), 
        'website' => array('required'), 
        'city' => array('required'), 
        'type' => array('required'), 
        'open_time' => array('required', 'date_format:H:i:s'), 
        'close_time' => array('required','date_format:H:i:s'), 
			);

    public static $storeUpdate = array(
        'name' => array('required'), 
        'address' => array('required'), 
        'profile_image' => array('required'), 
        'cover_image' => array('required'), 
        'pincode' => array('required'), 
        'email' => array('required'), 
        'phone' => array('required'), 
        'mobile' => array('required'), 
        'status' => array('required'), 
        'website' => array('required'), 
        'city' => array('required'), 
        'type' => array('required'), 
        'open_time' => array('required', 'date_format:H:i:s'), 
        'close_time' => array('required','date_format:H:i:s'), 
			);


}
