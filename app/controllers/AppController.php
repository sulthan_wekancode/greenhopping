<?php

class AppController extends Controller {

    public function getProduct()
    {
        $store = Category::where('id', Input::get('id'))->get();
        $Response = array('success' => '1', 'store' => $store);
        return $Response;
    }

    public function getStore()
    {
        $store = Store::where('id', Input::get('id'))->get();
        $Response = array('success' => '1', 'store' => $store);
        return $Response;
    }

    public function searchStore()
    {
        $results = Store::where('name', 'LIKE', '%' . Input::get('query') . '%')->where('city', Input::get('location') )->get();
        $Response = array('success' => '1', 'store' => $results);
        return $Response;
    }

    public function getCityList()
    {
        $city = City::where('status', 1)->get();
        $Response = array('success' => '1', 'city' => $city);
        return $Response;
    }

	public function resetEmail()
    {
        if (User::where('email', '=', Input::get('email'))->exists()) { 
            $userData['forget_link'] = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 50);
            $this->sendGridReset(Input::get('email'), $userData['forget_link']);
            User::where('email', Input::get('email'))->update($userData);
            $Response = array('success' => '1');
        } else {
            $Response = array('success' => '0', 'error' => 'User Not Found');
        }
        return $Response;
    }

    public function editPassword()
    {   
        $userData = User::where('id', Input::get('id'))->first();
        $inputData = Input::except('_token');
        $validation = Validator::make($inputData, User::$changePassword);
        if ($validation->passes()) {
            if ($userData) {
            if (Hash::check(Input::get('oldPassword'), $userData['password']))
            {
                $hashedPassword = Hash::make(Input::get('newPassword'));
                $updateData['password'] = $hashedPassword;
                User::where('id', '=', Input::get('id'))->update($updateData);
                $Response = array('success' => '1', 'message' => 'Password changed succesfully');       
            }
            else
            {
                $Response = array('success' => '2', 'message' => 'Old password is incorrect');
            }
        }
        else
        {
            $Response = array('success' => '3', 'message' => 'User not found');
        }
        }
        else
        {
            $Response = array('success' => '0', 'message' => $validation->messages());   
        }
        return $Response;
    }

    public function addAddress() {        
    if (User::where('id', '=', Input::get('id'))->exists()) { 
            $userData['user_id'] = Input::get('id');
            $userData['fullname'] = Input::get('fullName');
            $userData['address'] = Input::get('address');
            $userData['city'] = Input::get('city');
            $userData['state'] = Input::get('state');
            $userData['pincode'] = Input::get('pincode');
            $userData['country'] = Input::get('country');

            $createAddress = UserAddress::create($userData);
            if(Input::get('defaults'))
            {
            
            $defaultAddress['default_address'] = $createAddress->id;
            User::where('id', Input::get('id'))->update($defaultAddress);
            }
            $Response = array('success' => '1');
        } else {
            $Response = array('success' => '0', 'message' => 'User not Found');
        }
        return $Response;
    }

    public function getAddress()
    {
        if (User::where('id', '=', Input::get('id'))->exists()) 
            {
                $user = UserAddress::where('user_id', Input::get('id'))->first();
                $Response = array('success' => '1','data' => $user);
            }
            else
            {
                $Response = array('success' => '0', 'message' => 'User not Found');
            }
                return $Response;
    }

    public function updateAddress() {
        
        if (User::where('id', '=', Input::get('id'))->exists()) { 
            $userData['user_id'] = Input::get('id');
            $userData['fullname'] = Input::get('fullName');
            $userData['address'] = Input::get('address');
            $userData['city'] = Input::get('city');
            $userData['state'] = Input::get('state');
            $userData['pincode'] = Input::get('pincode');
            $userData['country'] = Input::get('country');
            UserAddress::where('id', Input::get('addr_id'))->update($userData);
            $Response = array('success' => '1');

        return $Response;
    }
    }
    public function setUserDefaultAddress()
    {
        if (User::where('id', '=', Input::get('id'))->exists()) { 
            if (UserAddress::where('id', '=', Input::get('default_address'))->where('user_id', '=', Input::get('id'))->exists()) 
            {
                $userData['default_address'] = Input::get('default_address');
                User::where('id', Input::get('id'))->update($userData);    
                $Response = array('success' => '1');
            }
            else
            {
                $Response = array('success' => '0', 'message' => 'Invalid Default Address');
            }
            
            
        } else {
            $Response = array('success' => '0', 'message' => 'User Not Found');
        }
        return $Response;
    }

    public function getCategory()
    {
        $category = Category::where('status', 1)->get();
        $Response = array('success' => '1', 'category' => $category);
        return $Response;       
    }

}