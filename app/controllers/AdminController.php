<?php

class AdminController extends Controller {
public function dashboard()
    {
            return View::make('admin.dashboard.index');
    }
//Store
        public function addStore()
    {
        $city = City::where('status', 1)->get();
        $type = Type::where('status', 1)->get();
        return View::make('admin.store.add')->with('cities', $city)->with('types', $type);
    }
       
    
    public function addStoreDetails()
    {
        $storeData = Input::except('_token');
        $storeData['phone_code'] = '+1';
        $storeData['mobile_code'] = '+1';
        
        if(isset($storeData['type']))
        {
            $storeType = $storeData['type'];
            unset($storeData['type']);    
        }
        
                

        $storeData['mobile'] = substr($storeData['mobile'], 4);
        $storeData['phone'] = substr($storeData['phone'], 5);
        $storeData['mobile'] = str_replace('-', '', $storeData['mobile']);
        $storeData['phone'] = str_replace('-', '', $storeData['phone']);
     
            

    $validation = Validator::make($storeData, Store::$storeAdd);
    if ($validation->passes()) {
                // String manupulations
                
                // $cover = $storeData['cover_image'];
                // $profile = $storeData['profile_image'];
                // $storeData['cover_image'] = $profile;
                // $storeData['profile_image'] = $cover;
                $store = Store::create($storeData);

                foreach ($storeType as $key) {
                $user = new StoreCategory;
                $user->store_id = $store->id;
                $user->category_id = $key;
                $user->save();
                }

                return Redirect::to('admin/addstore')->with('createCat', 1)->with('id', $store->id);
                    
            }
    else{
        

                return Redirect::to('admin/addstore')->withInput()->withErrors($validation->messages());
    }
                return $Response;
    }

    public function listStore()
    {
        $stores = Store::all();
        return View::make('admin.store.list')->with('stores', $stores);
    }
    public function listStoreDetails($id)
    {
        
        
        
        $city = City::where('status', 1)->get();
        $store = Store::where('id', $id)->first();
        return View::make('admin.store.edit')->with('store', $store)->with('cities', $city);
    }

    public function updateStoreDetails()
    {
        

        $storeData = Input::except('_token');

        $storeData['mobile'] = substr($storeData['mobile'], 4);
        $storeData['phone'] = substr($storeData['phone'], 5);
        $storeData['mobile'] = str_replace('-', '', $storeData['mobile']);
        $storeData['phone'] = str_replace('-', '', $storeData['phone']);
        unset($storeData['imgs_cover']);
        unset($storeData['imgs_profile']);
        $validation = Validator::make($storeData, Store::$storeUpdate);
    if ($validation->passes()) {

        // foreach($storeData['type'] as $store)
        // {
        //     $store = StoreCategory::where('id', $id)->delete();    
        // }

        
        //return $storeData;


                // $cover = $storeData['cover_image'];
                // $profile = $storeData['profile_image'];
                // $storeData['cover_image'] = $profile;
                // $storeData['profile_image'] = $cover;


        Store::where('id', Input::get('id'))->update($storeData);
        #return Redirect::to('admin/liststore/'.Input::get('id'))->with('message', 'Store information Updated');
        $store = Store::where('id', Input::get('id'))->first();
        #return View::make('admin.store.edit')->with('store', $store)->with('message', 'Store information Updated');  
        return Redirect::to('admin/editstore/'.Input::get('id'))->with('store', $store)->with('s-message', 'Store information Updated');  
        }
    else
    {
        //return Redirect::to('admin/liststore/'.Input::get('id'))->withInput()->withErrors($validation->messages());
        $store = Store::where('id', Input::get('id'))->first();
     //   return $validation->messages();
        return Redirect::to('admin/liststore/'.Input::get('id'))->with('store', $store)->withInput()->withErrors($validation->messages());
        //return View::make('admin.store.edit')->with('store', $store)->withInput()->withErrors($validation->messages());
    }
    }
    public function editStore($id)
    {

        $store = Store::where('id', $id)->first();
        $city = City::where('status', 1)->get();
        $type = Type::where('status', 1)->get();
        $storeCat = StoreCategory::where('store_id', $id)->get();
        return View::make('admin.store.edit')->with('storeCat', $storeCat)->with('store', $store)->with('cities', $city)->with('types', $type);
    }

    public function delStore($id)
    {
        $store = Store::where('id', $id)->delete();
        $stores = Store::all();
        return Redirect::to('admin/liststore')->with('stores', $stores);
    }    
    public function addProduct($id)
    {
        $category = Category::where('store_id', $id)->orderBy('created_at', 'desc')->get();
        $juice = Juice::where('store_id', $id)->get();
        return View::make('admin.product.add')->with('id', $id)->with('category', $category)->with('juice',$juice);
    }
    public function addCat()
    {
           
        $userData = Input::all();
        

    $validation = Validator::make($userData, Category::$catAdd);
    if ($validation->passes()) {
                // String manupulations
                $store = Category::create($userData);
                $Response = array('success' => '1');
                //return Redirect::to('admin/product'.$userData['store_id'])->with('message', 'Category information Saved')->with('catCreated', '1');
                    
            }
    else{
                
                $Response = array('success' => '0', 'error' => $validation->messages());
                
    }
                return $Response;
    }
    public function checkCat()
    {
        $store = Category::where('name', Input::get('name'))->where('store_id', Input::get('store_id'))->first();

        if(Input::get('update'))
        {
            //return $store['id'].Input::get('id');
            if($store)
            {
                if($store['id']==Input::get('id'))
            {
                $Response = array('success' => '1');       
            }
            else
            {
                $Response = array('success' => '0');       
            }

            }
            else
            {   
                $Response = array('success' => '1');       
            }
            
            return $Response;
                
            }
            
    else
    {         


        if($store)
        {
            if($store['store_id']==Input::get('id'))
            {
                $Response = array('success' => '1');       
            }
            else
            {
                $Response = array('success' => '0');       
            }
        }
        else
        {
                $Response = array('success' => '1');       
        }
        }
        return $Response;

    }

    public function checkJuice()
    {
        $juice = Juice::where('name', Input::get('name'))->where('category_id', Input::get('category_id'))->first();
        if(Input::get('update'))
        {   
            //$juice = Juice::where('name', Input::get('name'))->where('store_id', Input::get('store_id'))->first();
            
            if($juice)
            {
                $storeid = Input::get('store_id');
                if($juice['id']==$storeid)
                {
                    $Response = array('success' => '1');
                }
                else
                {
                    $Response = array('success' => '0');       
                }
            }
            else
            {
                $Response = array('success' => '1');       
            }
            return $Response;
        }
        else
        {
            if($juice)
            {
                $Response = array('success' => '0');   
            }
            else
            {
                $Response = array('success' => '1');
            }
            return $Response;
        }
    }

    public function updateCat()
    {
           $storeData = Input::except('_token');
        
     
        
        
        $validation = Validator::make($storeData, Category::$catAdd);

    if ($validation->passes()) {
        Category::where('id', Input::get('id'))->update($storeData);
        #return Redirect::to('admin/liststore/'.Input::get('id'))->with('message', 'Store information Updated');
        $store = Category::where('id', Input::get('id'))->first();
        #return View::make('admin.store.edit')->with('store', $store)->with('message', 'Store information Updated');  
        //return Redirect::to('admin/listcategory/'.Input::get('id'))->with('store', $store)->with('message', 'Category information Updated');    
        $Response = array('success' => '1');
        }
    else
    {
        //return Redirect::to('admin/liststore/'.Input::get('id'))->withInput()->withErrors($validation->messages());
        
        $store = Category::where('id', Input::get('id'))->first();
        //return $validation->messages();
        //return Redirect::to('admin/listcategory/'.Input::get('id'))->with('store', $store)->withInput()->withErrors($validation->messages());
        //return View::make('admin.store.edit')->with('store', $store)->withInput()->withErrors($validation->messages());
        $Response = array('success' => '0', 'error' => $validation->messages());
    }
        
        return $Response;
    }

    public function getCat()
    {
        
        $id = Input::get('store_id');
        $storeData = Category::where('store_id', $id)->orderBy('created_at', 'desc')->get();
        $html = '';
        foreach ($storeData as $store) {
            $html.='<div class="listCat list-group-item">'.$store['name'];
            $html.='<span class="" style="float:right"> 
            
            <a href="#" class="table-link">
            <span class="fa-stack">
            <i class="fa fa-square fa-stack-2x"></i>
            <i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
            </span>
            </a>
            <a href="#" class="table-link danger">
            <span class="fa-stack" style="color:red">
            <i class="fa fa-square fa-stack-2x"></i>
            <i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
            </span>
            </a>
            </span>';
            $html.='</div>';
        }
        $Response = array('success' => '1', 'html' => $html);
        return $Response;
    }

    public function getJuice()
    {
        $id = Input::get('cat_id');
        $storeData = Juice::where('category_id', $id)->orderBy('created_at', 'desc')->get();
        $head = Category::where('id', $id)->first();
        $head = $head['name'];
        $html = '<ul class="list-group">';
        foreach ($storeData as $store) {
            if($store->status==1)
            {
                $status = 'Enable';    
            }
            else
            {
                $status = 'Disabled';
            }
            
            $html.='<li class="list-group-item">
                    <center>
                    <img src="'.$store->image.'" height="40px" width="40px">
                    </center>
                    <br>
                    <b>Title : </b>'.$store->name.'<br>
                    <b>Price : </b>'.$store->price.'<br>
                    
                    <b>Ingredients : </b>
                    '.$store->description.'
                    <br><b>Status : </b>'.$status.'
                    <span class="" style="float:right">
                    <a class="editJuice table-link" id='.$store->id.'>
                    <span class="fa-stack">
                    <i class="fa fa-square fa-stack-2x"></i>
                    <i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
                    </span>
                    </a>
                    <a class="deleteJuice table-link danger" id='.$store->id.'>
                    <span class="fa-stack" style="color:red">
                    <i class="fa fa-square fa-stack-2x"></i>
                    <i class="fa fa-trash-o fa-stack-1x fa-inverse" ></i>
                    </span>
                    </a>
                    </span>
                    </li>';
            
        }
        $html.='</ul>';
//Jquery Started            
        $html .='<div id="aroni"></div><script>$(document).ready(function() { ';
//Click start
        $html.='$(".editJuice").click(function(event){   $("#juice_cover").hide();';
        $html.=' var ghDomain = $.cookie("ghDomain"); ';
        $html.=' $(".error-msg").html("");  ';
        $html.=' $("#juiceName").val("");  ';
        $html.=' $("#description").val("");  ';
        $html.=' $("#price").val("");  ';
        $html.=' $("#saveJuice").hide(); ';
        $html.=' $("#updateJuice").show(); ';
        $html.=' $("#showAddJuiceCol").show(); ';
        $html.=' $.ajax({ method: "POST",  url: ghDomain+"/admin/getJuiceDetails",  data: { "cat_id": this.id   }  }) ';
        $html.=' .done(function(msg) { ';
        $html.=' $("#juiceName").val(msg.cat.name);  ';
        $html.=' $("#description").val(msg.cat.description);  ';
        $html.=' $("#juice_status").val(msg.cat.status);  ';
        $html.=' $("#juice_Img_Pic").attr("src",msg.cat.image);  $("#juice_Img_Pic").show(); ';
        
        $html.=' $("#juice_image").val(msg.cat.image);  ';
        $html.=' $("#price").val(msg.cat.price);  ';
        $html.=' $("#juiceId").val(msg.cat.id);  ';
        $html.=' console.log(msg);  }); ';
        $html.='});';
//here
        $html.=' $("#updateJuice").click(function(event){ console.log("enter"); ';
        $html.='event.preventDefault();  var name = $("#cat_name").val(); ';
        $html.=' var ghDomain = $.cookie("ghDomain"); ';
         $html.=' var juiceName = $("#juiceName").val();  ';
         $html.=' var id = $("#juiceId").val();  ';
         $html.=' var cat_id = $("#catTitleId").val();  ';
         $html.=' var description = $("#description").val();  ';
         $html.=' var image = $("#juice_image").val();  ';
         $html.=' var price = $("#price").val();  console.log("clicked");';
         $html.=' var status = $("#juice_status option:selected").val();  ';
         $html.=' if(juiceName=="" || description =="" || price=="")  ';
         $html.=' { console.log("err"); ';
         $html.='  if(juiceName=="") { $("#error-juiceName").html("Juice name is required"); } if(description=="") { $("#error-description").html("Ingredients is required"); } if(price=="") { $("#error-price").html("Price is required"); } return false; } ';


         $html.=' var va = "ok"; $.ajax({ method: "POST",   url: ghDomain+"/admin/checkJuice", ';
         $html.=' data: { "store_id": id, "category_id": cat_id, "name": juiceName, "update": "update"} }) ';
         $html.=' .done(function(msg) {  if(msg.success==1) { console.log("ups"); ca();   } else {   $("#error-juiceName").html("Juice name is already taken"); return false;   } });  ';

         $html.=' function ca() { $.ajax({ method: "POST",  url: ghDomain+"/admin/updateJuiceDetails",  data: { "id" : id, "cat_id" : cat_id , "price" : price, "name": juiceName, "description" : description, "image" : image, "status" : status   }  }) ';
        $html.=' .done(function(msg) { console.log(msg);';
        $html.=' if(msg.success==1) { console.log("JI"); $("#"+msg.cat_id+".listCat").trigger("click"); } console.log(msg.id);  console.log(msg); console.log("999");  ';
        $html.=' console.log(msg.success); return false;  ';
        $html.=' $("#description").val(msg.cat.description);  ';
        $html.=' $("#price").val(msg.cat.price); console.log(msg.cat.image);  ';
        $html.=' $("#juice_Img_Pic").attr("src",msg.cat.image);  $("#juice_Img_Pic").show(); ';
        
        $html.=' console.log(msg);  }); ';
        $html.=' } }); ';
//here
        
        $html.='console.log("bf");';
        


//Click End
        $html.=' $(".deleteJuice").click(function(event){ var juice_id = this.id; ';
        $html.=' console.log("aw");  ';
        $html.=' localStorage.setItem("delJuice_cat", $("#catTitleId").val()); localStorage.setItem("delJuice", juice_id);  ';

        $html.='$("#dialog").show(); ';
            $html.='$( "#dialog" ).dialog( "open" ); ';
                $html.='event.preventDefault(); ';
                $html.='return false; ';


        //     $html.='});';

        $html.=' });';
//Click End
         $html .='});</script>';
//model main here
//model end here

//Jquery End
        if(count($storeData)==0)
        {
            $html= '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Juice added yet';
        }
        $Response = array('success' => '1', 'html' => $html, 'head' => $head);
        return $Response;   
    }

    public function getJuiceDetails()
    {
        $juice = Input::get('cat_id');
        $store = Juice::where('id', $juice)->first();
        $Response = array('success' => '1', 'cat' => $store);
        return $Response; 
    }

    public function getCatDetails()
    {
        $juice = Input::get('cat_id');
        $store = Category::where('id', $juice)->first();
        $Response = array('success' => '1', 'cat' => $store);
        return $Response; 
    }
    
    
    public function deleteCat()
    {
        $juice = Input::get('cat_id');
        $store = Category::where('id', $juice)->delete();
        $store = Juice::where('category_id', $juice)->delete();
        $Response = array('success' => '1', 'cat' => $store);
        return $Response; 
    }
    
    public function addJuice()
    {
        $shopData = Input::all();

    $validation = Validator::make($shopData, Juice::$juiceAdd);
    if ($validation->passes()) {
                // String manupulations
                Juice::create($shopData);
                $Response = array('success' => '1');
        
                    
            }
    else{
                $Response = array('success' => '0', 'error' => $validation->messages());
    }
                
                return $Response; 
    }

    public function updateJuiceDetails()
    {
        
        $storeData = Input::except('cat_id');
        
        $validation = Validator::make($storeData, Juice::$juiceAdd);
    if ($validation->passes()) {
        Juice::where('id', Input::get('id'))->update($storeData);
        $Response = array('success' => '1', 'cat_id' =>Input::get('cat_id'), 'id' => Input::get('id'));
        }
    else
    {
        $Response = array('success' => '0', 'error' => $validation->messages());
    }
        return $Response;
    }

    public function deleteJuice()
    {
        $juice = Input::get('juice_id');
        $store = Juice::where('id', $juice)->delete();
        $Response = array('success' => '1', 'cat' => $store);
        return $Response; 
    }
#tip of day
    public function addTipOfDay()
    {
        return View::make('admin.tipofday.add');
    }
    public function addTip()
    {
        
        $tipData = Input::except('_token');
        $tipData['created_by'] = 1;
        
    $validation = Validator::make($tipData, Tip::$tipData);
    $tipData['date'] = date("Y-d-m", strtotime($tipData['date']));

    if ($validation->passes()) {
                // String manupulations

                $store = Tip::create($tipData);

                return Redirect::to('admin/addtipofday')->with('s-message', 'Tip created succesfully');
                    
            }
    else{
                
                return Redirect::to('admin/addtipofday')->withInput()->withErrors($validation->messages());
    }
                return $Response;
        
    }

    public function listTipOfDay()
    {
        $stores = Tip::all();

        return View::make('admin.tipofday.list')->with('stores', $stores);
    }
    public function editTip($id)
    {
        
        $store = Tip::where('id', $id)->first();
        return View::make('admin.tipofday.edit')->with('store', $store);

        $tipData = Input::except('_token');
        $validation = Validator::make($tipData, Tip::$tipAdd);

    if ($validation->passes()) {
        Tip::where('id', Input::get('id'))->update($tipData);
        $store = Tip::where('id', Input::get('id'))->first();
        return Redirect::to('admin/edittip/'.Input::get('id'))->with('store', $store)->with('s-message', 'Tip Updated succesfully');  
        }
    else
    {
        $store = tip::where('id', Input::get('id'))->first();
        return Redirect::to('admin/edittip/'.Input::get('id'))->with('store', $store)->withInput()->withErrors($validation->messages());
    }

    }
    public function updateTip()
    {
     
         $tipData = Input::except('_token');
          $tipData['date'] = date("Y-d-m", strtotime($tipData['date']));
         
        $validation = Validator::make($tipData, Tip::$tipAdd);
    if ($validation->passes()) {
        Tip::where('id', Input::get('id'))->update($tipData);
        $store = Tip::where('id', Input::get('id'))->first();
        return Redirect::to('admin/edittip/'.Input::get('id'))->with('store', $store)->with('s-message', 'Tip Updated succesfully');  
        }
    else
    {
        $store = tip::where('id', Input::get('id'))->first();
        return Redirect::to('admin/edittip/'.Input::get('id'))->with('store', $store)->withInput()->withErrors($validation->messages());
    }
    }

    public function delTip($id)
    {
    
        $store = Tip::where('id', $id)->delete();
        $stores = Tip::all();
    
        return Redirect::to('admin/listtipofday')->with('stores', $stores);
    }

    #type
    public function addType()
    {
        return View::make('admin.type.add');
    }
    public function addTypeDetails()
    {
        
        $tipData = Input::except('_token');
        $tipData['status'] = 1;
        
    $validation = Validator::make($tipData, Type::$tipData);

    if ($validation->passes()) {
                // String manupulations

                $store = Type::create($tipData);

                return Redirect::to('admin/addtype')->with('s-message', 'Category Type created succesfully');
                    
            }
    else{
                
                return Redirect::to('admin/addtype')->withInput()->withErrors($validation->messages());
    }
                return $Response;
        
    }

    public function listType()
    {
        $stores = Type::all();

        return View::make('admin.type.list')->with('stores', $stores);
    }
    public function editType($id)
    {
        
        $store = Type::where('id', $id)->first();
        return View::make('admin.type.edit')->with('store', $store);


    }
    public function updateType()
    {
     
         $tipData = Input::except('_token');
          
         
        $validation = Validator::make($tipData, Type::$typeUpdate);
    if ($validation->passes()) {
        Type::where('id', Input::get('id'))->update($tipData);
        $store = Type::where('id', Input::get('id'))->first();
        return Redirect::to('admin/edittype/'.Input::get('id'))->with('store', $store)->with('s-message', 'Category Type Updated succesfully');  
        }
    else
    {
        $store = Type::where('id', Input::get('id'))->first();

        return Redirect::to('admin/edittype/'.Input::get('id'))->with('store', $store)->withInput()->withErrors($validation->messages());
    }
    }

    public function delType($id)
    {
        
        $store = Type::where('id', $id)->delete();
        $stores = Type::all();
    
        return Redirect::to('admin/listtype')->with('stores', $stores);
    }


#Citys
    public function addCity()
    {
        return View::make('admin.city.add');
    }
    public function addCityData()
    {
        $tipData = Input::except('_token');
        $tipData['created_by'] = 1;
    $validation = Validator::make($tipData, City::$cityData);
    if ($validation->passes()) {
                // String manupulations
                $store = City::create($tipData);
                return Redirect::to('admin/addcity')->with('s-message', 'City Added succesfully');
                    
            }
    else{
                
                return Redirect::to('admin/addcity')->withInput()->withErrors($validation->messages());
    }
                return $Response;
        
    }

    public function listCity()
    {
        $stores = City::all();

        return View::make('admin.city.list')->with('stores', $stores);
    }
    public function editCity($id)
    {
        
        $store = City::where('id', $id)->first();
        return View::make('admin.city.edit')->with('store', $store);
    }
    public function updateCity()
    {
         $tipData = Input::except('_token');
        $validation = Validator::make($tipData, City::$cityUpdate);
    if ($validation->passes()) {
        City::where('id', Input::get('id'))->update($tipData);
        $store = City::where('id', Input::get('id'))->first();
        return Redirect::to('admin/editcity/'.Input::get('id'))->with('store', $store)->with('s-message', 'City Updated succesfully');  
        }
    else
    {
        $store = City::where('id', Input::get('id'))->first();
        return Redirect::to('admin/editcity/'.Input::get('id'))->with('store', $store)->withInput()->withErrors($validation->messages());
    }
    }

    public function delCity($id)
    {
    
        $store = City::where('id', $id)->delete();
        $stores = City::all();
    
        return Redirect::to('admin/listcity')->with('stores', $stores);
    }



    public function listUser()
    {
        
        $stores = User::all();
        return View::make('admin.user.list')->with('stores',$stores);
    }

    public function viewUser($id)
    {
        $user = User::where('id', $id)->first();
        return View::make('admin.user.view')->with('user', $user);
    }

    public function profileAdmin()
    {
     $adminData = User::where('id', Auth::user()->id)->first();
     return View::make('admin.admin.profile')->with('data',$adminData);   
    }
    public function profileUpdate()
    {
     
    $tipData = Input::except('_token');
        $validation = Validator::make($tipData, Admin::$userAdd);
    if ($validation->passes()) {
        unset($tipData['profile_image']);
        unset($tipData['cover_image']);
        $tipData['mobile'] = substr($tipData['mobile'], 4);
                $tipData['phone'] = substr($tipData['phone'], 4);
                $tipData['mobile'] = str_replace('-', '', $tipData['mobile']);
                $tipData['phone'] = str_replace('-', '', $tipData['phone']);
        Admin::where('id', Input::get('id'))->update($tipData);
        $adminData = User::where('id', Auth::user()->id)->first();
        return Redirect::to('admin/profile')->with('data', $adminData)->with('message', 'Profile Updated succesfully');  
        }
    else
    {
        $store = User::where('id', Input::get('id'))->first();
        
        return Redirect::to('admin/profile')->with('store', $store)->withInput()->withErrors($validation->messages());
    }


    # $adminData = User::where('id', Auth::user()->id)->first();
    # return Redirect::to('admin/profile')->withInput()->with('data', $adminData);
    }
    
    public function changepassword()
    {
     $adminData = User::where('id', Auth::user()->id)->first();
     return View::make('admin.admin.password')->with('data',$adminData);   
    }
    
    public function changePasswordProcess() {
        $PasswordData = Input::all();

        Validator::extend('pwdvalidation', function($field, $value, $parameters) {
            return Hash::check($value, Auth::user()->password);
        });

        $messages = array('pwdvalidation' => 'The Old Password is Incorrect');

        $validator = Validator::make($PasswordData, Admin::$rulespwd, $messages);
        if ($validator->passes()) {
            $user = Admin::find(Auth::user()->id);
            $user->password = Input::get('NewPassword');
            $user->save();
            return Redirect::to('admin/changepassword')->with('message', 'The Password Information was Updated');
        } else {
            return Redirect::to('admin/changepassword')->withInput()->withErrors($validator);
        }
    }
    public function googlePlace()
    {      
        
        $request_url = "http://maps.googleapis.com/maps/api/geocode/xml?address=".Input::get('address')."&sensor=true";
          $xml = simplexml_load_file($request_url) or die("url not loading");
          $status = $xml->status;
          $Lon="";
           $Lat="";
          if ($status=="OK") {
              $Lat = $xml->result->geometry->location->lat;
              $Lon = $xml->result->geometry->location->lng;
              $LatLng = "$Lat,$Lon";
          }


        return $LatLng;
        #return Input::all();

    }


    public function updateProfile() {
        
        
        if (User::where('id', '=', Input::get('id'))->exists()) { 
            $userData['username'] = Input::get('name');
            $userData['default_city'] = Input::get('location');
            User::where('id', Input::get('id'))->update($userData);
            $Response = array('success' => '1');
        } else {
            $Response = array('success' => '0', 'err' => $validation->messages());
        }
        return $Response;
    }

    public function updateDefault() 
    {

        if (User::where('id', '=', Input::get('id'))->exists()) { 
            
            $userData['default_address'] = Input::get('default');
            User::where('id', Input::get('id'))->update($userData);
            //return $this->getProfileData(Input::get('id'));
            $Response = array('success' => '1');
        } else {
            $Response = array('success' => '0', 'err' => $validation->messages());
        }
        return $Response;


        
    }

        public function imageUpload()
        {
            return app('App\Http\Controllers\ImageController')->uploadImage(Input::all());
            // Bucket Name
            $bucket="greenhoppingbucket";
            if (!class_exists('S3'))require_once('S3.php');
            //AWS access info
            if (!defined('awsAccessKey')) define('awsAccessKey', 'ACCESS_KEY');
            if (!defined('awsSecretKey')) define('awsSecretKey', 'ACCESS_Secret_KEY');

            $s3 = new S3(awsAccessKey, awsSecretKey);
            $s3->putBucket($bucket, S3::ACL_PUBLIC_READ);
        }

}