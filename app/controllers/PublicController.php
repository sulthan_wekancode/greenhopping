<?php

class PublicController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	public function index()
	{
            
            $city = City::where('status', 1)->get();
            return View::make('user.index')->with('cities', $city);
	}
    public function index1()
    {
            
            $city = City::where('status', 1)->get();
            return View::make('user.index1')->with('cities', $city);
    }
    public function home()
    {
            $city = City::where('status', 1)->get();
            return View::make('public.home')->with('cities', $city);
    }
    public function store($id)
    {
            $city = City::where('status', 1)->get();
            $store = Store::where('id', $id)->first();
            $totalLike = StoreLike::where('store_id', $id)->count();
            if (Auth::check())
            {
                $like = StoreLike::where('user_id', Auth::user()->id)->where('store_id', $id)->first();
                return View::make('user.store')->with('cities', $city)->with('store', $store)->with('like', $like)->with('totalLike', $totalLike);
            }
            else
            {
                return View::make('user.store')->with('cities', $city)->with('store', $store)->with('totalLike', $totalLike);
            }
            
    }

    public function profile()
    {
            $city = City::where('status', 1)->get();
            $category = Category::where('status', 1)->get();
            return View::make('user.profile')->with('cities', $city)->with('category', $category);
    }

    public function changepassword()
    {
            $city = City::where('status', 1)->get();
            return View::make('user.password')->with('cities', $city); 
    }

    public function managecard()
    {
            $city = City::where('status', 1)->get();
            return View::make('user.card')->with('cities', $city); 
    }

    public function managecategory()
    {
            $city = City::where('status', 1)->get();
            $category = Category::where('status', 1)->get();
            return View::make('user.category')->with('cities', $city)->with('category', $category);
    }

    public function updateUserCategory()
    {

        if (User::where('id', '=', Input::get('user_id'))->exists()) 
        {
            if (UserCategory::where('user_id', '=', Input::get('user_id'))->where('category_id', '=', Input::get('category_id'))->exists()) 
            {
                $deletedRows = UserCategory::where('user_id', Input::get('user_id'))->where('category_id', Input::get('category_id'))->delete();
                $Response = array('success' => '1');
            }
            else
            {
                $user = new UserCategory;
                $user->user_id = Input::get('user_id');
                $user->category_id = Input::get('category_id');
                $user->save();
                $Response = array('success' => '1');
            }
        }
        else
        {
            $Response = array('success' => '0', 'message' => 'User not Found');
        }
            return $Response;
    }

    public function profile1()
    {
            $city = City::where('status', 1)->get();
            return View::make('public.profile')->with('cities', $city); 
    }
    public function search()
    {
        
        
        if($_COOKIE['latitude'] && $_COOKIE['longitude'])
        {
            $lat = $_COOKIE['latitude'];
            $long = $_COOKIE['longitude'];
            $results = DB::select(" SELECT *,
                    (((ACOS(SIN(( ".$lat." *PI()/180)) * SIN((latitude * PI()/180))
                    +COS((".$lat." *PI()/180)) * COS((latitude*PI()/180)) *
                    COS(((".$long." - longitude)*PI()/180))))*180/PI())*60*1.1515*1.609344) 
                    AS distance
                    FROM store
                    WHERE city = ".Input::get('location')."
                    AND name LIKE '%".Input::get('query')."%'");    
        }
        else
        {
            $results = DB::select(" SELECT *,
                    (((ACOS(SIN(( 12.9456895 *PI()/180)) * SIN((latitude * PI()/180))
                    +COS((12.9456895 *PI()/180)) * COS((latitude*PI()/180)) *
                    COS(((77.5440288 - longitude)*PI()/180))))*180/PI())*60*1.1515*1.609344) 
                    AS distance
                    FROM store
                    WHERE city = ".Input::get('location')."
                    AND name LIKE '%".Input::get('query')."%'");
        }

        
        

        $results_like = Store::where('name', 'LIKE', '%' . Input::get('query') . '%')->where('city', Input::get('location') )-> where('status', 1 )->get();



        $city = City::where('status', 1)->get();
        $cityName = City::where('id', Input::get('location'))->first();
        return View::make('user.search')->with('results_like', $results_like)->with('results', $results)->with('cities', $city)->with('cityName', $cityName)->with('location', Input::get('location'));
        

    }

   

    public function updateCart()
    {
        $Response = array('success' => '1');
        return $Response;
    }

#Admin Section
        #Admin Login
	public function adminLogin()
	{
            return View::make('admin.login.login');
	}
        #Admin Authentication
        public function adminAuth()
	{
            $loginData = Input::except('_token');
		$validation = Validator::make($loginData, Admin::$loginAdmin);
		if ($validation->passes()) {
                    $userData = User::where('email', '=', $loginData['email'])->first();
                        if ($userData) {
                            if (Hash::check($loginData['password'], $userData['password'])) {
                                Auth::loginUsingId($userData['id']);
                                return Redirect::intended('admin/liststore');
                            }
                            else
                            {
                                return Redirect::to('admin')->withInput()->with('message', 'Password is Wrong');
                            }
                        }
                        else
                        {
                            return Redirect::to('admin')->withInput()->with('message', 'Admin not Found');
                        }
                }
		else{
                            //return Redirect::to('admin')->withInput()->with('message', $validation->messages());
                            return Redirect::to('admin')->withInput()->withErrors($validation->messages());
		}
                    return $Response;
	}            
        //To create Admin
        public function createAdmin($email)
        {
            
            $loginData['usertype'] = 1; $loginData['email'] = $email; $loginData['password'] = 123456;
            Admin::create($loginData);
        }
        public function logme()
        {
            $user = User::find(63);  
            Auth::login($user);   
        }



        public function getProfileData($id)
    {
        
        if (User::where('id', '=', $id)->exists()) {
        $userData = User::where('id', $id)->first();       
        
        


        $userAddressData = '<script>

$(document).ready(function() {



$(".close").click(function(){

    var r = confirm("Are you sure you want to delete the Address?");
    if (r == true) {
        
    } else {
        return false;
    }

var id = $("#userId").val();

$.ajax({
    url: "deleteAddress",
    type : "POST",  
     data: { id: id, address : this.id},
    success: function(message)
    {
    console.log(message.success)
    if(message.success==1)
        {
    
var id = $("#userId").val();
    $.ajax({
    url: "getProfileData/"+id,
    type : "GET",  
     data: {  },
    success: function(message)
    {
    if(message.success==1)
        {
        $("#userAddressData").html(message.address);       
    $("#availableCity").html(message.city);        
        }
        else
        {

$("#availableCity").html(message.city);        
        }
        
    }
    });
        }
        else
        {

        }
        
    }
    });
 
});
$(".defAddr").click(function(){
    console.log(this.id);
var pass = this.id;



var id = $("#userId").val();

    $.ajax({
    url: "updateDefault",
    type : "POST",  
     data: {id : id, default : pass },
    success: function(message)
    {
    console.log(message);
    console.log(message.success);
    if(message.success==1)
        {



var id = $("#userId").val();
    $.ajax({
    url: "getProfileData/"+id,
    type : "GET",  
     data: {  },
    success: function(message)
    {
    if(message.success==1)
        {
        $("#userAddressData").html(message.address);       
    $("#availableCity").html(message.city);        
        }
        else
        {

$("#availableCity").html(message.city);        
        }
        
    }
    });
            
    $("#profileUpdateProfile").html("Profile Updated Successfully");        

        }
        else
        {
            $("#profileUpdateProfile").html("Error in Update");
        }
    }
    
});







});
});
        </script>


<script>
$(document).ready(function() {
$(".editAddress").click(function(){

console.log(this.id);
console.log("verp");

$(".updateAddressItem").html("Update Address");



$("#addressCapChanger").html("Edit Address");
$("#addr_id").val(this.id);


var id = $("#userId").val();

 $.ajax({
    url: "fillAddress",
    type : "POST",  
     data: { id : this.id},
    success: function(messages)
    {
    if(messages.success==1)
        {
        
    $("#fullName").val(messages.data.fullname);
    $("#addressVal").val(messages.data.address);
    $("#city").val(messages.data.city);
    $("#state").val(messages.data.state);
    $("#pincode").val(messages.data.pincode);
    $("#country").val(messages.data.country);
    $("#DefaultupdateAddress").hide();
    $("#updateAddress").hide();
    $(".updateAddressItem").show();
        }
        else
        {
            
        } 
       
    }
});    
















var WH = $(window).height();  
var SH = $("body")[0].scrollHeight;
$("html, body").stop().animate({scrollTop: SH-WH}, 1000);
});
});
</script>

        ';
        $userAddressDataFirst = '';
        $userAddress = UserAddress::where('user_id', $id)->get();
        foreach ($userAddress as $key) 
        {

            if ($userData['default_address']==$key['id']) 
            {
                
         $userAddressDataFirst ='

<div class="profileAddress">
<a href="#" class="close closebtn" id="'.$key['id'].'">&times;</a>
<h5>'.$key['fullname'].'&nbsp;<img src="public/static/tick.png" /><span class="editAddress" id="'.$key['id'].'"> Edit</span></h5> 
<p>'.$key['address'].', '.$key['city'].', '.$key['state'].'-'.$key['pincode'].', '.$key['country'].'</p>
</div>
';                   
            }
            else
            {

         $userAddressData.='


<div class="profileAddress">
<a href="#" class="close closebtn" id="'.$key['id'].'">&times;</a>
<h5>'.$key['fullname'].' <i class="defaultAddress"></i> <span class="editAddress" id="'.$key['id'].'"> Edit</span></h5> 
<p>'.$key['address'].', '.$key['city'].', '.$key['state'].'-'.$key['pincode'].', '.$key['country'].'</p>
</div>

';   

            }


        }



$afterRefine = $userAddressDataFirst.$userAddressData;


        $Response = array('success' => 1, 'data' => $userData, 'address' => $afterRefine);

        


        return $Response;
        }
        else{
        return 0;
        }   
    }


        public function getCity($id)
    {


        $city = City::lists('id' ,'name');
        $userData = User::where('id', $id)->first();

        $array = '<select class="form-control" id="userLocation">';
        foreach ($city as $key => $value) {

            if($userData['default_city']==$value)
            {
                $array.= '<option id="'.$value.'" selected>'.$key.'</option>';
            }
            else
            {
                $array.= '<option id="'.$value.'">'.$key.'</option>';    
            }

            
        }
        $array .=  '</select>';
        
        $Response = array('city' => $array);
    



        return $Response;

    }
public function updateAddressEdit() {
        
        if (User::where('id', '=', Input::get('id'))->exists()) { 
            $userData['user_id'] = Input::get('id');
            $userData['fullname'] = Input::get('fullName');
            $userData['address'] = Input::get('address');
            $userData['city'] = Input::get('city');
            $userData['state'] = Input::get('state');
            $userData['pincode'] = Input::get('pincode');
            $userData['country'] = Input::get('country');
            UserAddress::where('id', Input::get('addr_id'))->update($userData);
            $Response = array('success' => '1');

        return $Response;
    }
}

public function deleteAddress()
    {
        if (User::where('id', '=', Input::get('id'))->exists()) {
            if (UserAddress::where('id', '=', Input::get('address'))->exists()) {
             $deletedRows = UserAddress::where('id', Input::get('address'))->delete();
             $Response = array('success' => 1);        
            }
            else
            {
            $Response = array('success' => 0);        
            }   
            
        }
        else
        {
            $Response = array('success' => 0);           
        }
        return $Response;

    }
    
    public function updateAddress() {
        
    if (User::where('id', '=', Input::get('id'))->exists()) { 
            $userData['user_id'] = Input::get('id');
            $userData['fullname'] = Input::get('fullName');
            $userData['address'] = Input::get('address');
            $userData['city'] = Input::get('city');
            $userData['state'] = Input::get('state');
            $userData['pincode'] = Input::get('pincode');
            $userData['country'] = Input::get('country');

            $createAddress = UserAddress::create($userData);
            if(Input::get('defaults'))
            {
            
            $defaultAddress['default_address'] = $createAddress->id;
            User::where('id', Input::get('id'))->update($defaultAddress);
            }
            $Response = array('success' => '1');
        } else {
            $Response = array('success' => '0', 'err' => '1');
        }
        return $Response;
}

    public function updateImg()
    {
        $userImg['image'] = Input::get('image');
        User::where('id', Input::get('id'))->update($userImg);
    }

     public function updateDefault() 
    {

        if (User::where('id', '=', Input::get('id'))->exists()) { 
            
            $userData['default_address'] = Input::get('default');
            User::where('id', Input::get('id'))->update($userData);
            //return $this->getProfileData(Input::get('id'));
            $Response = array('success' => '1');
        } else {
            $Response = array('success' => '0', 'err' => $validation->messages());
        }
        return $Response;


        
    }


        public function Logouts()
    {
        Session::flush();
        return Redirect::to('/admin');
    }




    public function loginUser() {
        $userData = Input::all();
        $validation = Validator::make($userData, User::$loginUser);
        if ($validation->passes()) {
            $userEmail = Input::get('email');
            $userPassword = Input::get('password');
            if (User::where('email', '=', $userEmail)->exists()) {
            if (Auth::attempt(['email' => $userEmail, 'password' => $userPassword])) {
                $userData = User::where('email', $userEmail)->first();
                if ($userData['status']==1) {

                    // if (UserSession::where('user_id', '=', $userData['id'])->exists()) {
                    // $deviceData['ip_address'] = $_SERVER['REMOTE_ADDR'];
                    // $deviceData['user_id'] = $userData['id'];
                    // $deviceData['user_agent'] = $this->getPlatform();
                    // UserSession::where('user_id', '=', $userData['id'])->update($deviceData);
                    // }
                    // else
                    // {
                    //     $deviceData['ip_address'] = $_SERVER['REMOTE_ADDR'];
                    //     $deviceData['user_id'] = $userData['id'];
                    //     $deviceData['user_agent'] = $this->getPlatform();
                    //     UserSession::create($deviceData);
                    // }

                    
                    $Response = array('success' => '1', 'userData' => $userData);                    
                }
                else
                {
                    $Response = array('success' => '2', 'message' => 'Account Not Activated');
                }
            } 
            else{
                $Response = array('success' => '3', 'message' => 'Invalid Password');
            }
            }
            else
            {
                $Response = array('success' => '4', 'message' => 'User Not Found');
            }
        }
            else
            {
                $Response = array('success' => '0', 'error' => $validation->messages());       
            }
            return $Response;

    }

    public function registerUser() {
        $userData = Input::all();
        $validation = Validator::make($userData, User::$registerUser);
        if ($validation->passes()) {
            $userData['active_link'] = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 50);
            $createCustomer = \Stripe\Customer::create(array('account_balance'=>0, 'email'=>Input::get('email')));
            $userData['stripe_id'] = $createCustomer->id;
            $User = User::create($userData);
            $this->sendGrid(Input::get('email'), $userData['active_link']);
            $Response = array('success' => '1', 'userid' => $userData);
        } else {
            $Response = array('success' => '0', 'error' => $validation->messages());
        }
        return $Response;
    }

    public function updateProfile() {
        
        
        if (User::where('id', '=', Input::get('id'))->exists()) { 
            $userData['username'] = Input::get('name');
            $userData['default_city'] = Input::get('location');
            User::where('id', Input::get('id'))->update($userData);
            $Response = array('success' => '1');
        } else {
            $Response = array('success' => '0', 'err' => $validation->messages());
        }
        return $Response;
    }
    public function checkFbLogin()
    {

        $user = User::where('email', Input::get('email'))->first();
        //return Input::get('facebookId');
//return Input::all();
        if($user)
        {
            $Response = array('success' => '1','id' => $user->id);
        }
        else
        {   
            $createCustomer = \Stripe\Customer::create(array('account_balance'=>0, 'email'=>Input::get('email')));
            $user = new User;
            $user->facebook_id = Input::get('facebookId');
            $user->username = Input::get('username');
            $user->email = Input::get('email');
            $user->stripe_id = $createCustomer->id;
            $user->status = 1;
            
            $user->save();
            $Response = array('success' => '2','id' => $user->id);   
        }
            $user = User::find($user->id);
            Auth::login($user);   
        return $Response;
    }
    public function checkFBStatus()
    {
        return Input::all();
    }
    
    public function checkGoogleLogin()
    {
        $user = User::where('google_id', Input::get('googleID'))->first();
        if($user)
        {
            $Response = array('success' => '1','id' => $user->id);
        }
        else
        {   
            $createCustomer = \Stripe\Customer::create(array('account_balance'=>0, 'email'=>Input::get('email')));
            $user = new User;
            $user->google_id = Input::get('googleID');
            $user->username = Input::get('googleUserName');
            $user->email = Input::get('googleEmail');
            $user->stripe_id = $createCustomer->id;
            $user->status = 1;
            $user->save();
            $Response = array('success' => '2','id' => $user->id);   
        }
            $user = User::find($user->id);
            Auth::login($user);   
        return $Response;
    }
    public function logout()
    {
        Session::flush();
        return Redirect::intended('/');
    }

    public function fillAddress()
    {
        $user = UserAddress::where('id', Input::get('id'))->first();
        $Response = array('success' => '1','data' => $user);
        return $Response;
    }


        public function sendGrid($email, $link)
    {
        
        $mailHtml ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">


<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<title>GreenHopping</title>
<script type="text/javascript">
//<![CDATA[
try{if (!window.CloudFlare) {var CloudFlare=[{verbose:0,p:0,byc:0,owlid:"cf",bag2:1,mirage2:0,oracle:0,paths:{cloudflare:"/cdn-cgi/nexp/dok3v=1613a3a185/"},atok:"c50cc9be038eff534235017b430ed633",petok:"05521d0d16aca7c407e6f17a5f2291847a8dd3c5-1449817077-1800",zone:"adbee.technology",rocket:"0",apps:{"ga_key":{"ua":"UA-49262924-2","ga_bs":"2"}},sha2test:0}];!function(a,b){a=document.createElement("script"),b=document.getElementsByTagName("script")[0],a.async=!0,a.src="../ajax.cloudflare.com/cdn-cgi/nexp/dok3v%3d38857570ac/cloudflare.min.js",b.parentNode.insertBefore(a,b)}()}}catch(e){};
//]]>
</script>
<style type="text/css">#outlook a{padding:0;}.ReadMsgBody{width:100%;}.ExternalClass{width:100%;}.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height:100%;}body,table,td,p,a,li,blockquote{-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;}table,td{mso-table-lspace:0pt;mso-table-rspace:0pt;}img{-ms-interpolation-mode:bicubic;}body{margin:0;padding:0;}img{border:0;height:auto;line-height:100%;outline:none;text-decoration:none;}table{border-collapse:collapse!important;}body,#bodyTable,#bodyCell{height:100%!important;margin:0;padding:0;width:100%!important;}#bodyCell{padding:20px;}#templateContainer{width:600px;}body,#bodyTable{background-color:#EEEEEE;}#bodyCell{border-top:4px solid #BBBBBB;}#templateContainer{border:1px solid #BBBBBB;}h1{color:#202020!important;display:block;font-family:Helvetica;font-size:26px;font-style:normal;font-weight:bold;line-height:100%;letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;text-align:left;}h2{color:#404040!important;display:block;font-family:Helvetica;font-size:20px;font-style:normal;font-weight:bold;line-height:100%;letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;text-align:left;}h3{color:#606060!important;display:block;font-family:Helvetica;font-size:16px;font-style:italic;font-weight:normal;line-height:100%;letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;text-align:left;}h4{color:#808080!important;display:block;font-family:Helvetica;font-size:14px;font-style:italic;font-weight:normal;line-height:100%;letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;text-align:left;}#templatePreheader{background-color:#F4F4F4;border-bottom:1px solid #CCCCCC;}.preheaderContent{color:#808080;font-family:Helvetica;font-size:10px;line-height:125%;text-align:left;}.preheaderContent a:link,.preheaderContent a:visited,.preheaderContent a .yshortcuts{color:#606060;font-weight:normal;text-decoration:underline;}#templateHeader{background-color:#F4F4F4;border-bottom:1px solid #CCCCCC;}.headerContent{color:#505050;font-family:Helvetica;font-size:20px;font-weight:bold;line-height:100%;padding-top:0;padding-right:0;padding-bottom:0;padding-left:0;text-align:left;vertical-align:middle;}.headerContent a:link,.headerContent a:visited,.headerContent a .yshortcuts{color:#03a9f4;font-weight:normal;text-decoration:underline;}#headerImage{height:auto;max-width:600px;}#templateBody{background-color:#F4F4F4;border-top:1px solid #FFFFFF;border-bottom:1px solid #CCCCCC;}.bodyContent{color:#505050;font-family:Helvetica;font-size:16px;line-height:150%;padding-top:20px;padding-right:20px;padding-bottom:20px;padding-left:20px;text-align:left;}.bodyContent a:link,.bodyContent a:visited,.bodyContent a .yshortcuts{color:#03a9f4;font-weight:normal;text-decoration:underline;}.bodyContent img{display:inline;height:auto;max-width:560px;}.templateColumnContainer{width:200px;}#templateColumns{background-color:#F4F4F4;border-top:1px solid #FFFFFF;border-bottom:1px solid #CCCCCC;}.leftColumnContent{color:#505050;font-family:Helvetica;font-size:14px;line-height:150%;padding-top:0;padding-right:20px;padding-bottom:20px;padding-left:20px;text-align:left;}.leftColumnContent a:link,.leftColumnContent a:visited,.leftColumnContent a .yshortcuts{color:#03a9f4;font-weight:normal;text-decoration:underline;}.centerColumnContent{color:#505050;font-family:Helvetica;font-size:14px;line-height:150%;padding-top:0;padding-right:20px;padding-bottom:20px;padding-left:20px;text-align:left;}.centerColumnContent a:link,.centerColumnContent a:visited,.centerColumnContent a .yshortcuts{color:#03a9f4;font-weight:normal;text-decoration:underline;}.rightColumnContent{color:#505050;font-family:Helvetica;font-size:14px;line-height:150%;padding-top:0;padding-right:20px;padding-bottom:20px;padding-left:20px;text-align:left;}.rightColumnContent a:link,.rightColumnContent a:visited,.rightColumnContent a .yshortcuts{color:#03a9f4;font-weight:normal;text-decoration:underline;}.leftColumnContent img,.rightColumnContent img{display:inline;height:auto;max-width:260px;}#templateFooter{background-color:#F4F4F4;border-top:1px solid #FFFFFF;}.footerContent{color:#808080;font-family:Helvetica;font-size:10px;line-height:150%;padding-top:20px;padding-right:20px;padding-bottom:20px;padding-left:20px;text-align:left;}.footerContent a:link,.footerContent a:visited,.footerContent a .yshortcuts,.footerContent a span{color:#606060;font-weight:normal;text-decoration:underline;}@media only screen and (max-width: 480px){body,table,td,p,a,li,blockquote{-webkit-text-size-adjust:none!important;}body{width:100%!important;min-width:100%!important;}#bodyCell{padding:10px!important;}#templateContainer{max-width:600px!important;width:100%!important;}h1{font-size:24px!important;line-height:100%!important;}h2{font-size:20px!important;line-height:100%!important;}h3{font-size:18px!important;line-height:100%!important;}h4{font-size:16px!important;line-height:100%!important;}#templatePreheader{display:none!important;}#headerImage{height:auto!important;max-width:600px!important;width:100%!important;}.headerContent{font-size:20px!important;line-height:125%!important;}.bodyContent{font-size:18px!important;line-height:125%!important;}.templateColumnContainer{display:block!important;width:100%!important;}.columnImage{height:auto!important;max-width:480px!important;width:100%!important;}.leftColumnContent{font-size:16px!important;line-height:125%!important;}.centerColumnContent{font-size:16px!important;line-height:125%!important;}.rightColumnContent{font-size:16px!important;line-height:125%!important;}.footerContent{font-size:14px!important;line-height:115%!important;}.footerContent a{display:block!important;}}</style>
</head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<center>
<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
<tr>
<td align="center" valign="top" id="bodyCell">
 
<table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
<tr>
<td align="center" valign="top">
 
<table border="0" cellpadding="0" cellspacing="0" width="100%" id="templatePreheader">
<tr>
<td valign="top" class="preheaderContent" style="padding-top:10px; padding-right:20px; padding-bottom:10px; padding-left:20px;" mc:edit="preheader_content00">
GreenHopping Activation Mail
</td>
 
<td valign="top" width="180" class="preheaderContent" style="padding-top:10px; padding-right:20px; padding-bottom:10px; padding-left:0;" mc:edit="preheader_content01">
Email not displaying correctly?<br/><a href="#" target="_blank">View it in your browser</a>.
</td>
 
</tr>
</table>
 
</td>
</tr>
<tr>
<td align="center" valign="top">
 
<table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
<tr>
<td valign="top" class="headerContent">
<center>
<img src="http://www.self.com/wp-content/uploads/2014/06/GreenHopping-App-700x700.jpg" style="max-width:200px;" style="max-height:200px;" id="headerImage" mc:label="header_image" mc:edit="header_image" mc:allowdesigner mc:allowtext />
</center>
</td>
</tr>
</table>
 
</td>
</tr>
<tr>
<td align="center" valign="top">
 
<table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
<tr>
<td valign="top" class="bodyContent" mc:edit="body_content">
<h1>Welcome to GreenHopping</h1>
<h3>Find Organic Juice Store near you</h3>
Youre receiving this email because your email - <b>{{email}}</b> is used in GreenHopping Registration.
<br/>
<br/>
<h2>Activate your Account</h2>
<h4>And Start Hopping</h4>
Please click on following <b><a href="http://52.91.246.152//activate/{{link}}" target="_blank">link</a></b> to activate your profile.
</td>
</tr>
</table>
 
</td>
</tr>
<tr>
<td align="center" valign="top">
 
<table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateColumns">
<tr mc:repeatable>
<td align="center" valign="top" class="templateColumnContainer" style="padding-top:20px;">
<table border="0" cellpadding="20" cellspacing="0" width="100%">
<tr>
<td class="leftColumnContent">
<img src="http://s27.postimg.org/6tdo0rtnz/image.png" style="max-width:160px;" class="columnImage" mc:label="left_column_image" mc:edit="left_column_image"/>
</td>
</tr>
<tr>
<td valign="top" class="leftColumnContent" mc:edit="left_column_content">
<h3>Juice Press</h3>
<a href="http://kb.mailchimp.com/article/how-do-i-work-with-repeatable-content-blocks" target="_blank">Juice Press</a> are noted with plus and good for health.
<br/>
<br/>
You can go to greenhopping.com to know more about it.
</td>
</tr>
</table>
</td>
<td align="center" valign="top" class="templateColumnContainer" style="padding-top:20px;">
<table border="0" cellpadding="20" cellspacing="0" width="100%">
<tr>
<td class="centerColumnContent">
<img src="http://s4.postimg.org/ycjetqba1/image.png" style="max-width:160px;" class="columnImage" mc:label="center_column_image" mc:edit="center_column_image"/>
</td>
</tr>
<tr>
<td valign="top" class="centerColumnContent" mc:edit="center_column_content">
<h3>Gluten Free</h3>
<a href="a.com" target="_blank">Gluten Free</a> are noted with plus and minus signs so that you can add and subtract content blocks.
<br/>
<br/>
You can go to greenhoppingapp.com to know more about it.
</td>
</tr>
</table>
</td>
<td align="center" valign="top" class="templateColumnContainer" style="padding-top:20px;">
<table border="0" cellpadding="20" cellspacing="0" width="100%">
<tr>
<td class="rightColumnContent">
<img src="http://s9.postimg.org/ag51056u3/image.png" style="max-width:160px;" class="columnImage" mc:label="right_column_image" mc:edit="right_column_image"/>
</td>
</tr>
<tr>
<td valign="top" class="rightColumnContent" mc:edit="right_column_content">
<h3>Glo Mini</h3>
<a href="a.com" target="_blank">Glo Mini</a> are noted with plus and minus signs so that you can add and subtract content blocks.
<br/>
<br/>
You can go to greenhoppingapp.com to know more about it.
</td>
</tr>
</table>
</td>
</tr>
</table>
 
</td>
</tr>
<tr>
<td align="center" valign="top">
 
<table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter">
<tr>
<td valign="top" class="footerContent" mc:edit="footer_content00">
<a href="twitter.com">Follow on Twitter</a>&nbsp;&nbsp;&nbsp;<a href="__FACEBOOK_PROFILEURL__.html">Friend on Facebook</a>&nbsp;&nbsp;&nbsp;<a href="__FORWARD__.html">Forward to Friend</a>&nbsp;
</td>
</tr>
<tr>
<td valign="top" class="footerContent" style="padding-top:0;" mc:edit="footer_content01">
<em>Copyright &copy; 2015 GreenHopping, All rights reserved.</em>
<br/>
<br/>
<strong>Our mailing address is:</strong>
<br/>
US, Some Address at Greenhopping
</td>
</tr>
<tr>
<td valign="top" class="footerContent" style="padding-top:0;" mc:edit="footer_content02">
<a href="__UNSUB__.html">unsubscribe from this list</a>&nbsp;&nbsp;&nbsp;<a href="__UPDATE_PROFILE__.html">update subscription preferences</a>&nbsp;
</td>
</tr>
</table>
 
</td>
</tr>
</table>
 
</td>
</tr>
</table>
</center>
</body>


</html>';
$emails = $email;
$mailHtml = str_replace("{{email}}", $email, $mailHtml);
                $mailHtml = str_replace("{{link}}", $link, $mailHtml);
        $sendgrid = new SendGrid('app42520121@heroku.com', 'w1nkyxq19703');
        $email = new SendGrid\Email();
    $email
    ->addTo($emails)
    ->setFrom('admin@greenhopping.com')
    ->setSubject('GreenHopping Registration')
    ->setHtml($mailHtml);
$sendgrid->send($email);
    }   





       public function sendGridReset($email, $link)
    {
        
        $mailHtml ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">


<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<title>GreenHopping</title>
<script type="text/javascript">
//<![CDATA[
try{if (!window.CloudFlare) {var CloudFlare=[{verbose:0,p:0,byc:0,owlid:"cf",bag2:1,mirage2:0,oracle:0,paths:{cloudflare:"/cdn-cgi/nexp/dok3v=1613a3a185/"},atok:"c50cc9be038eff534235017b430ed633",petok:"05521d0d16aca7c407e6f17a5f2291847a8dd3c5-1449817077-1800",zone:"adbee.technology",rocket:"0",apps:{"ga_key":{"ua":"UA-49262924-2","ga_bs":"2"}},sha2test:0}];!function(a,b){a=document.createElement("script"),b=document.getElementsByTagName("script")[0],a.async=!0,a.src="../ajax.cloudflare.com/cdn-cgi/nexp/dok3v%3d38857570ac/cloudflare.min.js",b.parentNode.insertBefore(a,b)}()}}catch(e){};
//]]>
</script>
<style type="text/css">#outlook a{padding:0;}.ReadMsgBody{width:100%;}.ExternalClass{width:100%;}.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height:100%;}body,table,td,p,a,li,blockquote{-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;}table,td{mso-table-lspace:0pt;mso-table-rspace:0pt;}img{-ms-interpolation-mode:bicubic;}body{margin:0;padding:0;}img{border:0;height:auto;line-height:100%;outline:none;text-decoration:none;}table{border-collapse:collapse!important;}body,#bodyTable,#bodyCell{height:100%!important;margin:0;padding:0;width:100%!important;}#bodyCell{padding:20px;}#templateContainer{width:600px;}body,#bodyTable{background-color:#EEEEEE;}#bodyCell{border-top:4px solid #BBBBBB;}#templateContainer{border:1px solid #BBBBBB;}h1{color:#202020!important;display:block;font-family:Helvetica;font-size:26px;font-style:normal;font-weight:bold;line-height:100%;letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;text-align:left;}h2{color:#404040!important;display:block;font-family:Helvetica;font-size:20px;font-style:normal;font-weight:bold;line-height:100%;letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;text-align:left;}h3{color:#606060!important;display:block;font-family:Helvetica;font-size:16px;font-style:italic;font-weight:normal;line-height:100%;letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;text-align:left;}h4{color:#808080!important;display:block;font-family:Helvetica;font-size:14px;font-style:italic;font-weight:normal;line-height:100%;letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;text-align:left;}#templatePreheader{background-color:#F4F4F4;border-bottom:1px solid #CCCCCC;}.preheaderContent{color:#808080;font-family:Helvetica;font-size:10px;line-height:125%;text-align:left;}.preheaderContent a:link,.preheaderContent a:visited,.preheaderContent a .yshortcuts{color:#606060;font-weight:normal;text-decoration:underline;}#templateHeader{background-color:#F4F4F4;border-bottom:1px solid #CCCCCC;}.headerContent{color:#505050;font-family:Helvetica;font-size:20px;font-weight:bold;line-height:100%;padding-top:0;padding-right:0;padding-bottom:0;padding-left:0;text-align:left;vertical-align:middle;}.headerContent a:link,.headerContent a:visited,.headerContent a .yshortcuts{color:#03a9f4;font-weight:normal;text-decoration:underline;}#headerImage{height:auto;max-width:600px;}#templateBody{background-color:#F4F4F4;border-top:1px solid #FFFFFF;border-bottom:1px solid #CCCCCC;}.bodyContent{color:#505050;font-family:Helvetica;font-size:16px;line-height:150%;padding-top:20px;padding-right:20px;padding-bottom:20px;padding-left:20px;text-align:left;}.bodyContent a:link,.bodyContent a:visited,.bodyContent a .yshortcuts{color:#03a9f4;font-weight:normal;text-decoration:underline;}.bodyContent img{display:inline;height:auto;max-width:560px;}.templateColumnContainer{width:200px;}#templateColumns{background-color:#F4F4F4;border-top:1px solid #FFFFFF;border-bottom:1px solid #CCCCCC;}.leftColumnContent{color:#505050;font-family:Helvetica;font-size:14px;line-height:150%;padding-top:0;padding-right:20px;padding-bottom:20px;padding-left:20px;text-align:left;}.leftColumnContent a:link,.leftColumnContent a:visited,.leftColumnContent a .yshortcuts{color:#03a9f4;font-weight:normal;text-decoration:underline;}.centerColumnContent{color:#505050;font-family:Helvetica;font-size:14px;line-height:150%;padding-top:0;padding-right:20px;padding-bottom:20px;padding-left:20px;text-align:left;}.centerColumnContent a:link,.centerColumnContent a:visited,.centerColumnContent a .yshortcuts{color:#03a9f4;font-weight:normal;text-decoration:underline;}.rightColumnContent{color:#505050;font-family:Helvetica;font-size:14px;line-height:150%;padding-top:0;padding-right:20px;padding-bottom:20px;padding-left:20px;text-align:left;}.rightColumnContent a:link,.rightColumnContent a:visited,.rightColumnContent a .yshortcuts{color:#03a9f4;font-weight:normal;text-decoration:underline;}.leftColumnContent img,.rightColumnContent img{display:inline;height:auto;max-width:260px;}#templateFooter{background-color:#F4F4F4;border-top:1px solid #FFFFFF;}.footerContent{color:#808080;font-family:Helvetica;font-size:10px;line-height:150%;padding-top:20px;padding-right:20px;padding-bottom:20px;padding-left:20px;text-align:left;}.footerContent a:link,.footerContent a:visited,.footerContent a .yshortcuts,.footerContent a span{color:#606060;font-weight:normal;text-decoration:underline;}@media only screen and (max-width: 480px){body,table,td,p,a,li,blockquote{-webkit-text-size-adjust:none!important;}body{width:100%!important;min-width:100%!important;}#bodyCell{padding:10px!important;}#templateContainer{max-width:600px!important;width:100%!important;}h1{font-size:24px!important;line-height:100%!important;}h2{font-size:20px!important;line-height:100%!important;}h3{font-size:18px!important;line-height:100%!important;}h4{font-size:16px!important;line-height:100%!important;}#templatePreheader{display:none!important;}#headerImage{height:auto!important;max-width:600px!important;width:100%!important;}.headerContent{font-size:20px!important;line-height:125%!important;}.bodyContent{font-size:18px!important;line-height:125%!important;}.templateColumnContainer{display:block!important;width:100%!important;}.columnImage{height:auto!important;max-width:480px!important;width:100%!important;}.leftColumnContent{font-size:16px!important;line-height:125%!important;}.centerColumnContent{font-size:16px!important;line-height:125%!important;}.rightColumnContent{font-size:16px!important;line-height:125%!important;}.footerContent{font-size:14px!important;line-height:115%!important;}.footerContent a{display:block!important;}}</style>
</head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<center>
<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
<tr>
<td align="center" valign="top" id="bodyCell">
 
<table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
<tr>
<td align="center" valign="top">
 
<table border="0" cellpadding="0" cellspacing="0" width="100%" id="templatePreheader">
<tr>
<td valign="top" class="preheaderContent" style="padding-top:10px; padding-right:20px; padding-bottom:10px; padding-left:20px;" mc:edit="preheader_content00">
GreenHopping Reset Password EMail
</td>
 
<td valign="top" width="180" class="preheaderContent" style="padding-top:10px; padding-right:20px; padding-bottom:10px; padding-left:0;" mc:edit="preheader_content01">
Email not displaying correctly?<br/><a href="#" target="_blank">View it in your browser</a>.
</td>
 
</tr>
</table>
 
</td>
</tr>
<tr>
<td align="center" valign="top">
 
<table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
<tr>
<td valign="top" class="headerContent">
<center>
<img src="http://www.self.com/wp-content/uploads/2014/06/GreenHopping-App-700x700.jpg" style="max-width:200px;" style="max-height:200px;" id="headerImage" mc:label="header_image" mc:edit="header_image" mc:allowdesigner mc:allowtext />
</center>
</td>
</tr>
</table>
 
</td>
</tr>
<tr>
<td align="center" valign="top">
 
<table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
<tr>
<td valign="top" class="bodyContent" mc:edit="body_content">
<h1>Welcome to GreenHopping</h1>
<h3>Find Organic Juice Store near you</h3>
Youre receiving this email because your email - <b>{{email}}</b> is used in GreenHopping Account
<br/>
<br/>
<h2>Reset your Password</h2>
<h4>And Start Hopping</h4>
Please click on following <b><a href="http://52.91.246.152/reset/{{link}}" target="_blank">link</a></b> to reset your password.
</td>
</tr>
</table>
 
</td>
</tr>
<tr>
<td align="center" valign="top">
 
<table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateColumns">
<tr mc:repeatable>
<td align="center" valign="top" class="templateColumnContainer" style="padding-top:20px;">
<table border="0" cellpadding="20" cellspacing="0" width="100%">
<tr>
<td class="leftColumnContent">
<img src="http://s27.postimg.org/6tdo0rtnz/image.png" style="max-width:160px;" class="columnImage" mc:label="left_column_image" mc:edit="left_column_image"/>
</td>
</tr>
<tr>
<td valign="top" class="leftColumnContent" mc:edit="left_column_content">
<h3>Juice Press</h3>
<a href="http://kb.mailchimp.com/article/how-do-i-work-with-repeatable-content-blocks" target="_blank">Juice Press</a> are noted with plus and good for health.
<br/>
<br/>
You can go to greenhoppingapp.com to know more about it.
</td>
</tr>
</table>
</td>
<td align="center" valign="top" class="templateColumnContainer" style="padding-top:20px;">
<table border="0" cellpadding="20" cellspacing="0" width="100%">
<tr>
<td class="centerColumnContent">
<img src="http://s4.postimg.org/ycjetqba1/image.png" style="max-width:160px;" class="columnImage" mc:label="center_column_image" mc:edit="center_column_image"/>
</td>
</tr>
<tr>
<td valign="top" class="centerColumnContent" mc:edit="center_column_content">
<h3>Gluten Free</h3>
<a href="a.com" target="_blank">Gluten Free</a> are noted with plus and minus signs so that you can add and subtract content blocks.
<br/>
<br/>
You can go to greenhoppingapp.com to know more about it.
</td>
</tr>
</table>
</td>
<td align="center" valign="top" class="templateColumnContainer" style="padding-top:20px;">
<table border="0" cellpadding="20" cellspacing="0" width="100%">
<tr>
<td class="rightColumnContent">
<img src="http://s9.postimg.org/ag51056u3/image.png" style="max-width:160px;" class="columnImage" mc:label="right_column_image" mc:edit="right_column_image"/>
</td>
</tr>
<tr>
<td valign="top" class="rightColumnContent" mc:edit="right_column_content">
<h3>Glo Mini</h3>
<a href="a.com" target="_blank">Glo Mini</a> are noted with plus and minus signs so that you can add and subtract content blocks.
<br/>
<br/>
You can go to greenhoppingapp.com to know more about it.
</td>
</tr>
</table>
</td>
</tr>
</table>
 
</td>
</tr>
<tr>
<td align="center" valign="top">
 
<table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter">
<tr>
<td valign="top" class="footerContent" mc:edit="footer_content00">
<a href="twitter.com">Follow on Twitter</a>&nbsp;&nbsp;&nbsp;<a href="__FACEBOOK_PROFILEURL__.html">Friend on Facebook</a>&nbsp;&nbsp;&nbsp;<a href="__FORWARD__.html">Forward to Friend</a>&nbsp;
</td>
</tr>
<tr>
<td valign="top" class="footerContent" style="padding-top:0;" mc:edit="footer_content01">
<em>Copyright &copy; 2015 GreenHopping, All rights reserved.</em>
<br/>
<br/>
<strong>Our mailing address is:</strong>
<br/>
US, Some Address at Greenhopping
</td>
</tr>
<tr>
<td valign="top" class="footerContent" style="padding-top:0;" mc:edit="footer_content02">
<a href="__UNSUB__.html">unsubscribe from this list</a>&nbsp;&nbsp;&nbsp;<a href="__UPDATE_PROFILE__.html">update subscription preferences</a>&nbsp;
</td>
</tr>
</table>
 
</td>
</tr>
</table>
 
</td>
</tr>
</table>
</center>
</body>


</html>';
$emails = $email;
$mailHtml = str_replace("{{email}}", $email, $mailHtml);
                $mailHtml = str_replace("{{link}}", $link, $mailHtml);
        $sendgrid = new SendGrid('app42520121@heroku.com', 'w1nkyxq19703');
        $email = new SendGrid\Email();
    $email
    ->addTo($emails)
    ->setFrom('admin@greenhopping.com')
    ->setSubject('GreenHopping Reset Email')
    ->setHtml($mailHtml);
$sendgrid->send($email);
    }
    
    
        public function getPlatform() {
        $user_agent = $_SERVER['HTTP_USER_AGENT'];


        $os_platform = "Unknown OS Platform";

        $os_array = array(
            '/windows nt 10/i' => 'Windows 10',
            '/windows nt 6.3/i' => 'Windows 8.1',
            '/windows nt 6.2/i' => 'Windows 8',
            '/windows nt 6.1/i' => 'Windows 7',
            '/windows nt 6.0/i' => 'Windows Vista',
            '/windows nt 5.2/i' => 'Windows Server 2003/XP x64',
            '/windows nt 5.1/i' => 'Windows XP',
            '/windows xp/i' => 'Windows XP',
            '/windows nt 5.0/i' => 'Windows 2000',
            '/windows me/i' => 'Windows ME',
            '/win98/i' => 'Windows 98',
            '/win95/i' => 'Windows 95',
            '/win16/i' => 'Windows 3.11',
            '/macintosh|mac os x/i' => 'Mac OS X',
            '/mac_powerpc/i' => 'Mac OS 9',
            '/linux/i' => 'Linux',
            '/ubuntu/i' => 'Ubuntu',
            '/iphone/i' => 'iPhone',
            '/ipod/i' => 'iPod',
            '/ipad/i' => 'iPad',
            '/android/i' => 'Android',
            '/blackberry/i' => 'BlackBerry',
            '/webos/i' => 'Mobile'
        );

        foreach ($os_array as $regex => $value) {

            if (preg_match($regex, $user_agent)) {
                $os_platform = $value;
            }
        }

        return $os_platform;
    }
  public function activate($id)
    {


        $userData = User::where('active_link', $id)->first();
        
        if ($userData['status']==0) 
        {
                    $msg = '<span class="alert alert-success fade in">

<i class="fa fa-check-circle fa-fw fa-lg"></i>
<strong>Well done!</strong> You are successfully verified your mail.
</span>';    
            $updateData['status'] = 1;
            User::where('active_link', '=', $id)->update($updateData);
        }
        else if ($userData['status']==1) {
        $msg = '<span class="alert alert-danger fade in">
        
        <i class="fa fa-times-circle fa-fw fa-lg"></i>
        <strong>Oops  !</strong> Your mail is already verified.
        </span>';    
        }

        if ($userData=='') {
         

         $msg = '<span class="alert alert-danger fade in">
        
        <i class="fa fa-times-circle fa-fw fa-lg"></i>
        <strong>Oops  !</strong> Invalid User Activation Link.
        </span>';    

        }
        
       # return $userData;
$city = City::where('status', 1)->get();
        return View::make('user.activate')->with('cities', $city)->with('msg', $msg); 
    }

    public function resetEmail()
    {
        if (User::where('email', '=', Input::get('email'))->exists()) { 
            $userData['forget_link'] = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 50);
            $this->sendGridReset(Input::get('email'), $userData['forget_link']);
            User::where('email', Input::get('email'))->update($userData);
            $Response = array('success' => '1');
        } else {
            $Response = array('success' => '0', 'error' => 'User Not Found');
        }
        return $Response;
    }

    public function reset($id)
    {
        if (User::where('forget_link', '=', $id)->exists()) {
        $userData = User::where('forget_link', $id)->first();
        $updateData['forget_link_token'] = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 50);
        $updateData['forget_link'] = '';
        User::where('forget_link_token', '=', $id)->update($updateData);
        $city = City::where('status', 1)->get();
        return View::make('user.reset')->with('valid',1)->with('token',$userData['forget_link_token'])->with('msg',1)->with('cities',$city);
        }
        else
        {
        $city = City::where('status', 1)->get();
        return View::make('user.reset')->with('valid',0)->with('msg',1)->with('cities',$city);
        }
    }

     public function updatePassword()
    {

        if (User::where('forget_link_token', '=', Input::get('token'))->exists()) {
        $userData = User::where('forget_link_token', Input::get('token'))->first();
        $password = Hash::make(Input::get('password'));
        $updateData['password'] = $password;
        $updateData['forget_link_token'] = '';
        $updateData['forget_link'] = '';
        User::where('forget_link_token', '=', Input::get('token'))->update($updateData);
        $Response = array('success' => '1');
        }
        else
        {
        $Response = array('success' => '0');
        }
        return $Response;
            
    }

    public function updateProfileImage()
    {
        $updateData['image'] = Input::get('image');
        User::where('id', '=', Input::get('id'))->update($updateData);
        $Response = array('success' => '1');
        return $Response;
            
    }

    
    public function changePasswordProcess() {
        $PasswordData = Input::all();

        Validator::extend('pwdvalidation', function($field, $value, $parameters) {
            return Hash::check($value, Auth::user()->password);
        });

        $messages = array('pwdvalidation' => 'The Old Password is Incorrect');

        $validator = Validator::make($PasswordData, Admin::$rulespwd, $messages);
        if ($validator->passes()) {
            $user = Admin::find(Auth::user()->id);
            $user->password = Input::get('NewPassword');
            $user->save();
            $Response = array('success' => '1');
        } else {
            $Response = array('success' => '0','error' => $validator->messages());
        }
        return $Response;
    }   

    public function likeStore()
    {

        $likeStatus = StoreLike::where('store_id', Input::get('store_id'))->where('user_id', Input::get('user_id'))->first();
        
        if($likeStatus)
        {
            $deletedRows = StoreLike::where('store_id', Input::get('store_id'))->where('user_id', Input::get('user_id'))->delete();
            $totalLike = StoreLike::where('store_id', Input::get('store_id'))->count();
            $Response = array('success' => '2','totalLike' => $totalLike);
        }
        else
        {   
            $user = new StoreLike;
            $user->user_id = Input::get('user_id');
            $user->store_id = Input::get('store_id');
            $user->save();
            $totalLike = StoreLike::where('store_id', Input::get('store_id'))->count();
            $Response = array('success' => '1', 'totalLike' => $totalLike);
        }
            return $Response;
    }
    
}