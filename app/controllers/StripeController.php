<?php

class StripeController extends Controller {

    public function addCard()
    {
        $userData['stripe_token'] = Input::get('token');
        $userData['stripe_card'] = Input::get('card');
        $user = User::where('id',Input::get('user_id'))->first();
        $customer = \Stripe\Customer::retrieve($user->stripe_id);
        $c = $customer->sources->create(array("source" =>Input::get('token')));
        User::where('id', Input::get('user_id'))->update($userData);
        $Response = array('success' => '1');
        return $Response;
    }

    public function deleteAllCard()
    {

        $userData = User::where('id',Input::get('user_id'))->first();
        $customer = \Stripe\Customer::retrieve($userData['stripe_id']);
        $customer->sources->retrieve($userData['stripe_card'])->delete();

        $inputData['stripe_token'] = Input::get('token');
        $inputData['stripe_card'] = Input::get('card');
        User::where('id', Input::get('user_id'))->update($inputData);

        $Response = array('success' => '1');
        return $Response;
    }

    public function deleteCards()
    {

        $userData = User::where('id',Input::get('id'))->first();
        $customer = \Stripe\Customer::retrieve($userData['stripe_id']);
        $customer->sources->retrieve($userData['stripe_card'])->delete();
        
        $inputData['stripe_token'] = '';
        $inputData['stripe_card'] = '';
        User::where('id', Input::get('id'))->update($inputData);

        $Response = array('success' => '1');
        return $Response;
    }

    

    public function deleteCard($customer,$card)
    {
        $customer = \Stripe\Customer::retrieve($customer);
        $customer->sources->retrieve($card)->delete();
    }
    public function listCard()
    {
        $userData = User::where('id', Input::get('id'))->first();
        $allCard = \Stripe\Customer::retrieve($userData['stripe_id'])->sources->all(array("object" => "card"));
        $Response = array('success' => '1', 'card' => $allCard);
        return $Response;
    }


	
}