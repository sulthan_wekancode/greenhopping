@extends('layout.public-search')
@section('content')

<?php
$one = 'sup';
?>



<script src="public/asset/jquery.min.js"></script>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDKw1I9ZlI-piCBp2zXSuviBDVRjju-aYI&sensor=true"></script>

<div class="col-lg-7">
<div id="map" class="col-lg-12" style="height: 575px;
    
    display: inline-block;"></div>
</div>
<!--
in body
style="background-color:black"
-->


<span class="col-lg-5" style="float:right;background-color:white;">
<div class="main-box clearfix">
<header class="main-box-header clearfix">
<h2>{{ $cityName->name }} <i class="fa fa-bars"></i> {{ count($results)  }} Results</h2> 
</header>
<div class="main-box-body clearfix">
<table class="table table-condensed table-hover">
<thead>
<tr>
<th></th>
<th></th>
<th></th>
</tr>
</thead>
<tbody>
@foreach($results as $result)
<tr>
<td>
<span class="img">
<a href="{{ URL::to('store/').'/'.$result->id }}" style="text-decoration:none">
<img src="{{ $result->profile_image }}" alt="" style="height:50px;width:50px">
{{ $result->name }}</a>

</span>

<i class="fa fa-heart"></i> 	 4.2
</td>
<td class="text-center">
{{ $result->address }}
</td>
<td class="text-center status green">
<i class="fa fa-level-up"></i> 	100 % Pure Veg
</td>
</tr>
@endforeach
</tbody>
</table>


</div>
</div>
</span>

<script>

$(document).ready(function() {
console.log('start');
  var locations = [
  	  <?php foreach($results as $result){
  	  	
  	  	$label = $result->name;

  	  	echo "['".$result->name."',".$result->latitude.",".$result->longitude."],";
  	  ?>
  	  <?php } ?>
      
    ];
    
    // Setup the different icons and shadows
    var iconURLPrefix = 'http://maps.google.com/mapfiles/ms/icons/';
    
    var icons = ['public/img/gmarker.png']
    var iconsLength = icons.length;

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 14,
      center: new google.maps.LatLng(-37.92, 151.25),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      streetViewControl: false,
      panControl: false,
      zoomControlOptions: {
         position: google.maps.ControlPosition.LEFT_BOTTOM
      }
    });

    var infowindow = new google.maps.InfoWindow({
      maxWidth: 160
    });

    var markers = new Array();
    
    var iconCounter = 0;
    
    // Add the markers and infowindows to the map
    for (var i = 0; i < locations.length; i++) {  
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map,
        icon: icons[iconCounter]
      });

      markers.push(marker);

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
      
      iconCounter++;
      // We only have a limited number of possible icon colors, so we may have to restart the counter
      if(iconCounter >= iconsLength) {
        iconCounter = 0;
      }
    }

    function autoCenter() {
      //  Create a new viewpoint bound
      var bounds = new google.maps.LatLngBounds();
      //  Go through each...
      for (var i = 0; i < markers.length; i++) {  
        bounds.extend(markers[i].position);
      }
      //  Fit these bounds to the map
      map.fitBounds(bounds);
    }
    

    autoCenter();

    });
</script>
@stop