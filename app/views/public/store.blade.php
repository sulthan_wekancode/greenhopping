@extends('layout.public')
@section('content')

<input type="hidden" value="1" name="userId" id="userId">
<input type="hidden" value="{{$store->id}}" name="storeId" id="storeId">


<div style="position: relative; background: url({{ $store->cover_image }}); width: 100%; height: 280px" >

<div style="position: absolute; bottom: 0; left: 0.5em; width: 400px; font-weight: bold; color: #fff;">
<p><b>{{ $store->name }}</b></p>
<p>{{ $store->address }}</p>
<p><i class="fa fa-clock-o"></i>10 am - 10pm <span class="fa fa-phone" style='color:green'></span>  ({{ $store->phone_code.')'.$store->mobile }} <span style='color:green'><a href="http://{{ $store->website }}" target="_new" style="color:green">{{ $store->website }}</a></span></p>
</div>

<p style="position: absolute; bottom: 0; right: 2em; width: 120px; padding: 4px; font-weight: bold;">
<span style='color:red;background-color:black'>
<font color="white">
{{ $store->category }}
</font>
</span>
</p>

</div>



<span >


<div id="content-wrapper" style="background-color:white;margin:0px 0px 0px 0px">
<?php
$cat = Category::where('store_id', $store->id)->get();
$juice = Juice::where('store_id', $store->id)->get();
?>

<div class="col-lg-3" style="border">
<font size='6px'>Category</font><br><br>
@foreach($cat as $category)
	<span class='cl' data-gen='{{ $category->id}}'> <font size='3px'>{{ $category->name }}</font></span> <br><br>
@endforeach
</div>
<div class="col-lg-4" style="border">
<ul class="widget-todo">
@foreach($juice as $j)
<li class="jl jl_{{ $j->category_id }} clearfix" style="display:none">
<div class="name">
<label for="todo-2">
<img src="../public/img/samples/img_8.jpg" height="30px" width="30px">
<font size="3px">
{{ $j->name }}
</font>
</label>

</div>
<div class="actions">
<i class="less-cart fa fa-minus" data-gen='{{ $j->id }}'></i>
<span id='va_{{$j->id}}'>
0
</span>
<i class="add-cart fa fa-plus" data-gen='{{ $j->id }}'></i>
</div>
</li>
@endforeach
</ul>
</div>


</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script>
$(document).ready(function() {

$(document).on('click','.cl',function(){
   $('.jl').hide().filter('.jl_' + $(this).data('gen') ).show();
})

$(document).on('click','.add-cart',function(){
   var val = $(this).data('gen');
   var value = parseInt($('#va_'+val).html());
   if(value>=0)
   {
   	if(value>=51)
   	{
   		alert('Cant')
   		return false;
   	}
   var newValue = value+1;
   $('#va_'+val).html(newValue);
   var value = parseInt($('.bag').html());
   var newValue = value+1;
   $('.bag').html(newValue);

updateCartInfo(newValue);
   	

    }
})

$(document).on('click','.less-cart',function(){
  var val = $(this).data('gen');
  var value = parseInt($('#va_'+val).html());
  if(value>=1)
   {
  var newValue = value-1;
  $('#va_'+val).html(newValue);
  var value = parseInt($('.bag').html());
   var newValue = value-1;
   $('.bag').html(newValue);
   updateCartInfo(newValue);
}
})

function updateCartInfo(newValue)
{
	var userId = $('#userId').val();
	var storeId = $('#storeId').val();
	console.log(newValue);
	$.ajax({
                method: "POST",
                url: "../updateCart",
                data: {
                    
                    userId : userId,
                    storeId : storeId,
                    juiceId : newValue
                }
            })
            .done(function(msg) {
                if(msg.success==1)
                {
                    console.log('ok');
                }
                else
                {
                 console.log('not ok');
                    
                }
            });
}

$('.cl').first().click();
})
</script>

@stop