@extends('public.index')
@section('content')


<script>
$(document).ready(function() {
localStorage.setItem("ghUserId", {{ Auth::user()->id }});
	});

</script>

<script src="public/asset/userProfile.js"></script>
<div id="page-wrapper" class="container nav-small">
<div class="row">

  
    <link href="public/asset/style.css" rel="stylesheet">
	
	<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDKw1I9ZlI-piCBp2zXSuviBDVRjju-aYI&sensor=true"></script>
	
    <script src="public/asset/where.js"></script>
	
  
<div class="col-lg-6" id="googlemaps" style='width:25%;'>
</div>
<div class="col-lg-9" style='float:right;background:white'>
<div class="row">
<div class="col-lg-12">

<br>
<h1>Personal Information</h1>
</div>
</div>
<div class="row" id="user-profile">

<div class="col-lg-4 col-md-4 col-sm-4">
<div class="main-box clearfix">
<header class="main-box-header clearfix">
<h2 class="ghUserName" style="display:none"></h2>
</header>
<?php
if(Auth::user()->facebook_id !='' || Auth::user()->google_id !='')
{
?>
<script>
$(document).ready(function() {
var fbImg = localStorage.getItem("fbImg");
$("#fbImg").attr("src",fbImg);
});
</script>
<div class="main-box-body clearfix">
<img src="" alt="" class="profile-img img-responsive center-block" id="fbImg" style="height:283px">
<form id="newAddressNew" name="newAddressNew">
<span id='profileUpdatedSuccess'></span>
<div class="profile-message-btn center-block text-center">
<div class="form-group">
<input type="text" class="form-control" id="fullNames" name="fullNames" placeholder="Your Full Name" maxlength="30"  pattern="[a-zA-Z0-9!@#$%^*_|]{0,100}" value="{{ Auth::user()->username }}" disabled="disabled">
<span id='fullNamesValidate'></span>
</div>
<div class="form-group">
<input type="text" class="form-control" id="ghUserEmail" placeholder="email" disabled value="{{ Auth::user()->email }}">
</div>
<span id='availableCity'></span><br>
<a class="btn btn-success" id="updateProfileData">
Update Profile
</a>
</div>
</div>
</div>
</div>
</form>
<?php
}
else
{
?>
<div class="main-box-body clearfix">
<?php
if(Auth::user()->image=='')
{
?>
<img src="http://s3.postimg.org/x9oygwioz/profile.png" alt="" class="profile-img img-responsive center-block" id='cat_covers'>
<?php
}
else
{
?>
<img src="{{Auth::user()->image}}" alt="" class="profile-img img-responsive center-block" id='cat_covers'>
<?php
}
?>






<script src="{{ asset('/').('public/asset/ajaxupload.3.5.js') }}"></script>
<script src="{{ asset('/').('public/asset/s3_user_profile.js') }}"></script>

<a href="javascript:;" id="cat_pic" style="font-size:15px; font-weight:bold; padding:3px; text-decoration:none">Browse Files & Upload</a> 
<input type="hidden" name="cat_cover" value="" id="cat_cover">
<input type="hidden" value="" id="cat_image" name="cat_image">
<span id="statuss"></span>
<div id="cat_cover"></div>


<form id="newAddressNew" name="newAddressNew">
<span id='profileUpdatedSuccess'></span>
<div class="profile-message-btn center-block text-center">
<div class="form-group">
<input type="text" class="form-control" id="fullNames" name="fullNames" placeholder="Your Full Name" maxlength="30"  pattern="[a-zA-Z0-9!@#$%^*_|]{0,100}" value="{{ Auth::user()->username }}">
<span id='fullNamesValidate'></span>
</div>
<div class="form-group">
<input type="text" class="form-control" id="ghUserEmail" placeholder="email" disabled value="{{ Auth::user()->email }}">
</div>
<span id='availableCity'></span><br>
<a class="btn btn-success" id="updateProfileData">
Update Profile
</a>
</div>
</div>
</div>
</div>
</form>
<?php
}
?>


<div class="col-lg-8 col-md-8 col-sm-8">
<div class="main-box clearfix">
<div class="tabs-wrapper profile-tabs">
<ul class="nav nav-tabs">
<li class="active"><a id='showAddress' data-toggle="tab">Address</a></li>
<li><a  id='showCards' data-toggle="tab">Cards</a></li>
<li><a  id='showOrders' data-toggle="tab">Orders</a></li>
</ul>
<span id='profileUpdatedSuccessEdit'></span>
<div class="tab-content" id="addAdr">
<img src="public/img/plus-4-xxl.png" class='myMenuLink' style="float:right;margin-top:-11px;height:30px;width:30px">
<br><br>

<div class="tab-pane fade in active" id="showAddressArea">
<span id='userAddressData'></span>
<div class="main-box">
<header class="main-box-header clearfix">
<h2 id='addressCapChanger'>Add a new address</h2>

</header>

<div class="main-box-body clearfix">

<form name="newAddress" id="newAddress">
<div class="form-group">
<input type="text" class="form-control" id="fullName" name="fullName" placeholder="Your Full Name" maxlength="30">
<span id='fullNameValidate'></span>
</div>
<div class="form-group">
<input type="text" class="form-control" id="addressVal" name="addressVal" placeholder="Address Line 1" maxlength="40">
<span id='addressValValidate'></span>
</div>
<div class="form-group">
<input type="text" class="form-control" id="city" name="city" placeholder="Town / City" maxlength="20">
<span id='cityValidate'></span>
</div>
<div class="form-group">
<input type="text" class="form-control" id="state" name="state" placeholder ="State" maxlength="25">
<span id='stateValidate'></span>
</div>
<div class="form-group">
<input type="text" class="form-control" id="pincode" name="pincode" placeholder="Zip Code" maxlength="25">
<span id='pincodeValidate'></span>
</div>
<div class="form-group">
<input type="text" class="form-control" id="country" name="country" placeholder="Country" maxlength="25">
<span id='countryValidate'></span>
</div>
<a class="DefaultupdateAddress btn btn-success" id='DefaultupdateAddress'>
Deliver to this Address</a> 
<a class="updateAddress btn btn-success" id="updateAddress">Add Address</a> 
<input type="hidden" id="addr_id" value="">
<a class="updateAddressItem btn btn-success" id="" style="display:none">Update Address</a> 
</form>
</div>
</div>
</div>



<div class="tab-pane fade in active" id="showCardsArea" style='display:none'>


<div class="table-responsive">



<div class="col-lg-12">
<div class="main-box no-header clearfix">
<div class="main-box-body clearfix">
<div class="table-responsive">
<table class="table user-list table-hover">
<thead>
<tr>
<th><span>Card Details</span></th>
<th><span>Created</span></th>
<th class="text-center"><span>Status</span></th>
<th>&nbsp;</th>
</tr>
</thead>
<tbody>
<tr style="display:none">
<td>


<span class="user-subhead">
xxxx-xxxx-xxxx-5643
<br>
01/2020
</span>
</td>
<td>
2013/08/08
</td>
<td class="text-center">
<span class="label label-success">Default</span>
</td>

<td style="width: 20%;">

<a class="table-link">
<span class="fa-stack" style='display:none'>

</span>
</a>
<a class="table-link danger">
<span class="fa-stack">
<i class="fa fa-square fa-stack-2x"></i>
<i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
</span>
</a>
</td>
</tr>
<tr>
<td>

<span class="user-subhead">
xxxx-xxxx-xxxx-5643
<br>
01/2020

</td>
<td>
2013/08/12
</td>
<td class="text-center">
<span class="label label-default" style='background-color:green'>Active</span>
</td>
<td style="width: 20%;">

<a href="#" class="table-link">
<span class="fa-stack">

</span>
</a>
<a href="#" class="table-link danger">
<span class="fa-stack">

</span>
</a>
</td>
</tr>


</tbody>
</table>
</div>
</div>
</div>
</div>

</div>
</div>
</div>


<div class="tab-pane fade in active" id="showOrdersArea" style='display:none'>

<table class="table footable toggle-circle-filled" data-page-size="6" data-filter="#filter" data-filter-text-only="true">
<thead>
<tr>
<th>Order ID</th>
<th>Date</th>
<th data-hide="phone">Store Name</th>
<th data-hide="phone,tablet" class="text-center">Status</th>
<th data-hide="all" class="text-right">Order Summary</th>
</tr>
</thead>
<tbody>
<tr>
<td>
#8000
</td>
<td>
2013/08/08
</td>
<td>
Wil De Store
</td>
<td class="text-center">
<span class="label label-success">Completed</span>
</td>
<td class="text-right">
&dollar; 25.50
</td>
</tr>
<tr>
<td>
#5832
</td>
<td>
2013/08/08
</td>
<td>
John Wayne Store
</td>
<td class="text-center">
<span class="label label-success">Completed</span>
</td>
<td class="text-right">
&dollar; 30.00
</td>
</tr>
<tr>
<td>
#2547
</td>
<td>
2013/08/08
</td>
<td>
Anthony Organics
</td>
<td class="text-center">
<span class="label label-success">Completed</span>
</td>
<td class="text-right">
&dollar; 15.50
</td>
</tr>
<tr>
<td>
#9274
</td>
<td>
2013/08/08
</td>
<td>
Charles Chaplin
</td>
<td class="text-center">
<span class="label label-danger">Cancelled</span>
</td>
<td class="text-right">
&dollar; 35.34
</td>
</tr>
<tr>
<td>
#8463
</td>
<td>
2013/08/08
</td>
<td>
Orgnic Cooper
</td>
<td class="text-center">
<span class="label label-success">Completed</span>
</td>
<td class="text-right">
&dollar; 30.50
</td>
</tr>


</tbody>
</table>



<a href="#" class="btn btn-success pull-right">View all Orders</a>
</div>


</div>

</div>


</div>

</div>
</div>
</div>



</div>
</div>




@stop