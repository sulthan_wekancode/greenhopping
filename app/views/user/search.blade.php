@extends('user.layout.index')
@section('content')

<script src="{{ asset('/').('public/js/jquery.dataTables.min.js') }}"></script>

<?php
if(isset($_GET['query']))
{
?>
<script>
$(document).ready(function() {
$('#query').val("<?php echo $_GET['query']?>");  
});
</script>
<?php
}
?>
<?php
if(isset($_GET['location']))
{
?>
<script>
$(document).ready(function() {
	
$("#location").val("<?php echo $_GET['location']; ?>").change();
});
</script>
<?php
}
?>

<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDKw1I9ZlI-piCBp2zXSuviBDVRjju-aYI&sensor=true"></script>

<div class="col-md-7" style="padding:0px 0px 0px 0px">
<div id="map" class="col-lg-12" style="height: 575px;
    
    display: inline-block;"></div>
</div>
<!--
in body
style="background-color:black"
-->


<div class="col-md-5" style="float:right;background-color:white;">

<!--start tab-->

<div class="row">
<div class="col-lg-12">
<div class="main-box clearfix">

<div class="main-box-body clearfix">
<div class="tabs-wrapper">
<ul class="nav nav-tabs nav-justified semibold">
<li class="active"><a href="#searchresult" data-toggle="tab">Search Result</a></li>
<li><a href="#recommendation" data-toggle="tab">Recommendation</a></li>
</ul>
<div class="tab-content np">
<div class="tab-pane fade in active" id="searchresult">

	
<div class="row">
<div class="col-lg-12">
<div class="main-box clearfix">
<header class="main-box-header clearfix pt10 pb10">
<div class="filter-block pull-left">

<h5 class="pull-left semibold mtonly10">  {{ $cityName->name }} </h5> 
<img src="./public/static/menu-icon.jpg" style="height: 10px;padding-left:20px">

<span class="fs12 pl5 text-muted">
<span id="resultCountMain"></span> Results
</span>
<span class="fs12 ml70 text-muted">Sort by
<select class="nb" id="sortBy">
  <option value="prox">Proximity</option>
  <option value="popu">Popularity</option>
</select>
</span>



</div>
</header>
<div class="main-box-body clearfix">
<div class="table-responsive">
<table class="table table-hover searchtable"  id="dataTables-example" >
<thead>
            <tr>
                <th style="display:none">Distance</th>
                <th style="display:none">Popular</th>
                <th style="display:none">Name</th>
                <th style="display:none">Position</th>
                <th style="display:none">Office</th>
                <th style="display:none">Age</th>
                
            </tr>
        </thead>
<tbody>

<?php $cts = 0; ?>
@foreach($results as $result)
<?php 
$cat = Category::where('store_id', $result->id)->where('status', 1)->first();
if(count($cat)==0)
{  goto end; }
$juice = Juice::where('category_id', $cat->id)->where('status', 1)->first();
if(count($juice)==0)
{  goto end; }
$cts = $cts+1;
$type = Type::where('id', $result->type)->first();
$totalLike = StoreLike::where('store_id', $result->id)->count();
?>
<tr>
<td style="display: none;">
{{ $result->distance }}
</td>
<td style="display: none;">
{{ $result->likes }}
</td>
<td width="5%">
	<img src="{{ $result->profile_image }}" alt="image"  height="58px" width="58px" style="margin: -8px 0px -9px -7px;" />
</td>
<td width="25%">
<div class="semibold">
<a href="{{ URL::to('store/').'/'.$result->id }}" style="text-decoration:none;color:black">
{{ $result->name }}  
</a>
</div>
<div class="text-muted"><i class="fa fa-heart yellow"></i> {{ $totalLike }}</div>
</td>
<td width="35%">
<div class="text-muted">
<a href="{{ URL::to('store/').'/'.$result->id }}" style="text-decoration:none;color:black">
{{ $result->address }}
</a>
</div>
</td>
<td width="35%" class="text-right">
<?php
$cat = StoreCategory::where('store_id', $result->id)->get(['category_id']);
?>
@foreach($cat as $imgCat)
<?php 
$catImg = Category::where('id', $imgCat->category_id)->pluck('image');
?>
<a href="{{ URL::to('store/').'/'.$result->id }}" style="text-decoration:none;color:black">
<img src="{{ $catImg }}" alt="image" height="20px" width="20px" />
</a>
@endforeach
</td>
</tr>
<?php end: ?>
@endforeach
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>


</div>
<div class="tab-pane fade" id="recommendation">
<div class="row">
<div class="col-lg-12">
<div class="main-box clearfix">
<header class="main-box-header clearfix pt10 pb10">
<div class="filter-block pull-left">

<h5 class="pull-left semibold mtonly10">  {{ $cityName->name }} </h5> 
<img src="./public/static/menu-icon.jpg" style="height: 10px;padding-left:20px">

<span class="fs12 pl5 text-muted">
<span id="resultCountMain"></span> Results
</span>




</div>
</header>
<div class="main-box-body clearfix">
<div class="table-responsive">
<table class="table table-hover searchtable"  id="dataTables-example" >
<thead>
            <tr>
                <th style="display:none">Name</th>
                <th style="display:none">Position</th>
                <th style="display:none">Office</th>
                <th style="display:none">Age</th>
                
            </tr>
        </thead>
<tbody>

<?php $ctr = 0; ?>
@foreach($results_like as $result)
<?php 

$cat = Category::where('store_id', $result->id)->where('status', 1)->first();
if(count($cat)==0)
{  goto ends; }
$juice = Juice::where('category_id', $cat->id)->where('status', 1)->first();
if(count($juice)==0)
{  goto ends; }

if (Auth::check())
{
  $like = UserLike::where('user_id', Auth::user()->id)->where('store_id', $result->id)->first();  
}
else
{
  $like = UserLike::where('user_id', 0)->where('store_id', $result->id)->first();  
}

if(count($like)==0)
{  goto ends; }
$ctr = $ctr+1;
$type = Type::where('id', $result->type)->first();
$totalLike = StoreLike::where('store_id', $result->id)->count();
?>
<tr>
<td width="5%">

  <img src="{{ $result->profile_image }}" alt="image"  height="58px" width="58px" style="margin: -8px 0px -9px -7px;" />

</td>
<td width="40%">
<div class="semibold">
<a href="{{ URL::to('store/').'/'.$result->id }}" style="text-decoration:none;color:black">
{{ $result->name }}
</a>
</div>
<div class="text-muted"><i class="fa fa-heart yellow"></i> {{ $totalLike }}</div>
</td>
<td width="40%">
<div class="text-muted">
<a href="{{ URL::to('store/').'/'.$result->id }}" style="text-decoration:none;color:black">
{{ $result->address }}
</a>
</div>
</td>
<td width="15%">
<?php
$cat = StoreCategory::where('store_id', $result->id)->get(['category_id']);
?>
@foreach($cat as $imgCat)
<?php 
$catImg = Category::where('id', $imgCat->category_id)->pluck('image');
?>
<a href="{{ URL::to('store/').'/'.$result->id }}" style="text-decoration:none;color:black">
<img src="{{ $catImg }}" alt="image" height="20px" width="20px" />
</a>
@endforeach
</td>

</tr>
<?php ends: ?>
@endforeach
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<!--end tab-->


</div>

<script>
$(document).ready(function() {

$('#resultCountMain').html(<?php echo $cts?>);  
$('#resultCountRec').html(<?php echo $ctr?>);  
console.log('start');
  var locations = [
      <?php foreach($results as $result){
        $cat = Category::where('store_id', $result->id)->where('status', 1)->first();
if(count($cat)==0)
{  goto end_lat; }
$juice = Juice::where('category_id', $cat->id)->where('status', 1)->first();
if(count($juice)==0)
{  goto end_lat; }


        $name = $result->name;
        $address = str_replace(","," ",$result->address);
        $totalLike = StoreLike::where('store_id', $result->id)->count();
        $rate = '<img src="./public/static/yellow-icon.png"> '.$totalLike;
        $label = '<b>'.$result->name.'</b><br>'.$address.'<br>'.$rate;
        //$label = '<div class="col-lg-5 "> <a href="http://localhost/greenhopping/store/60" style="text-decoration:none"> <img src="http://greenhoppingbucket.s3.amazonaws.com/store/profile/1454605663.png" height="58px" width="58px"> </a><h2>kfc </h2> <img src="./public/static/yellow-icon.png"> 0</div>';
        $label = '<style>th {font-weight: normal;}</style><table style="height:70px;width:auto;"><thead>  <tr>    <th><img src="'.$result->profile_image.'" height="58px" width="58px"></th>    <th>'.$label.'  </th>  </tr></thead></table><span style="float:right"> <a href="store/'.$result->id.'" style="text-decoration:none;color:#0F752B">View Store</a></span>';
        echo "['".$label."',".$result->latitude.",".$result->longitude."],";
      ?>
      <?php 
end_lat:
    } ?>
      
    ];
    
    // Setup the different icons and shadows
    var iconURLPrefix = 'http://maps.google.com/mapfiles/ms/icons/';
    
    var icons = ['public/img/gmarker.png']
    var iconsLength = icons.length;

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 8,
      center: new google.maps.LatLng(-37.92, 151.25),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      streetViewControl: false,
      panControl: false,
      zoomControlOptions: {
         position: google.maps.ControlPosition.LEFT_BOTTOM
      }
    });

    var infowindow = new google.maps.InfoWindow({
      maxWidth: 180
    });

    var markers = new Array();
    
    var iconCounter = 0;
    
    // Add the markers and infowindows to the map
    for (var i = 0; i < locations.length; i++) {  
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map,
        icon: icons[iconCounter]
      });

      markers.push(marker);

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
      
      iconCounter++;
      // We only have a limited number of possible icon colors, so we may have to restart the counter
      if(iconCounter >= iconsLength) {
        iconCounter = 0;
      }
    }

    function autoCenter() {
      //  Create a new viewpoint bound
      var bounds = new google.maps.LatLngBounds();
      //  Go through each...
      for (var i = 0; i < markers.length; i++) {  
        bounds.extend(markers[i].position);
      }
      //  Fit these bounds to the map
      map.fitBounds(bounds);
    }
    

    autoCenter();

    });
</script>


<script>
    $(document).ready(function() {
       var oTable = $('#dataTables-example').DataTable({
                responsive: true,
                "bFilter": false,
              "bInfo": false,
               "bPaginate": false,
                 "language": {
        "emptyTable":     "No Store found"
    }          

        });

      function sortPopularity()
      {
oTable.destroy();
        oTable = 
         $('#dataTables-example').DataTable({
                responsive: true,
                "bFilter": false,
              "bInfo": false,
               "bPaginate": false,
               "order":  [ [0,'desc'] ],
               "emptyTable":     "No Store found"

        });
      }

      function sortProximity()
      {
        oTable.destroy();
    oTable = 
         $('#dataTables-example').DataTable({
                responsive: true,
                "bFilter": false,
              "bInfo": false,
               "bPaginate": false,
               "order":  [ [1,'asc'] ],
               "emptyTable":     "No Store found"

        });
      }
     




$('#sortBy').change( function () {
 console.log($(this).val());
  switch ($(this).val()) {
      case "prox":
      sortProximity();
      break;
    case "popu":
      sortPopularity();
      break;
 
  }
 
});


    });
    </script>
<style type="text/css">
  .dataTables_length
  {
    display:none;
  }
</style>

@stop