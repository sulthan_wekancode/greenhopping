@extends('user.layout.index')
@section('content')

<div class="col-lg-12 banner"><img src="{{ $store->cover_image }}" class="banner-img" style="height:233px;width:1365px">
<div class="profile"><img src="{{ $store->profile_image }}" class="profile-img" style="height:100px;width:93px">

<h2>{{ $store->name }}</h2>
<p>{{ $store->address }} </p>
<?php


$open =  date('h:i a', strtotime($store->open_time));
$close =  date('h:i a', strtotime($store->close_time));

if (strpos($open, '00') !== false) {
    $open =  date('h a', strtotime($store->open_time));
}
else
{
  $open =  date('h:i a', strtotime($store->open_time));
}

if (strpos($close, '00') !== false) {
    $close =  date('h a', strtotime($store->close_time));
}
else
{
  $close =  date('h:i a', strtotime($store->close_time));
}

$webAddr = $store->website;
if (strpos($webAddr, 'http://') !== false) {
    
}
else
{
$webAddr = 'http://'.$webAddr;
}

//$phone = substr($store->phone, 0, -3);


?>

<b> <img src="../public/static//time.png"> {{ $open }} - {{ $close }}</b>
<strong> <img src="../public/static//phone.png"> ({{ $store->phone_code }})  {{ $store->phone }}</strong>
<span><a href='{{ $webAddr }}' target="_new" style="text-decoration:none">{{ $store->website }}</a></span>

</div>

<div class="profile-right">
<div> <span id='totalLike'>{{ $totalLike }}</span> 
<input type="hidden" id="store_id" value="{{ $store->id }}">
<?php
  if (Auth::check())
  {
?>
<input type="hidden" id="user_id" value="{{ Auth::user()->id }}">
<?php
    if($like)
    {
?>

<img src="../public/static/like.png" id='likeStore' class="like">
<?php
    }
    else
    {
?>
<img src="../public/static/unlike.png" id='likeStore' class="unlike">
<?php
    }
  }
  else
  {
?>
<img src="../public/static/unlike.png">
<?php
  }
?>
</div>

<?php $type = Type::where('id', $store->type)->first(); ?>







  <a href="#" data-toggle="tooltip" title="Hooray!" style="display:none">Hover over me</a>


<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>







</div>




</div>
    <!-- Page Content -->
    <div class="container store ">
 <h2 style="color:#7F7474">CATEGORY</h2>
       <div class="col-lg-2 ">
      
       <ul class="category">
<?php
$cat = Category::where('store_id', $store->id)->get();
$juice = Juice::where('store_id', $store->id)->get();
?>
@foreach($cat as $category)
<li><span class='cl' data-gen='{{ $category->id}}'><b>{{ $category->name }}</b></span></li>
@endforeach
       </ul>
       </div>

<div class="col-lg-5 ">
@foreach($juice as $j)
<div class="mid-list jl jl_{{ $j->category_id }} clearfix" style="display:none">
<span class="" >
<img src="{{ $j->image }}" height="50px" width="50px"><p> {{ $j->name }} </p> 

 <b>$ {{ $j->price }}
<span class='add-cart' data-gen='{{ $j->id }}'>+</span>
<strong><span id='va_{{$j->id}}' style='color:#545353'>0</span> </strong> 
<span class='less-cart' data-gen='{{ $j->id }}' >- </span></b>
</span>
</div>
@endforeach
</div>
</div>




<script>
$(document).ready(function() {

$(document).on('click','.cl',function(){
   $('.jl').hide().filter('.jl_' + $(this).data('gen') ).show();
})

$(document).on('click','.add-cart',function(){
    console.log("lesss");
   var val = $(this).data('gen');
   var value = parseInt($('#va_'+val).html());
   if(value==0)
   {

   }
   else
   {

   }
   if(value>=0)
   {
    if(value>=51)
    {
        alert('Cant')
        return false;
    }
   var newValue = value+1;
   $('#va_'+val).html(newValue);
   var value = parseInt($('.bag').html());
   var newValue = value+1;
   $('.bag').html(newValue);

updateCartInfo(newValue);
    

    }
})

$(document).on('click','.less-cart',function(){
    console.log("lesss");
  var val = $(this).data('gen');
  var value = parseInt($('#va_'+val).html());
  if(value>=1)
   {
  var newValue = value-1;
  $('#va_'+val).html(newValue);
  var value = parseInt($('.bag').html());
   var newValue = value-1;
   $('.bag').html(newValue);
   updateCartInfo(newValue);
}
})

function updateCartInfo(newValue)
{
    var userId = $('#userId').val();
    var storeId = $('#storeId').val();
    console.log(newValue);
    $.ajax({
                method: "POST",
                url: "../updateCart",
                data: {
                    
                    userId : userId,
                    storeId : storeId,
                    juiceId : newValue
                }
            })
            .done(function(msg) {
                if(msg.success==1)
                {
                    console.log('ok');
                }
                else
                {
                 console.log('not ok');
                    
                }
            });
}

$('.cl').first().click();
})
</script>


@stop