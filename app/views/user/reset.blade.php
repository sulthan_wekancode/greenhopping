@extends('user.layout.index')
@section('content')
<!-- Page Content -->

<div>
<div class="row">
<div>
<div class="row" style="opacity: 1;">
<div class="col-lg-12">
<div id="error-box">
<div class="row">
<div class="col-xs-12" style="min-height: 900px;">
<br><br>
<p>



<div class="row">
<div class="form-group col-xs-4" style='visibility:hidden'>
<input type="text" class="form-control" placeholder=".col-xs-3">
</div>
<div class="form-group col-xs-4">
<div >
<div class="main-box">
<header class="main-box-header clearfix">
<h2>Reset Password</h2>
</header>
<div class="main-box-body clearfix">
<form role="form" name='newPasswordForm' id='newPasswordForm'>
<?php
if($valid==1)
{
?>
<div class="form-group">

<input type="password" class="form-control" id="password" name='password' placeholder="New Password" maxlength="25" >
<input type="hidden" value='{{ $token }}' id="token">
</div>
<span id='passwordValidate'></span>
<div class="form-group">

<input type="password" class="form-control" id="password_confirm" name='password_confirm' placeholder="Confirm Password" maxlength="25">
</div>
<span id='password_confirmValidate'></span>
<div>
<a class="changePassword btn btn-success">
Change Password</a> 
</div>
</div>
<?php
}
else
{
?>
<div class="alert alert-danger">
<i class="fa fa-times-circle fa-fw fa-lg"></i>
<strong>Oopss !</strong> The Password Reset Link in Invalid.
</div>
<?php
}
?>

<div class="form-group col-xs-4" style='visibility:hidden'>
<input type="text" class="form-control" placeholder=".col-xs-3">
</div>
</div>





</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
@stop