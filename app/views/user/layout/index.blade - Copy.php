<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <link rel="shortcut icon" href="http://greenhoppingapp.com/favicon.ico">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="google-signin-client_id" content="606388516556-27gbda05t739m582c2457hbdga1s3v0u.apps.googleusercontent.com">

    <title>Green Hopping</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('/').('public/static/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('/').('public/static/style.css')}}" rel="stylesheet">
    <link href="{{ asset('/').('public/static/custom.css')}}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ asset('/').('public/static/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<style type="text/css"></style></head>

<body>

    <!-- Navigation -->
    <div class="topbar" style='height:67px'>
        <div class="col-lg-3" style="padding : 17px 10px 10px 10px">
           <a href="{{ URL::to('/') }}">
           <img src="{{ asset('/').('public/static/logo.png')}}" style='height:70px;width:275px;margin:-20px 0px 0px 30px'>
           </a>
        </div>
        {{ Form::open(array('method' => 'get', 'url' => 'search', 'name' => 'addStore', 'id' => 'addStore', 'role' => 'form'))}}
        <div class="col-lg-7 " style='margin:10px -2px -18px -63px'>
        <div class="select" style='height:43px'><img src="{{ asset('/').('public/static/location-icon.png')}}">
          <select name="location" class="select-top" id='location' style='height:41px;padding: 0px 0px 0px 1px'>
            @foreach($cities as $city)
          <option value="{{ $city->id }}"

<?php
if(isset('location'))
{
    echo 'selected';
}
?>

          >{{ $city->name }}</option>
          @endforeach
          </select>
          
          <input name="query" id='query' type="text" class="search-top" 

          value="
<?php
if(isset('cityName'))
{
    echo $cityName;
}
?>
          " 

           placeholder="Search" style='height:40px'>
          </div>
        </div>
        {{ Form::close() }}



<?php
if (Auth::check())
{
?>
<div class="col-lg-2 top-right " style='margin:21px 1px 1px -15px'>
<a id='showProfile' href='javascript:;' style='text-decoration:none;color:white'>

<span id='openSettings'>

<?php
if(Auth::user()->image=='')
{
?>
<img src="https://s3.amazonaws.com/greenhoppingbucket/default-avatar.jpg?X-Amz-Date=20160211T170512Z&X-Amz-Expires=300&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Signature=84d3e0d4dbcf30c8e8a1399436e9ffde45b6ed75854e45f0a4d375505c18671f&X-Amz-Credential=ASIAI2XXSJJ7JVNLUX4A/20160211/us-east-1/s3/aws4_request&X-Amz-SignedHeaders=Host&x-amz-security-token=AQoDYXdzEBoagAIty4fvDL1SaFKD5JlI3jVf6CUBe44rhjzrmrCgctWcovnAj4IF6CO5tQZk6cbQ4UYOFK210Lav5ZRTTiMDVZcIuhk83Xz9OCKWgmjQn2MnOA0qoOcLnymJS5UG%2BlHpTkwh/uoDdYpLdpi7zaeNfnt961XjkD7nIVJhY7DkHPD3lb2L81DbUjwFfOdAcqJXTNDfsDtb%2B4EeyIlcCHBF5LASj4uftYjmpdGhB%2Bqsk%2BHGYUPI6ISQSiHIWRMTfQRdyhnIr/5jDbhXgXA2NFzv3mpTPmOzbK1J6xWauMiodDx80q0OzhlJyxpBbSbyuJ5LVhDBVGOXlSYVSnWoULwrkLHiIPf98rUF" style="height:40px;width:40px;border-radius:26px;margin:-3px 0px -1px 0px">
<?php
}else {
?>
<img src="{{ Auth::user()->image }}" style="height:40px;width:40px;border-radius:26px;margin:-3px 0px -1px 0px">
<?php
}
?>
<span id='userNameNew' style='display:none'>{{ Auth::user()->username }}</span>




 <img src="{{ asset('/').('public/static/dropdown.png')}}" style="height:10px;width:10px">
 </span>
</a> 
<b><img src="{{ asset('/').('public/static/cart_mt.png')}}" style='height:40px;margin:-10px -36px 1px 2px'>
<span class='bag' id='bag' style='color:#545353;padding: 0px 0px 0px 18px'>0</span></b>


</div>


<div class="col-lg-1 pull-right open" id="settingsMenu" style="background-color:#EFEFEF;z-index:9999;font-weight:600;padding:7px 3px 5px 13px;margin:-7px 123px 2px 2px">


<a href="{{ URL::to('profile') }}" style="text-decoration:none"></i>Edit Profile</a><br>
<a href="{{ URL::to('changepassword') }}" style="text-decoration:none"></i>Settings</a><br>
<a href="{{ URL::to('logout') }}" style="text-decoration:none"></i>Logout</a>

</div>



<?php
} else {
?>
        <div class="col-lg-2 top-right " style='margin:21px 1px 1px -15px'>
         <a id='showPopup' href='javascript:;' style='text-decoration:none;color:white'> <img src="{{ asset('/').('public/static/login.png')}}" > Login</a> 
         <b>
         <img src="{{ asset('/').('public/static/cart.png')}}" style='height:38px;margin:-12px -36px 0px 0px;display:none'>
         
         <img src="{{ asset('/').('public/static/cart_mt.png')}}" style='height:40px;margin:-10px -36px 1px 2px'>

         <span class='bag' id='bag' style='color:#545353;padding: 0px 0px 0px 18px'>0</span></b>
        </div>

<?php
}
?>

		
<!-- Login alert -->



<div class="col-lg-3 open pull-right" id='popupArea'>
<div class="col-lg-12" style="background-color:#EFEFEF;z-index:101;height:auto">
<div class="main-boxallau clearfixallau" style="background-color:#EFEFEF">
<div class="tabs-wrapper profile-tabs" style="background-color:#EFEFEF">
<ul class="nav nav-tabs" style="background-color:#EFEFEF">
<li class="active" style="background-color:#EFEFEF;width:47%;padding:-2px 10px 10px 24px;margin: 0px 0px 0px 5px;">
<a data-toggle="tab" style="color:black;height:36px;border-left-color: #EFEFEF;background-color:#EFEFEF !important;" id="showLogin"><b style="margin:0px 0px 0px 13px">LOGIN</b></a></li>
</li>
<li class="" style="background-color:#EFEFEF !important;width:40%">
<a data-toggle="tab" id="showRegister" style="color:black;border: 2;
background-color:#EFEFEF !important;outline: 0;height: 36px;">
<b style="margin:0px 0px 0px 18px">REGISTER</b></a>
<!--
<a data-toggle="tab" id="showRegister" style="color:black;border: 2;
background-color:#EFEFEF !important;border-bottom: 2px solid #43882C;outline: 0;height: 36px;">
<b style="margin:0px 0px 0px 18px">REGISTER</b></a>
-->

</ul>
<div class="tab-content" style="padding:0px 0px 0px 0px "> 
<div class="tab-pane fade active in" id="loginArea">
<div id="login-box-innerallau" style="background-color:#EFEFEF;margin-top: -6%;">
<form role="form" action="#" id="loginForm" name="loginForm" novalidate="novalidate" style="padding : 20px 0px 0px 0px">
<div class="input-group">
<br>
<input class="form-control" id="loginEmail" name="loginEmail" type="text" placeholder="Email Address" style="border:none;background-color:#EFEFEF;

border-bottom:1px;width:135%;
border: 0;

border-bottom: 1px solid #D0C8C8
outline: 0;
">
</div>
<span id="loginEmailValidate"  class='err'></span>
<span id="errorAreaLogin"></span>
<div class="input-group">

<input type="password" id="loginPassword" name="loginPassword" class="form-control" placeholder="Password" style="background-color:#EFEFEF;width:135%;
border: 0;

border-bottom: 1px solid #D0C8C8
outline: 0;
">
</div>
<span id="loginPasswordValidate" class='err'></span>
<br>
<div id="remember-me-wrapper">
<div class="row">
<div class="col-xs-12">
<div class="checkbox-nice">
<input type="checkbox" id="remember-me">&nbsp;Keep me logged in 
<span id="showForget" style='float:right'><a style="text-decoration:none;padding : 0px 0px 0px 31px">Forgot Password</a> </span>

</div>
</div>
</div>

</div>

	
<div class="row">
<div class="col-xs-12">
<br>	
<button type="button" class="btn btn-success col-xs-12" id="doLogin">Login</button>
</div>
</div>
<br>

</form>
</div>
</div>
<div class="tab-pane register fade active in" id="outputArea" style="display:none">
</div>
<div class="tab-pane fade active in" id="registerArea" style="display:none;padding:10px 0px 0px 0px">
<div class="row">
<div class="col-xs-12 col-sm-6">
<button type="submit" class="btn btn-primary col-xs-12 btn-facebook" id="loginFB">
<i class="fa fa-facebook"></i> facebook
</button>


</div>
<div class="col-xs-12 col-sm-6">
<button type="submit" class="btn btn-primary col-xs-12 btn-google-plus" style="background-color: #D62929;display:none">
<i class="fa fa-google"></i> Google
</button>
<div id="my-signin2"></div>

</div>
</div>
<div id="login-box-innerallau" style="background-color:#EFEFEF;margin-top: -6%;">
<form role="form" id="registerForm" name="registerForm" novalidate="novalidate">
<div class="input-group">
<br>
<input class="form-control" type="text" placeholder="User Name" style="border:none;background-color:#EFEFEF;

border-bottom:1px;width:135%;
border: 0;

border-bottom: 1px solid #D0C8C8
outline: 0;
" id="registerUserName" name="registerUserName" maxlength="20">
</div>
<span id="registerUserNameValidate" class='err'></span>
<span id="errorAreaRegister"></span>
<div class="input-group">

<input class="form-control" type="text" placeholder="Email Address" style="border:none;background-color:#EFEFEF;

border-bottom:1px;width:135%;
border: 0;

border-bottom: 1px solid #D0C8C8
outline: 0;
" id="registerEmail" name="registerEmail">
</div>
<span id="registerEmailValidate" class='err'></span>

<div class="input-group">

<input type="password" class="form-control" placeholder="Password" style="background-color:#EFEFEF;width:135%;
border: 0;

border-bottom: 1px solid #D0C8C8
outline: 0;
" id="registerPassword" name="registerPassword" maxlength="15">
</div>
<span id="registerPasswordValidate" class='err'></span>
<div class="input-group">

<input type="password" class="form-control" placeholder="Confirm Password" id="registerPasswordConfirmation" name="registerPasswordConfirmation" maxlength="15" style="background-color:#EFEFEF;width:135%;
border: 0;

border-bottom: 1px solid #D0C8C8
outline: 0;
">
<br>
</div>
<span id="registerPasswordConfirmationValidate" class='err'></span>
<div class="row">
<div class="col-xs-12">
<br>
<button type="submit" class="btn btn-success col-xs-12" id="doRegister">Register</button>
</div>
</div>
<br>

</form>
</div>
</div>

<!-- reset -->
<div class="tab-pane fade active in" id="forgetArea" style="display:none">
<div id="login-box-innerallau" style="background-color:#EFEFEF;margin-top: -6%;">
<form role="form" id="resetForm">
<div class="input-group">
<br>
<input class="form-control" type="email" name="email" id="resetEmail"  placeholder="Email Address" style="border:none;background-color:#EFEFEF;

border-bottom:1px;width:135%;
border: 0;

border-bottom: 1px solid #D0C8C8
outline: 0;
">
</div>
<br>


<div class="row">
<div class="col-xs-12">

<button type="button" class="btn btn-success col-xs-12" id="doReset" style="padding : 5px 6px 7px 20px;margin:0px 0px 0px 37px">Reset</button>

</div>
</div>

<br>
</form>
</div>
</div>


</div>
</div>
</div>
</div>
		</div>
		<style>
		.open
		{
			display:none
		}
		</style>
<!-- Login alert -->
		
        
        
        
    </div>
    <script src="{{ asset('/').('public/js/jquery.js') }}"></script>
    <script src="{{ asset('/').('public/js/jquery.cookie.js') }}"></script>
<script src="{{ asset('/').('public/asset/jquery.validate.js') }}"></script>

    <script src="{{ asset('/').('public/static/custom.js') }}"></script>


    @yield('content')

    <!-- jQuery -->


    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('/').('public/static/bootstrap.min.js') }}"></script>


<script>

function testAPI() {
        console.log('Welcome!  Fetching your information.... ');
         var url = '/me?fields=name,email,link';
        FB.api(url, function (response) {
          console.log(response);
          var imgUrl = 'http://graph.facebook.com/'+response.id+'/picture/?type=large';
          localStorage.setItem("fbImg", imgUrl);
          var fbId = response.id;
          var fbName = response.name;
          var fbEmail = response.email;

          

        $.ajax({
            url: 'checkFBLogin',
            type: 'POST',
            data: {facebookId: fbId, username : fbName, email : fbEmail},
            success: function (message)
            {
                console.log(message);        
                console.log('good');
                console.log(message.success);
                
                if (message.success == 1)
                {
                    location.reload();
                }
                if (message.success == 2)
                {
                    location.reload();
                }

                

            }

        
        });
        });
      }


var csrf = '';
// This is called with the results from from FB.getLoginStatus().
function statusChangeCallback(response) {
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
        // Logged into your app and Facebook.
        getFBDetails(response);
    } else if (response.status === 'not_authorized') {
        // The person is logged into Facebook, but not your app.
        var c = 'Please log ' + 'into this app.';
        //document.getElementById('status').innerHTML = c;
        console.log(c);
    } else {
        // The person is not logged into Facebook, so we're not sure if
        // they are logged into this app or not.
        var c = 'Please log ' + 'into Facebook.';
        //document.getElementById('status').innerHTML = c;
        console.log(c);
    }
}

// This function is called when someone finishes with the Login
// Button.  See the onlogin handler attached to it in the sample
// code below.
function checkLoginState() {
    FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
    });
}

window.fbAsyncInit = function() {
};

// Load the SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Here we run a very simple test of the Graph API after login is
// successful.  See statusChangeCallback() for when this call is made.
function getFBDetails(resObj) {
    console.log('Welcome!  Fetching fb information.... ');
    FB.api('/me', { locale: 'en_US', fields: 'name, first_name, last_name, gender, email' }, function(response) {
        var c = 'Thanks for logging in, ' + response.name + ', ' + response.email + ', ' + resObj.authResponse.accessToken;
        //document.getElementById('status').innerHTML = c;
        console.log(c);
        console.log(response);
        console.log(resObj.authResponse.accessToken);

       testAPI();

    });
}
  
function fbLogin(){
    FB.init({
        appId: '1487150151594094',
        cookie     : true,  
        xfbml      : true,
        version    : 'v2.2',
        status      : true,
    });

    FB.login(function(response){
        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });
    }, {scope: 'public_profile,email'});

}

function onSuccessGPLogin(googleUser) {
    console.log(googleUser.getBasicProfile());
    var accessToken = googleUser.hg.access_token;
    var name = googleUser.getBasicProfile().getName();
    var firstName = googleUser.getBasicProfile().Za;
    var lastName = googleUser.getBasicProfile().Na;
    var email = googleUser.getBasicProfile().hg;
    console.log('Logged in as: ' + name);

    $.ajax({
        method: "POST",
        url: "/account/gmail_login/",
        async:false,
        data: {
            'csrfmiddlewaretoken': csrf,
            'first_name' : firstName,
            'last_name' : lastName,
            'email' : email,
            'access_token' : accessToken,
        }
    })
    .done(function(msg) {
        if (msg.status) {
            document.location.href = '/';
            return false;
        } else {
            var errMessage = '';
            jQuery.each(msg.result, function(index, item) {
                if (errMessage == '') {
                    errMessage += item;
                }
            });
            console.log('GMail login error ' + errMessage);
        }
    });
}

function onFailureGPLogin(error) {
    console.log(error);
}

function renderButtonGPLogin() {
    gapi.signin2.render('div-login-google', {
    'scope': 'https://www.googleapis.com/auth/plus.login',
    'width': 355,
    'height': 50,
    'longtitle': true,
    'theme': 'dark',
    'onsuccess': onSuccessGPLogin,
    'onfailure': onFailureGPLogin
  });
}

$(document).ready(function() {
    
    $("#loginFB").click(function(event) {
        event.preventDefault();
        fbLogin();      
    });
});

</script>


<script>
    function onSuccess(googleUser) {
console.log(googleUser);

googleId = googleUser.El;

googleUserName = googleUser.wc.wc;
googleEmail = googleUser.wc.hg;
gphoto = googleUser.wc.Ph;





$.ajax({
            url: 'checkGoogleLogin',
            type: 'POST',
            data: {googleID: googleId, googleUserName : googleUserName, googleEmail : googleEmail},
            success: function (message)
            {
                console.log(message);        
                console.log('good');
                console.log(message.success);
                
                if (message.success == 1)
                {
                    location.reload();
                }
                if (message.success == 2)
                {
                    location.reload();
                }

                

            }

        });

      //console.log('Logged in as: ' + googleUser.getBasicProfile().getName());
    }
    function onFailure(error) {
      console.log(error);
    }
    function renderButton() {
      gapi.signin2.render('my-signin2', {
        'scope': 'https://www.googleapis.com/auth/plus.login',
        'width': 115,
        'height': 35,
        'longtitle': true,
        'theme': 'dark',
        'onsuccess': onSuccess,
        'onfailure': onFailure
      });
    }
  </script>
  <script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>
 
<style>
.abcRioButton
{
    border-radius: 4px
}

</body>
</html>