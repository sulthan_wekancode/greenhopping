<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="shortcut icon" href="http://greenhoppingapp.com/favicon.ico">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="google-signin-client_id" content="606388516556-27gbda05t739m582c2457hbdga1s3v0u.apps.googleusercontent.com">
    <title>Green Hopping</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('/').('public/static/bootstrap.min.css')}}" rel="stylesheet">
    <!-- jquery dd plugin css for select box - Asfath -->
    <link href="{{ asset('/').('public/css/dd.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('/').('public/static/style.css')}}" rel="stylesheet">
    <link href="{{ asset('/').('public/static/custom.css')}}" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="{{ asset('/').('public/static/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="{{ asset('/').('public/js/jquery.js') }}"></script>
    <script src="{{ asset('/').('public/js/jquery.cookie.js') }}"></script>
    <script src="{{ asset('/').('public/asset/jquery.validate.js') }}"></script>
    <script src="{{ asset('/').('public/static/custom.js') }}"></script>
    <!-- jquery dd plugin for select box - Asfath -->
    <script src="{{ asset('/').('public/js/jquery.dd.min.js') }}"></script>
    <script src="{{ asset('/').('public/js/temp.js') }}"></script>
</head>
<body>
    <!-- Login content -->
        <!--<div class="topbar" style="height:67px !important">-->
        <div class="topbar">
            <div class="col-lg-3">
              <a href="/"> <img src="{{ asset('/').('public/img/logo.png')}}" /> </a>
               <!--I used it as background image-->
            </div>
            
            <div class="col-lg-5 ">
			{{ Form::open(array('method' => 'get', 'url' => 'search', 'name' => 'addStore', 'id' => 'addStore', 'role' => 'form'))}}
            <input type="submit" style="display:none" />
            <div class="select">
            <!--<img src="public/static/location-icon.png">-->
              <select name="location" class="select-top" id="select-loc">
                <option disabled>Choose your city</option>
				        @foreach ($cities as $city)
                <option value="{{ $city->id }}" 

<?php
if(Input::get('location'))
{
  if($city->id==Input::get('location'))
  {
    echo 'selected';
  }
}
?>
                >{{ $city->name}}</option>
                @endforeach
              </select>
              
              <div style="margin-left: 200px;">
                
				<input name="query" id='query'  type="text" class="search-top" placeholder="Search" >
              </div>
              </div>
			  {{ Form::close() }}
            </div>
            <div class="col-lg-4 top-right ">
            <div class="pull-right">
              <div class="container">
              <!--<a href="javascript:void(0);"><img src="{{ asset('/').('public/static/cart-content.png') }}"/></a>--> 
              
              <a href="javascript:void(0);"><img style="margin-top:-10px" src="{{ asset('/').('public/img/cart_empty.png') }}">
<span style="color: rgb(84, 83, 83); background-color: rgb(255, 255, 255); border-radius: 50%; overflow: hidden; text-align: center; position: absolute; margin-left: -28px; margin-top: 10px; height: 20px; width: 20px;" id="bag" class="bag hide">0</span></a>
  			  <span class="menu-divider"></span>
		  
			    <a href="javascript:void(0);"><img src="{{ asset('/').('public/static/blub.png') }}"/></a>
			    <span class="menu-divider"></span>
  			  
      <?php
if (Auth::check())
{
?>
 
 
  <a href="javascript:void(0);"><img src="{{ asset('/').('public/static//notification.png') }}"/></a>
  <span class="menu-divider"></span>
  
<div class="btn-group pull-right profilebutton">
<button data-toggle="dropdown" class="btn btn-info pointer" type="button"><img alt="profile" height="32" width="32" class="img-circle" src="https://s3.amazonaws.com/greenhopping/images/userAvatar.jpg" /></button>
<button data-toggle="dropdown" class="btn btn-info dropdown-toggle" type="button">
<span class="caret"></span>
</button>
<ul role="menu" class="dropdown-menu">
<li><a href="{{ URL::to('profile') }}">Edit Profile</a></li>
<li><a href="{{ URL::to('changepassword') }}">Settings</a></li>
<!--<li class="divider"></li>-->
<li><a href="{{ URL::to('logout') }}">Logout</a></li>
</ul>
</div>


 
 
<?php } else { ?>
  
    
      <!-- Trigger the modal with a button -->
      <a type="button" id="myloginbutton"><img src="{{ asset('/').('public/static/login.png') }}" style="border-right:none"> Login </a>
      <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content" style="background-color:#EFEFEF">
   <div class="container">
      <ul class="nav nav-pills">
        <img src="{{ asset('/').('public/static/arrow-top.png') }}" class="arw">
        <li class="active"><a data-toggle="pill" href="#login" class="displayLogin">LOGIN</a></li>
        <li><a data-toggle="pill" href="#register" class="displayRegister">REGISTER</a></li>
      </ul>
      <span class="showSocial">
      <a href="">
      <img src="{{ asset('/').('public/static/fb.png') }}" class="connect" style="border-right:none;padding:0px 0px 0px 0px"></a>
       <a href="">
       <img src="{{ asset('/').('public/static/gplus.png') }}" class="connect" style="border-right:none;padding:0px 0px 0px 0px"></a>
      </span>
      <div class="tab-content">
      <div id="msgArea">
      </div>
      <div id="resetArea" style="display:none" class="tab-pane">
      <form role="form" id="resetForm">
            Email
           <input type="email" name="email" id="resetEmail" maxlength="25" class="sign-tab txt"><span id="resetEmailValidate" class='err'></span><br>
           <input name="" type="button" value="Reset Password" id="doReset" class="sign" style='height: 33px;width: 109px;'>
           </form>
      </div>
        <div id="registerArea" class="tab-pane fade" style="margin:-15px 0px 0px 0px">
        <form role="form" id="registerForm" name="registerForm" novalidate="novalidate">
           User Name
           <br>
          <input name="registerUserName" id="registerUserName" type="text" class="sign-tab txt" maxlength="25">
          <span id="registerUserNameValidate" class='err'></span><span id="errorAreaRegister"></span><br>
          Email
          <input name="registerEmail" id="registerEmail" type="email" class="sign-tab txt" maxlength="15">
          <span id="registerEmailValidate" class='err'></span><br>
          Password
           <input id="registerPassword" name="registerPassword" type="password" class="sign-tab txt" maxlength="15">
           <span id="registerPasswordValidate" class='err'></span><br>
           Confirm Password
           <input type="password" class="sign-tab txt" id="registerPasswordConfirmation" name="registerPasswordConfirmation" maxlength="15">
           <span id="registerPasswordConfirmationValidate" class='err'></span><br>
           <input name="" id="doRegister" type="button" value="SIGN-UP" class="sign" style='height: 33px;width: 80px;'>
           </form>
        </div>
        <div id="loginArea" class="tab-pane fade in active">
        <form role="form" action="#" id="loginForm" name="loginForm" novalidate="novalidate">
           Email
          <input id="loginEmail" name="loginEmail" type="text" type="email" class="sign-tab txt"  maxlength="25"><span id="loginEmailValidate"  class='err'></span><span id="errorAreaLogin" style="color: red;font-size: 11px;font-weight: bold;"></span><br>
          Password
           <input type="password" id="loginPassword" name="loginPassword" class="sign-tab txt" maxlength="25"><span id="loginPasswordValidate" class='err'></span><br>
           <span class='displayReset' style="float:right;color:#42830f;">Forgot password</span>
           <input name="" type="checkbox" value=""> keep me logged in<br>
           <input name="" type="button" id="doLogin" value="LOGIN" class="sign" style='height: 33px;width: 80px;'>
           <br><br><br>
           <center style='color:#42830f;font-weight: 600;' id=''><a data-toggle="pill" href="#register" style="color:#42830f" class="displayRegister">Not a Hopper yet? Sign Up</a></center>
           </form>
        </div>
       
      </div>
    </div>

    
     </div>
          
        </div>
      </div>
    
<?php } ?>
</div>
</div>
    </div>
    </div>
    @yield('content')
    <!-- jQuery -->
    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('/').('public/static/bootstrap.min.js') }}"></script>
<script>
function testAPI() {
        console.log('Welcome!  Fetching your information.... ');
         var url = '/me?fields=name,email,link';
        FB.api(url, function (response) {
          console.log(response);
          var imgUrl = 'http://graph.facebook.com/'+response.id+'/picture/?type=large';
          localStorage.setItem("fbImg", imgUrl);
          var fbId = response.id;
          var fbName = response.name;
          var fbEmail = response.email;
        $.ajax({
            url: 'checkFBLogin',
            type: 'POST',
            data: {facebookId: fbId, username : fbName, email : fbEmail},
            success: function (message)
            {
                console.log(message);        
                console.log('good');
                console.log(message.success);
                if (message.success == 1)
                {
                    location.reload();
                }
                if (message.success == 2)
                {
                    location.reload();
                }
            }        
        });
        });
      }

var csrf = '';
// This is called with the results from from FB.getLoginStatus().
function statusChangeCallback(response) {
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
        // Logged into your app and Facebook.
        getFBDetails(response);
    } else if (response.status === 'not_authorized') {
        // The person is logged into Facebook, but not your app.
        var c = 'Please log ' + 'into this app.';
        //document.getElementById('status').innerHTML = c;
        console.log(c);
    } else {
        // The person is not logged into Facebook, so we're not sure if
        // they are logged into this app or not.
        var c = 'Please log ' + 'into Facebook.';
        //document.getElementById('status').innerHTML = c;
        console.log(c);
    }
}

// This function is called when someone finishes with the Login
// Button.  See the onlogin handler attached to it in the sample
// code below.
function checkLoginState() {
    FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
    });
}

window.fbAsyncInit = function() {
};

// Load the SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Here we run a very simple test of the Graph API after login is
// successful.  See statusChangeCallback() for when this call is made.
function getFBDetails(resObj) {
    console.log('Welcome!  Fetching fb information.... ');
    FB.api('/me', { locale: 'en_US', fields: 'name, first_name, last_name, gender, email' }, function(response) {
        var c = 'Thanks for logging in, ' + response.name + ', ' + response.email + ', ' + resObj.authResponse.accessToken;
        //document.getElementById('status').innerHTML = c;
        console.log(c);
        console.log(response);
        console.log(resObj.authResponse.accessToken);

       testAPI();

    });
}
  
function fbLogin(){
    FB.init({
        appId: '1487150151594094',
        cookie     : true,  
        xfbml      : true,
        version    : 'v2.2',
        status      : true,
    });

    FB.login(function(response){
        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });
    }, {scope: 'public_profile,email'});

}

function onSuccessGPLogin(googleUser) {
    console.log(googleUser.getBasicProfile());
    var accessToken = googleUser.hg.access_token;
    var name = googleUser.getBasicProfile().getName();
    var firstName = googleUser.getBasicProfile().Za;
    var lastName = googleUser.getBasicProfile().Na;
    var email = googleUser.getBasicProfile().hg;
    console.log('Logged in as: ' + name);

    $.ajax({
        method: "POST",
        url: "/account/gmail_login/",
        async:false,
        data: {
            'csrfmiddlewaretoken': csrf,
            'first_name' : firstName,
            'last_name' : lastName,
            'email' : email,
            'access_token' : accessToken,
        }
    })
    .done(function(msg) {
        if (msg.status) {
            document.location.href = '/';
            return false;
        } else {
            var errMessage = '';
            jQuery.each(msg.result, function(index, item) {
                if (errMessage == '') {
                    errMessage += item;
                }
            });
            console.log('GMail login error ' + errMessage);
        }
    });
}

function onFailureGPLogin(error) {
    console.log(error);
}

function renderButtonGPLogin() {
    gapi.signin2.render('div-login-google', {
    'scope': 'https://www.googleapis.com/auth/plus.login',
    'width': 355,
    'height': 50,
    'longtitle': true,
    'theme': 'dark',
    'onsuccess': onSuccessGPLogin,
    'onfailure': onFailureGPLogin
  });
}
$(document).ready(function() {

    $('.showSocial').hide();
    $("#loginFB").click(function(event) {
        event.preventDefault();
        fbLogin();      
    });
    $(".displayLogin").click(function(event) {
        hideAllTab();
        $('#loginArea').removeClass('tab-pane fade').addClass('tab-pane fade in active');
        $('#registerArea').removeClass('in active');
        $('#loginArea').show();
        
    });
    $(".displayRegister").click(function(event) {
        hideAllTab();
        $('.showSocial').show();
        $('#registerArea').removeClass('tab-pane fade').addClass('tab-pane fade in active');
        $('#loginArea').removeClass('in active');
        $('#registerArea').show();
        
    });
    $(".displayReset").click(function(event) {
        hideAllTab();
        $('#resetArea').show();
    });
    function hideAllTab()
    {
        
        $('#msgArea').hide(); $('#msgArea').html();
        $('.err').html('');
        $('#registerArea').hide();
        $('#loginArea').hide();
        $('#resetArea').hide();
        $('.showSocial').hide();
        
    }

});
</script>
<script>
function onSuccess(googleUser) {
console.log(googleUser);
googleId = googleUser.El;
googleUserName = googleUser.wc.wc;
googleEmail = googleUser.wc.hg;
gphoto = googleUser.wc.Ph;
$.ajax({
            url: 'checkGoogleLogin',
            type: 'POST',
            data: {googleID: googleId, googleUserName : googleUserName, googleEmail : googleEmail},
            success: function (message)
            {
                console.log(message);        
                console.log('good');
                console.log(message.success);
                
                if (message.success == 1)
                {
                    location.reload();
                }
                if (message.success == 2)
                {
                    location.reload();
                }

                

            }

        });

      //console.log('Logged in as: ' + googleUser.getBasicProfile().getName());
    }
    function onFailure(error) {
      console.log(error);
    }
    function renderButton() {
      gapi.signin2.render('my-signin2', {
        'scope': 'https://www.googleapis.com/auth/plus.login',
        'width': 115,
        'height': 35,
        'longtitle': true,
        'theme': 'dark',
        'onsuccess': onSuccess,
        'onfailure': onFailure
      });
    }
  </script>
    
<script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>
 <style>
.txt
{
background-color:#EFEFEF;
border: 0 !important;
border-bottom: 1px solid #D0C8C8 !important;
outline: 0 !important;
margin: -4px 0px 0px 0px;
}
 </style>
</body>
</html>