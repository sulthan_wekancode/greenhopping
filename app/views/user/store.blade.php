@extends('user.layout.index')
@section('content')

<div class="col-lg-12 banner"><img src="http://images8.alphacoders.com/399/399003.jpg" class="banner-img" style="height:175px;width:100%"><!--{{ $store->cover_image }}-->
<div class="profile"><img src="{{ $store->profile_image }}" class="profile-img" style="height:70px;width:70px">

<h2>{{ $store->name }}</h2>
<p class='fs10 lightwhite'>{{ $store->address }} </p>
<?php


$open =  date('h:i a', strtotime($store->open_time));
$close =  date('h:i a', strtotime($store->close_time));

if (strpos($open, '00') !== false) {
    $open =  date('h a', strtotime($store->open_time));
}
else
{
  $open =  date('h:i a', strtotime($store->open_time));
}

if (strpos($close, '00') !== false) {
    $close =  date('h a', strtotime($store->close_time));
}
else
{
  $close =  date('h:i a', strtotime($store->close_time));
}

$webAddr = $store->website;
if (strpos($webAddr, 'http://') !== false) {
    
}
else
{
$webAddr = 'http://'.$webAddr;
}

//$phone = substr($store->phone, 0, -3);


?>

<b class="fs10 lightwhite"> <img src="../public/static//time.png"> {{ $open }} - {{ $close }}</b>
<strong class="fs12"> <img src="../public/static//phone.png"> ({{ $store->phone_code }})  {{ $store->phone }}</strong>
<span><a class="darkgreen semibold" href='{{ $webAddr }}' target="_new" style="text-decoration:none">{{ $store->website }}</a></span>

</div>

<div class="profile-right">
<div> <span id='totalLike'>{{ $totalLike }}</span> 
<input type="hidden" id="store_id" value="{{ $store->id }}">
<?php
  if (Auth::check())
  {
?>
<input type="hidden" id="user_id" value="{{ Auth::user()->id }}">
<?php
    if($like)
    {
?>

<img src="../public/static/like.png" id='likeStore' class="like">
<?php
    }
    else
    {
?>
<img src="../public/static/unlike.png" id='likeStore' class="unlike">
<?php
    }
  }
  else
  {
?>
<img src="../public/static/unlike.png">
<?php
  }
?>
</div>

<?php $type = Type::where('id', $store->type)->first(); ?>







  <a href="#" data-toggle="tooltip" title="Hooray!" style="display:none">Hover over me</a>


<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>







</div>




</div>
    
    <!--New page starts-->
    
    <div class="">
    	<div class="col-md-2 leftsidebar">
			<div class="main-box clearfix pl20">
			<h2 class="fs16 semibold pb20">CATEGORY</h2>
			
			<ul class="nav nav-stacked catmenu">
			
			<?php
				$cat = Category::where('store_id', $store->id)->get();
				$juice = Juice::where('store_id', $store->id)->get();
			?>
			
			@foreach($cat as $category)
				<li class="cl" data-gen="{{ $category->id}}" class="text-muted semibold fs12">
					<a class="pl0 pt0" href="#">
					<img class="img-circle" width="32" height="32" src="{{  $category->image }}" />
					<span>{{ $category->name }}</span>
					</a>
				</li>
				
			@endforeach
				
				<!--<li class="text-muted semibold fs12">
					<a class="pl0 pt0" href="#">
					<img width="32" height="32" src="/greenhopping/public/img/caticon.png" />
					<span>Category 1</span>
					</a>
				</li>-->
				
			
			
			</ul>

			</div>
		</div>
		<div class="col-md-10">
			<div class="main-box clearfix">
				<h2>&nbsp;</h2>
				
				
				
				
								
				<div class="row">
				

				<?php
				$c = 0;
				foreach($juice as $j){
					$c++;
					$bc = "";
					if($c > 3)
						$bc = " fruititem";
				
				echo '<div class="col-md-4 col-sm-4 col-xs-6 jl jl_'.$j->category_id.'">
						<div class="pt10 pb10 ml10 m120 fruitclass'.$bc.'">
							<div class="pull-left">
								<img height="32" width="32" src="'.$j->image.'"/>
								<span class=" fs12 text-muted">'.$j->name.'</span>
							</div>
							<div class="pull-right mt5">
							<span class="semibold fs12 pr15">$'.$j->price.'</span>
							<span class="">
								<span data-gen="'.$j->id.'" class="pr10 green fs12 less-cart pointer"><i class="fa fa-minus"></i></span>
								<span id="va_'.$j->id.'">0</span>
								<span data-gen="'.$j->id.'" class="pl10 green fs12 add-cart pointer"><i class="fa fa-plus"></i></span>
							</span>	
							</div>
							<div class="clearfix"></div>
						</div>
					
					</div>';
					
			} ?>
			
				
					
			
				
				</div>
				
			   
			</div>
		</div>
    </div>
    
    <!--New Page ends-->
    
    <!-- Page Content -->
    



<script>
$(document).ready(function() {

$(document).on('click','.cl',function(){

	$('.cl').removeClass("active");
	 $(this).addClass("active");
refreshTab($(this).data('gen'));
   $('.jl').hide().filter('.jl_' + $(this).data('gen') ).show();
})
function refreshTab(el)
{
	console.log('refreshTab');
$(".fruitclass").removeClass('fruititem');
var ctr = 0;
$(".jl_"+el).each(function(){
ctr++;
if(ctr>3)
{
	$(this).addClass('fruititem');
}
});
}


$(document).on('click','.add-cart',function(){
    console.log("lesss");
   var val = $(this).data('gen');
   var value = parseInt($('#va_'+val).html());
   if(value==0)
   {
   	
   }
   else
   {
   	
   }
   if(value>=0)
   {
    if(value>=51)
    {
        alert('Cant')
        return false;
    }
   var newValue = value+1;
   $('#va_'+val).html(newValue);
   var value = parseInt($('.bag').html());
   var newValue = value+1;
   if(newValue==0)
   {
		$('.bag').addClass('hide');
   }
   else
   {
   		$('.bag').removeClass('hide');
   }
   $('.bag').html(newValue);

updateCartInfo(newValue);
    

    }
})

$(document).on('click','.less-cart',function(){
    console.log("lesss");
  var val = $(this).data('gen');
  var value = parseInt($('#va_'+val).html());
  if(value>=1)
   {
  var newValue = value-1;
  $('#va_'+val).html(newValue);
  var value = parseInt($('.bag').html());
   var newValue = value-1;
   $('.bag').html(newValue);
   if(newValue==0)
   {
		$('.bag').addClass('hide');
   }
   else
   {
   		$('.bag').removeClass('hide');
   }
   updateCartInfo(newValue);
}
})

function updateCartInfo(newValue)
{
    var userId = $('#userId').val();
    var storeId = $('#storeId').val();
    console.log(newValue);
    $.ajax({
                method: "POST",
                url: "../updateCart",
                data: {
                    
                    userId : userId,
                    storeId : storeId,
                    juiceId : newValue
                }
            })
            .done(function(msg) {
                if(msg.success==1)
                {
                    console.log('ok');
                }
                else
                {
                 console.log('not ok');
                    
                }
            });
}

$('.cl').first().click();
})
</script>
<style>
.hide
{
	display: none
}
</style>

@stop