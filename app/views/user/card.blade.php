@extends('user.layout.index')
@section('content')
<!-- Page Content -->



<!-- Page Content -->
    <div class=" ">
<link href="public/asset/style.css" rel="stylesheet">
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDKw1I9ZlI-piCBp2zXSuviBDVRjju-aYI&sensor=true"></script>
<script src="public/asset/where.js"></script>

       <div class="col-lg-6 halfbox" style="position:static">
       <div  id="googlemaps" style="margin:0px 0px 0px 0px;width:50%"></div></div>
       <div class="col-lg-6 halfbox-right">
     <div class="col-lg-12 user" style="padding : 8px 15px 26px 27px">
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>


<span id='showAddressArea' class="opens" style='width: 90%;'>
<input type="hidden" id="user_id" value="{{ Auth::user()->id }}">

<span><b style='font-size: 18px'>Manage Card</b></span>
<br>
<?php
if(Auth::user()->stripe_card=='')
{
?>
<span id="showAddCard">
  <h4>Add Card</h4>
</span>
<?php
}
else
{
?>
<span id="showUpdateCard">
  <h4>Update Card</h4>
</span>
<span id="editCardArea" class="showCard">
<form action="" method="POST" id="update-payment-form">
    <span class="payment-errors"></span>

    <div class="form-row">
      <label>
        <span>Card Number</span>
        <input type="text" size="20" data-stripe="number"/>
      </label>
    </div>

    <div class="form-row">
      <label>
        <span>CVC</span>
        <input type="text" size="4" data-stripe="cvc"/>
      </label>
    </div>

    <div class="form-row">
      <label>
        <span>Expiration (MM/YYYY)</span>
        <input type="text" size="2" data-stripe="exp-month"/>
        <input type="text" size="4" data-stripe="exp-year"/>
      </label>
      
    </div>

    <button type="submit">Update Card</button>
  </form>
</span>


<span id="updateCardArea">
<img src="public/static/delete.png" height="15px" class="close closebtn" id="64">
<strong><span id="cardNumber"></span> <em class="editCard" id="64" style="font-size:12px"> <a href="#">Edit</a></em></strong> 
<hr>
</span>
<?php
}
?>
<span id="cardAdd"></span>
<span id="addCardArea" class="showCard">
<form action="" method="POST" id="payment-form">
    <span class="payment-errors"></span>

    <div class="form-row">
      <label>
        <span>Card Number</span>
        <input type="text" size="20" data-stripe="number"/>
      </label>
    </div>

    <div class="form-row">
      <label>
        <span>CVC</span>
        <input type="text" size="4" data-stripe="cvc"/>
      </label>
    </div>

    <div class="form-row">
      <label>
        <span>Expiration (MM/YYYY)</span>
        <input type="text" size="2" data-stripe="exp-month"/>
        <input type="text" size="4" data-stripe="exp-year"/>
      </label>
      
    </div>

    <button type="submit">Save Card</button>
  </form>
</span>

</span>
 </div>
   
    </div>
    <!-- /.container -->
    

<script type="text/javascript">
$(document).ready(function() {

//Regular Initiate

    Stripe.setPublishableKey('pk_test_9chBKqWyA0WjWnvRwP4AGT5W');

    $('#payment-form').submit(function(e) {
        var $form = $(this);
        Stripe.card.createToken($form, stripeResponseHandler);
        return false;
      });

      $('#update-payment-form').submit(function(e) {
        var $form = $(this);
        Stripe.card.createToken($form, stripeResponseHandlerUpdate);
        return false;
      });


    $("#showAddCard").click(function(){
      console.log('shownhide');
      $("#addCardArea").toggleClass("showCard");
    });

    $(".editCard").click(function(){
      console.log('editCard');
      $("#updateCardArea").hide();
      $("#editCardArea").toggleClass("showCard");
    });

   

    var stripeResponseHandler = function(status, response) {
      var $form = $('#payment-form');
      if (response.error) {
        $form.find('.payment-errors').text(response.error.message);
      } else {
        console.log(response.card.id);        
        var token = response.id;
         $.ajax({
    url: 'addCard',
    type : 'POST',  
     data: {  user_id :  $("#user_id").val(), token : token, card : response.card.id},
    success: function(message)
    {
    if(message.success==1)
        {
          location.reload();

          $("#addCardArea").toggleClass("showCard");     
          $("#cardAdd").html('<div class="alert alert-success fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><i class="fa fa-check-circle fa-fw fa-lg"></i>Card Added Succesfully</div>');        
          $("#cardAdd").show();
          setTimeout(function(){ $("#cardAdd").html('');  $("#cardAdd").hide();  }, 2000);
          $("#showAddCard").show();

          $("#showUpdateCard").show();
          $("#updateCardArea").show();

        }
        else
        {
        } 
    }
    }); 
      }
    };

    var stripeResponseHandlerUpdate = function(status, response) {
      var $form = $('#payment-form');
      if (response.error) {
        $form.find('.payment-errors').text(response.error.message);
      } else {
        console.log(response.card.id);  
        alert('delete old card');      
        var token = response.id;

        $.ajax({
    url: 'deleteAllCard',
    type : 'POST',  
     data: {  user_id :  $("#user_id").val(), token : token, card : response.card.id},
    success: function(message)
    {
    if(message.success==1)
        {
          location.reload();
          $("#addCardArea").toggleClass("showCard");     
          $("#cardAdd").html('<div class="alert alert-success fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><i class="fa fa-check-circle fa-fw fa-lg"></i>Card Added Succesfully</div>');        
          $("#cardAdd").show();
          setTimeout(function(){ $("#cardAdd").html('');  $("#cardAdd").hide();  }, 2000);
          $("#showAddCard").show();

          $("#showUpdateCard").show();
          $("#updateCardArea").show();
        }
        else
        {
        } 
    }
    }); 


         $.ajax({
    url: 'addCard',
    type : 'POST',  
     data: {  user_id :  $("#user_id").val(), token : token, card : response.card.id},
    success: function(message)
    {
    if(message.success==1)
        {
          $("#addCardArea").toggleClass("showCard");     
          $("#cardAdd").html('<div class="alert alert-success fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><i class="fa fa-check-circle fa-fw fa-lg"></i>Card Added Succesfully</div>');        
          $("#cardAdd").show();
          setTimeout(function(){ $("#cardAdd").html('');  $("#cardAdd").hide();  }, 2000);
          $("#showAddCard").show();

          $("#showUpdateCard").show();
          $("#updateCardArea").show();
        }
        else
        {
        } 
    }
    }); 
      }
    };


  });
</script>

<?php if(Auth::user()->stripe_card=='') {  ?>

<?php } else { ?>

<script type="text/javascript">
$(document).ready(function() {
  listCard();
  function listCard()
{
  $.ajax({
    url: 'listCard',
    type : 'POST',  
     data: {  id :  $("#user_id").val()},
    success: function(message)
    {
    if(message.success==1)
        {
          $.each(message.card.data, function(k, v) {
          console.log(k);
          
              $('#cardNumber').html('XXXX-XXXX-XXXX-'+v.last4);
          });
        }
        else
        {
            
        } 
    }
    });
}
});
</script>

<?php } ?>

<script>
$(document).ready(function() {


$(".close").click(function(){

    var r = confirm("Are you sure you want to delete this Card ?");
    if (r == true) {
            
    } else {
        return false;
    }

     $.ajax({
    url: 'deleteCard',
    type : 'POST',  
     data: {  id :  $("#user_id").val()},
    success: function(message)
    {
    if(message.success==1)
        {
          alert('Card deleted Succesfully');
        }
        else
        {
            
        } 
    }
    });

});

});
</script>


<style type="text/css">
.in
{
   border: 0;
    outline: 0;
    background: transparent;
    border-bottom: 1px solid #CCCCCC;
    width: 90%;    
}
.opens
{
  display: none
}
.showCard
  {
    display: none !important;
  }
</style>



@stop