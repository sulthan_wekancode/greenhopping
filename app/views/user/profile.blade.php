@extends('user.layout.index')
@section('content')
<!-- Page Content -->

<script src="public/asset/userProfile.js"></script>
<script src="{{ asset('/').('public/asset/ajaxupload.3.5.js') }}"></script>
<script src="{{ asset('/').('public/asset/s3_user_profile.js') }}"></script>

<link href="public/asset/style.css" rel="stylesheet">
<link href="public/css/new.css" rel="stylesheet">
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDKw1I9ZlI-piCBp2zXSuviBDVRjju-aYI&sensor=true"></script>
<script src="public/asset/where.js"></script>
<!-- Page Content -->

<div id="googlemaps" class="googleMap halfMap"></div>
  <div class="halfPage">
	<span id="profileUpdatedSuccess"></span>
	  <div class="col-md-5 profileBasic">
		<div class="text-center">
		  <div id="tip_pic">
			<div class="profile-img-container img-circle"  id="tip_cover">
			<span id='rep_pic'
			
			<?php
			if(strlen(Auth::user()->image)<5)
			{
			?>
			  style="background-image: url(http://greenhopping.s3.amazonaws.com/images/userAvatar.jpg)"
			<?php
			}else {
			?>
			  style="background-image: url({{ Auth::user()->image }})"
			<?php
			}
			?>
			>
			  <!--<img src="http://greenhopping.s3.amazonaws.com/images/userAvatar.jpg">-->
			  <!--<img src="{{ Auth::user()->image }}" style="height:100px;width:100px">-->
			
			</span>
			<input type="file" / style="display:none">
		  <!--  <div class="icon-wrapper">-->
		  <!--	<i class="fa fa-camera fa-5x"></i>-->
		  <!--  </div>-->
		  </div>
	  </div>
	  <input type="hidden" value="" id="image_val" name="image_val" />
	  <h4 class="profileName">{{ Auth::user()->username }}</h4>
	  <div>
		<button class="user-btn" id='showEdit'>Edit</button>
		<button class="user-btn" id='showPassword'>Change password</button>
	  </div>
	</div>
	  <span id='addStore-profile_image' class='error-msg'>{{ $errors->first('image', ':message') }}</span>
		<span id='profileLeft' style=''>
		  <span id='editPanel'>
			<form id="newAddressNew" name="newAddressNew" class="profileForm">
			  <p>
				<label><font style="font-weight: 100">Your fullname</font></label>
				<input type="text" value="{{ Auth::user()->username }}" id="fullNames" class="in" name="fullNames" placeholder='Full Name' maxlength="25">
				<span id='fullNamesValidate'></span>
			  </p>
			  <p>
				<label><font style="font-weight: 100">Email</font></label>
				<input type="text" value="{{ Auth::user()->email }}" disabled=""  class="in" >
				<input type="hidden" value="{{ Auth::user()->id }}" name="userId" id="userId"/>
			  </p>
			  <p>
				<label>Your default location</label>
				<select name="location" class="select-top" id='userLocation'>
				  @foreach($cities as $city)
				  <option value="{{ $city->id }}" id="{{ $city->id }}" <?php if(Auth::user()->default_city==$city->id){ echo "selected"; } ?> >{{ $city->name }}</option>
				  @endforeach
				</select>
			  </p>
			  <button type="button" class="user-btn" id="updateProfileData">Update </button>
			</form>
		  </span>
		  <span id='passwordPanel' style='display:none'>
			<form id="changePasswordForm" name="changePasswordForm">
				<input type="password" id="OldPassword" name="OldPassword" class="in" placeholder='Old Password' maxlength="20"><span id='OldPasswordValidate' class='error'></span>
				<span id='fullNamesValidate'></span>
				<input type="password" id="NewPassword" name="NewPassword" class="in" placeholder='New Password' maxlength="20"><span id='NewPasswordValidate' class='error'></span>
				<span id='addressValValidate'></span>
				<input type="password" id="NewPassword_confirmation" name="NewPassword_confirmation" class="in" maxlength="20" placeholder='Confirm Password' class='error'><span id='NewPassword_confirmationValidate'></span>
				<span id='addressValValidate'></span>
				<button type="button" class="user-btn" id="changePassword" >Change Password</button>
				<input type="hidden" id="addr_id" value="">
			</form>
		  </span>
		</span>
	  <ul class="user-list">
		<li><a href="#" id="showUserAddress">Address</a></li>
		<li><a href="#" id="showPaymentAddress">Payment Settings</a></li>
		<li><a href="#" id="showPersonalAddress">Personal Preference</a></li>
	  </ul>
	</div>
	<div class="col-lg-6 user" style="padding-top:0px;" id="userAddressArea">
	  <div class="profileDetails">
		<!--<span id='profileUpdatedSuccessEdit'></span>    -->
		<h6>Shipping Address</h6>
		<span id='userAddressData'></span>
		<div class="new-address">
		  <h6>
			<b  id="showAddressPanel">Add a new address</b>
			<span>Be sure to click "Deliver to this address" when you've finished.</span>
		  </h6>
		  <div id='showAddressArea'>
			<form id="newAddress" name="newAddress">
			  <input type="text" id="fullName" maxlength="30" name="fullName" class="in" placeholder='Full Name'><span id='fullNameValidate'></span>
			  <span id='fullNameValidate'></span>
			  <input type="text" id="addressVal" maxlength="30" name="addressVal" class="in" placeholder='Address'><span id='addressValValidate'></span>
			  <span id='addressValValidate'></span>
			  <input type="text" id="city" name="city"  maxlength="30" class="in" placeholder='City'><span id='cityValidate'></span>
			  <span id='cityValidate'></span>
			  <input type="text" id="state" class="in" maxlength="30" name="state" placeholder='State'><span id='stateValidate'></span>
			  <span id='stateValidate'></span>
			  <input type="text" id="pincode" class="in" maxlength="6"  name="pincode" placeholder='Zip Code'><span id='pincodeValidate'></span>
			  <span id='pincodeValidate'></span>
			  <input type="text" id="country" class="in" maxlength="10" name="country" placeholder='Country'><span id='countryValidate'></span>
			  <span id='countryValidate'></span>
			  <div class="clearfix">
				<button type="button" class="updateAddress user-btn" id="updateAddress" >Add Address</button>
				<button type="button" class="DefaultupdateAddress user-btn" id="DefaultupdateAddress">Deliver to this Address</button>
				<button type="button" class="updateAddressItem user-btn" id=""  style="display:none">Update Address</button>
			  </div>
			  <input type="hidden" id="addr_id" value="">
			</form>
		  </div>
		</div>
	  </div>
	</div>

	<div class="col-lg-6 user" id="userPaymentArea">
	  <div class="new-address"><b>Update credit card number </b>
	  <?php
	  if(Auth::user()->stripe_card=='')
	  {
	  ?>
		<span id="showAddCard">
		  <!--<h4>Add Card</h4>-->
		</span>
	  <?php
	  }
	  else
	  {
	  ?>
  
		<span id="editCardArea" class="showCard">
		  <span class="payment-errors"></span>
		  <form action="" method="POST" id="update-payment-form" class="addCard">
			<input type="hidden" id="user_id" value="{{ Auth::user()->id }}">
			<input type="text" size="20" data-stripe="number" class="pay-box" placeholder="XXXX-XXXX-XXXX-XXXX">
			<input type="text" size="2" data-stripe="exp-month" class="pay-box1" placeholder="MM">
			<input type="text" size="4" data-stripe="exp-year" class="pay-box1" placeholder="YY"> 
			<input type="text" size="4" data-stripe="cvc" class="pay-box2" placeholder="CVV">
			<button type="submit" class="user-btn">Update </button>
		  </form>
		</span>
		<span id="updateCardArea">
		  <!--<img src="public/static/delete.png" height="15px" class="close closebtn" id="64">-->
		  <a href="#" class="closed closebtn" id="64">&times;</a>
		  <strong>
			<span id="cardNumber"></span>
			<em class="editCard" id="64">
			  <a href="#">Edit</a>
			</em>
		  </strong>
		</span>
	  <?php
	  }
	  ?>
  
	  <span id="addCardArea" class="">
		<form action="" method="POST" id="payment-form" class="addCard">
		  <input type="hidden" id="user_id" value="{{ Auth::user()->id }}">
		  <input type="text" size="20" data-stripe="number" class="pay-box" placeholder="XXXX-XXXX-XXXX-XXXX">
		  <input type="text" size="2" data-stripe="exp-month" class="pay-box1" placeholder="MM">
		  <input type="text" size="4" data-stripe="exp-year" class="pay-box1" placeholder="YY"> 
		  <input type="text" size="4" data-stripe="cvc" class="pay-box2" placeholder="CVV">
		  <button type="submit" class="user-btn">Add</button>
		</form>
	  </span>
	</div>
  </div>

  <div class="col-lg-6 user" id="userPersonalArea">
	<b>My Preference</b>
	@foreach($category as $cat)
	  <?php 
	  $catExist = UserCategory::where('user_id', Auth::user()->id)->where('category_id', $cat->id)->first();
	  if($catExist)
	  {
		$class = 'selected';
	  }
	  else
	  {
		$class = '';
	  }
	  ?>
	  <span style="background-image: url(<?php echo $cat->image ?>)" class='imgCat regular <?php echo $class ?>' id='<?php echo $cat->id ?>'></span>
	@endforeach
  </div>
	
</div>
</div>
</div>  
</div>
</div>
<!-- /.container -->
    
<script>
$(document).ready(function(){
clearAllPanel();
$("#userAddressArea").show();
  $("#showUserAddress").click(function(){
	clearAllPanel();
	$(this).parent().addClass('active');
	$("#userAddressArea").show();
  });

  $("#showPaymentAddress").click(function(){
	clearAllPanel();
	$(this).parent().addClass('active');
	$("#userPaymentArea").show();
  });

  $("#showPersonalAddress").click(function() {
	clearAllPanel();
	$(this).parent().addClass('active');
	$("#userPersonalArea").show();
  });

  function clearAllPanel() {
	$("#showUserAddress, #showPaymentAddress, #showPersonalAddress").parent().removeClass('active');
    $("#userAddressArea, #userPaymentArea, #userPersonalArea").hide();
  }

});
</script>

<script>
$(document).ready(function() {
$(".imgCat").click(function(){
$(this).toggleClass('selected').toggleClass('');
    $.ajax({
    url: 'updateUserCategory',
    type : 'POST',  
     data: {  user_id :  $("#user_id").val(), category_id :  this.id },
    success: function(message)
    {
    if(message.success==1) {   } else {  }  
  }
    });


});
});
</script>


<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
$(document).ready(function() {

$("#showEdit").click(function(){
$('#editPanel').show();
$('#passwordPanel').hide();
});
$("#showPassword").click(function(){
$('#editPanel').hide();
$('#passwordPanel').show();
});

//Regular Initiate

    Stripe.setPublishableKey('pk_test_9chBKqWyA0WjWnvRwP4AGT5W');

    $('#payment-form').submit(function(e) {
        var $form = $(this);
        Stripe.card.createToken($form, stripeResponseHandler);
        return false;
      });

      $('#update-payment-form').submit(function(e) {
        var $form = $(this);
        Stripe.card.createToken($form, stripeResponseHandlerUpdate);
        return false;
      });


    $("#showAddCard").click(function(){
      console.log('shownhide');
      //$("#addCardArea").toggleClass("showCard");
    });

    $(".editCard").click(function(){
      console.log('editCard');
      $("#updateCardArea").hide();
      $("#editCardArea").toggleClass("showCard");
    });

   

    var stripeResponseHandler = function(status, response) {
      var $form = $('#payment-form');
      if (response.error) {
        $form.find('.payment-errors').text(response.error.message);
      } else {
        console.log(response.card.id);        
        var token = response.id;
         $.ajax({
    url: 'addCard',
    type : 'POST',  
     data: {  user_id :  $("#user_id").val(), token : token, card : response.card.id},
    success: function(message)
    {
    if(message.success==1)
        {
          location.reload();

          $("#addCardArea").toggleClass("showCard");     
          $("#cardAdd").html('<div class="alert alert-success fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><i class="fa fa-check-circle fa-fw fa-lg"></i>Card Added Succesfully</div>');        
          $("#cardAdd").show();
          setTimeout(function(){ $("#cardAdd").html('');  $("#cardAdd").hide();  }, 2000);
          $("#showAddCard").show();

          $("#showUpdateCard").show();
          $("#updateCardArea").show();

        }
        else
        {
        } 
    }
    }); 
      }
    };

    var stripeResponseHandlerUpdate = function(status, response) {
      var $form = $('#payment-form');
      if (response.error) {
        $form.find('.payment-errors').text(response.error.message);
      } else {
        console.log(response.card.id);  
        var token = response.id;
        $("#loading").show();
        $.ajax({
    url: 'deleteAllCard',
    type : 'POST',  
     data: {  user_id :  $("#user_id").val(), token : token, card : response.card.id},
    success: function(message)
    {
    if(message.success==1)
        {
			$.ajax({
			    url: 'addCard',
			    type : 'POST',  
			     data: {  user_id :  $("#user_id").val(), token : token, card : response.card.id},
			    success: function(message)
			    {
			    if(message.success==1)
			        {
			        	$("#loading").hide();
			          location.reload();
			        }
			        else
			        {
			        } 
			    }
			}); 
        }
        else
        {
        } 
    }
    }); 
      }
    };
  });
</script>

<?php if(Auth::user()->stripe_card=='') {  ?>

<?php } else { ?>

<script type="text/javascript">
$(document).ready(function() {
  $("#addCardArea").hide();
  if (localStorage.getItem("cardNumber") === null) {
  	listCard();	
}
else
{
	listCard();	
	var card = localStorage.getItem('cardNumber');
$('#cardNumber').html('XXXX-XXXX-XXXX-'+card);
}
  
  function listCard()
{
  $.ajax({
    url: 'listCard',
    type : 'POST',  
     data: {  id :  $("#user_id").val()},
    success: function(message)
    {
    if(message.success==1)
        {
          $.each(message.card.data, function(k, v) {
          console.log(k);
          
              $('#cardNumber').html('XXXX-XXXX-XXXX-'+v.last4);

              localStorage.setItem("cardNumber", v.last4);

          });
        }
        else
        {
            
        } 
    }
    });
}
});
</script>

<?php } ?>

<script>
$(document).ready(function() {




$(".closed").click(function(){

    var r = confirm("Are you sure you want to delete this Card ?");
    if (r == true) {
            	
    } else {
        return false;
    }

     $.ajax({
    url: 'deleteCard',
    type : 'POST',  
     data: {  id :  $("#user_id").val()},
    success: function(message)
    {
    if(message.success==1)
        {
          location.reload();
        }
        else
        {
            
        } 
    }
    });

});

});
</script>


<style type="text/css">
.in
{
   border: 0;
    outline: 0;
    background: transparent;
    border-bottom: 1px solid #CCCCCC;
    width: 90%;    
}
.opens
{
  display: none
}
.showCard
  {
    display: none !important;
  }
</style>




@stop