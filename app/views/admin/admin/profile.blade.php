@extends('layout.admin')
@section('content')
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=true&libraries=places"></script>

<!-- Country Code Picker -->







<!-- End of Country Code Picker -->



<div id="content-wrapper" style="background-color:white">
<div class="row">
<div class="col-lg-12">
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li><a href="#">Home</a></li>
<li><a href="#">Profile</a></li>
<li class="active"><span>Edit Profile</span></li>




</ol>




</div>
</div>
<div class="row">
<div class="col-lg-6">
<div class="main-box">
<header class="main-box-header clearfix">
<h2>Basic Information</h2>
</header>
{{ Form::open(array('url' => 'admin/profileUpdate', 'name' => 'addStore', 'id' => 'addStore', 'role' => 'form'))}}
<input type="hidden" name="id" value="{{ Auth::user()->id }}">
<div class="main-box-body clearfix">
@if(Session::has('message'))
<span class="error-msg">
<h5>{{ Session::get('message') }}</h5>
</span>
@endif
<div class="form-group">
<input type="text" class="form-control" id="name" placeholder="Name" maxlength="25" name="username"   value="<?php if(Input::old('username')=='') { echo $data->username; } else { echo Input::old('username'); }  ?>">
<span id='addStore-name' class='error-msg'>{{ $errors->first('username', ':message') }}</span>
</div>


<div class="form-group" style="display:none">
<label for="exampleInputEmail1">Profile Picture</label>
<input type="file" class="form-control" placeholder="Profile Picture" name="profile_image" id="profile_image">
<span id='addStore-profile_image' class='error-msg'>{{ $errors->first('profile_image', ':message') }}</span>
</div>

<div class="form-group" style="display:none">
<label for="exampleInputEmail1">Cover Picture</label>
<input type="file" class="form-control"  placeholder="Cover Picture" id="cover_image" name="cover_image"> 
<span id='addStore-cover_image' class='error-msg'>{{ $errors->first('cover_image', ':message') }}</span>
</div>





</div>
</div>
</div>
<div class="col-lg-6">
<div class="main-box">
<header class="main-box-header clearfix">
<h2>Contact Information</h2>
<span style='float:right'>
<a href="{{ URL::to('admin/changepassword') }}">Change Password </a>
</span>


</header>
<div class="main-box-body clearfix">

<div class="form-group">
<input type="text" class="form-control" placeholder='Email Address' name="email" id="email"  maxlength="30" value="<?php if(Input::old('email')=='') { echo $data->email; } else { echo Input::old('email'); }  ?>" disabled>
<span id='addStore-email' class='error-msg'>{{ $errors->first('email', ':message') }}</span>
</div>



<div class="form-group" style="display:none"> 

<input type="text" class="form-control" id="mobile" name="mobile" placeholder='Mobile Number' value="<?php if(Input::old('mobile')=='') { echo $data->mobile; } else { echo Input::old('mobile'); }  ?>">
</div>
<span id='addStore-mobile' class='error-msg'>{{ $errors->first('mobile', ':message') }}</span>

<div class="form-group" style="display:none">

<input type="text" class="form-control" id="phone" name="phone" placeholder='Phone Number' value="<?php if(Input::old('phone')=='') { echo $data->phone; } else { echo Input::old('phone'); }  ?>">
</div>
<span id='addStore-phone' class='error-msg'>{{ $errors->first('phone', ':message') }}</span>
	



</div>
</div>
</div>




<input type="hidden" name="profile_image" value="2">
<input type="hidden" name="cover_image" value="1">

</div>

<center>
<button type="submit" class="btn btn-success" id="createStore">Update Profile</button>
<button type="submit" class="btn btn-danger">Cancel</button>
</center>
{{ Form::close() }}
<div class="row" style="display:none">
<div class="col-lg-12">
<div class="main-box">
<br>
<div class="main-box-body clearfix">
<div class="row">
<div class="col-lg-12">

<center>
<button type="submit" class="btn btn-success">Add Store</button>
</center>
<div  style="display:none">

<div class="onoffswitch">
<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" checked="">
<label class="onoffswitch-label" for="myonoffswitch">
<div class="onoffswitch-inner"></div>

<div class="onoffswitch-switch"></div>

</label>

</div>

</div>




</div>
</div>
</div>
</div>
</div>
</div>



</div>

<script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('address'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var mesg = "address: " + address;


                $.ajax({
           type: "POST",
           url : "googleplace",
           data: {address: address},
           success : function(data){
          console.log(data);
          arr = data.split(',');
          var latitude = arr[0];
          var longitude = arr[1];
          console.log(arr[0]);
          console.log(arr[1]);
          $('#latitude').val(latitude);
        $('#longitude').val(longitude);
          }
        });

          
            });
        });
    </script>
<script>
    $(document).ready(function() {

        $('#mobile').mask('(+1) 999-999-9999');
$('#phone').mask('(+1) 99999999999');
        });
</script>





@if(Session::has('createCat'))

<script>
$(document).ready(function() {
$( "#dialog" ).dialog({
    autoOpen: true,
    width: 400,
    buttons: [
        {
            text: "Yes",
            click: function() {
                $( this ).dialog( "close" );
                window.location.href  = $.cookie('ghDomain')+'/admin/product/'+{{ Session::get('id') }};
                
            }
        },
        {
            text: "Later",
            click: function() {
                $( this ).dialog( "close" );
            }
        }
    ]
});

// Link to open the dialog
$( "#dialog-link" ).click(function( event ) {
    $( "#dialog" ).dialog( "open" );
    event.preventDefault();
});

});
</script>
<!-- ui-dialog -->
<div id="dialog" title="Shop created Successfully">
    <p>Do you want to add Categories / Juices to the created Store ?</p>
</div>
@endif

@stop