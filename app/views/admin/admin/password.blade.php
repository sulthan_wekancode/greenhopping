@extends('layout.admin')
@section('content')
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=true&libraries=places"></script>

<!-- Country Code Picker -->







<!-- End of Country Code Picker -->



<div id="content-wrapper" style="background-color:white">
<div class="row">
<div class="col-lg-12">
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li><a href="#">Home</a></li>
<li><a href="#">Password</a></li>
<li class="active"><span>Change Password</span></li>




</ol>




</div>
</div>
<div class="row">
<div class="col-lg-6">
<div class="main-box">
<header class="main-box-header clearfix">
<h2>Change Password</h2>
</header>
{{ Form::open(array('url' => 'admin/changepasswordprocess', 'id' => 'changepasswordprocess')) }}
<div class="main-box-body clearfix">
@if(Session::has('message'))
<span class="error-msg">
<h5>{{ Session::get('message') }}</h5>
</span>
<script>
    $(document).ready(function() {

        $('#changepasswordprocess').trigger('reset');
        });
</script>

@endif
<div class="form-group">
<input type="password" class="form-control" id="OldPassword" placeholder="Old Password" maxlength="25" name="OldPassword"  value="{{Input::old('OldPassword') }}">
<span id='addStore-name' class='error-msg'>{{ $errors->first('OldPassword', ':message') }}</span>
</div>
<div class="form-group">
<input type="password" class="form-control" id="name" placeholder="New Passsword" maxlength="25" name="NewPassword" value="{{Input::old('NewPassword') }}" >
<span id='addStore-name' class='error-msg'>{{ $errors->first('NewPassword', ':message') }}</span>
</div>
<div class="form-group">
<input type="password" class="form-control" id="name" placeholder="Confirm New Password" maxlength="25" name="NewPassword_confirmation" value="{{Input::old('NewPassword_confirmation') }}">
<span id='addStore-name' class='error-msg'>{{ $errors->first('NewPassword_confirmation', ':message') }}</span>
</div>






</div>
</div>
</div>





<input type="hidden" name="profile_image" value="2">
<input type="hidden" name="cover_image" value="1">

</div>

<center>
<button type="submit" class="btn btn-success" id="createStore">Update Password</button>
<button type="submit" class="btn btn-danger">Cancel</button>
</center>
{{ Form::close() }}
<div class="row" style="display:none">
<div class="col-lg-12">
<div class="main-box">
<br>
<div class="main-box-body clearfix">
<div class="row">
<div class="col-lg-12">

<center>
<button type="submit" class="btn btn-success">Add Store</button>
</center>
<div  style="display:none">

<div class="onoffswitch">
<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" checked="">
<label class="onoffswitch-label" for="myonoffswitch">
<div class="onoffswitch-inner"></div>

<div class="onoffswitch-switch"></div>

</label>

</div>

</div>




</div>
</div>
</div>
</div>
</div>
</div>



</div>

<script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('address'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var mesg = "address: " + address;


                $.ajax({
           type: "POST",
           url : "googleplace",
           data: {address: address},
           success : function(data){
          console.log(data);
          arr = data.split(',');
          var latitude = arr[0];
          var longitude = arr[1];
          console.log(arr[0]);
          console.log(arr[1]);
          $('#latitude').val(latitude);
        $('#longitude').val(longitude);
          }
        });

          
            });
        });
    </script>
<script>
    $(document).ready(function() {

        $('#mobile').mask('(+1) 999-999-9999');
$('#phone').mask('(+1) 99999999999');
        });
</script>





@if(Session::has('createCat'))

<script>
$(document).ready(function() {
$( "#dialog" ).dialog({
    autoOpen: true,
    width: 400,
    buttons: [
        {
            text: "Yes",
            click: function() {
                $( this ).dialog( "close" );
                window.location.href  = $.cookie('ghDomain')+'/admin/product/'+{{ Session::get('id') }};
                
            }
        },
        {
            text: "Later",
            click: function() {
                $( this ).dialog( "close" );
            }
        }
    ]
});

// Link to open the dialog
$( "#dialog-link" ).click(function( event ) {
    $( "#dialog" ).dialog( "open" );
    event.preventDefault();
});

});
</script>
<!-- ui-dialog -->
<div id="dialog" title="Shop created Successfully">
    <p>Do you want to add Categories / Juices to the created Store ?</p>
</div>
@endif

@stop