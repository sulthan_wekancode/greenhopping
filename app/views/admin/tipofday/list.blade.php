@extends('layout.admin')
@section('content')
<div id="content-wrapper">
<div class="row">
<div class="col-lg-12">
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li><a href="#">Home</a></li>
<li class="active"><span>Tip of Day</span></li>
</ol>
</div>
</div>

<div class="row">
<div class="col-lg-12">
<div class="main-box clearfix">
<header class="main-box-header clearfix">
<h2 class="pull-left">Tips List</h2>
<div class="filter-block pull-right">

<a href="{{ URL::to('admin/addtipofday') }}" class="btn btn-primary pull-right">
<i class="fa fa-plus-circle fa-lg"></i> Add Tip of Day
</a>
</div>
</header>
<div class="main-box-body clearfix">
<div class="table-responsive">
<table class="table" id="dataTables-example">
<thead>
<tr>
<th><span>Tip Content</span></th>
<th><span>Created at</span></th>
<th><span>Action   </span></th>
</tr>
</thead>
<tbody>
@foreach($stores as $store)
<tr>
<td >
<?php

$string = substr($store->content,0,20).'...';
?>
{{ $string  }}
</td>

<td >
<?php
$date = $store->date;
$newDate = date("m-d-Y", strtotime($date));
?>
{{ $newDate; }}
</td>
<td >

<a href="{{ URL::to('admin/edittip').'/'.$store->id }}" class="table-link">
<span class="fa-stack">
<i class="fa fa-square fa-stack-2x"></i>
<i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
</span>
</a>
<a href='#' class="delTip table-link danger" id="{{ $store->id }}">
<span class="fa-stack">
<i class="fa fa-square fa-stack-2x"></i>
<i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
</span>
</a>
</td>

</tr>

@endforeach
</tbody>
</table>
</div>
<ul class="pagination pull-right">
<li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
<li><a href="#">1</a></li>
<li><a href="#">2</a></li>
<li><a href="#">3</a></li>
<li><a href="#">4</a></li>
<li><a href="#">5</a></li>
<li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
</ul>
</div>
</div>
</div>
</div>


</div>
</div>


<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>



<script>
$(document).ready(function() {
$( "#dialog" ).dialog({
    autoOpen: false,
    width: 400,
    buttons: [
        {
            text: "Yes",
            click: function() {
                $( this ).dialog( "close" );
                var delStoreId = localStorage.getItem("delTip");
                window.location.href  = $.cookie('ghDomain')+'/admin/delTip/'+delStoreId;
                
            }
        },
        {
            text: "Later",
            click: function() {
                $( this ).dialog( "close" );
            }
        }
    ]
});

// Link to open the dialog
$( ".delTip" ).click(function( event ) {
    localStorage.setItem("delTip", this.id);
    $( "#dialog" ).dialog( "open" );
    event.preventDefault();
});

});
</script>
<!-- ui-dialog -->
<div id="dialog" title="Delete Tip">
    <p><b>Do you want to delete this Tip ?</b><br></p>
</div>


@stop