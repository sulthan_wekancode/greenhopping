@extends('layout.admin')
@section('content')

<script src="{{ asset('/').('public/asset/ajaxupload.3.5.js') }}"></script>
<script src="{{ asset('/').('public/asset/s3_admin_tip.js') }}"></script>

<div id="content-wrapper" style="background-color:white">
<div class="row">
<div class="col-lg-12">
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li><a href="#">Home</a></li>
<li class="active"><span>Tip of Day</span></li>
</ol>
</div>
</div>
<div class="row">
<div class="col-lg-6">
<div class="main-box">
<header class="main-box-header clearfix">
<h2>Main Information</h2>
</header>
<div class="main-box-body clearfix">
{{ Form::open(array('url' => 'admin/updateTip', 'name' => 'addStore', 'id' => 'addStore', 'role' => 'form'))}}
<input type="hidden" value="{{$store->id}}" name="id">
@if(Session::has('message'))
<span class="error-msg">
<h5>{{ Session::get('message') }}</h5>
</span>
@endif
@if(Session::has('s-message'))
<span class="error-msg" style='color:green'>
<h5><b>{{ Session::get('s-message') }}</b></h5>
</span>
@endif
<div class="form-group">

<textarea class="form-control" id="exampleTextarea" rows="3" name="content" placeholder="Content" > {{ $store->content }}</textarea>
<span class='error-msg'>{{ $errors->first('content', ':message') }}</span>
</div>

<div class="form-group" style="">
<label for="exampleInputEmail1">Tip Picture</label>
<br>
<img src="{{ $store->image }}" style="height:50px;width:50px">

<a href="javascript:;" id="tip_pic" style=" font-size:15px; font-weight:bold; padding:3px; text-decoration:none">Browse Files & Upload</a> 

<input type="hidden"id="tip_image" name="image" value="{{ $store->image }}">
<span id="statuss"></span>
<div id="tip_cover"></div>
<span id='addStore-profile_image' class='error-msg'>{{ $errors->first('image', ':message') }}</span>	
</div>







</div>
</div>
</div>
<div class="col-lg-6">
<div class="main-box">
<header class="main-box-header clearfix">
<h2>Post Day</h2>
</header>
<div class="main-box-body clearfix">



<div class="col-lg-6">
<div class="row">
<div class="form-group col-md-12">
<label for="datepickerDate">Date</label>
<div class="input-group">
<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
<?php
$date = $store->date;
$newDate = date("m-d-Y", strtotime($date));
?>

<input type="text" class="form-control" id="datepickerDate" name="date" value="{{ $newDate }}">
</div>

<span class='error-msg'>{{ $errors->first('date', ':message') }}</span>
</div>
</div>

</div>



</div>
</div>
</div>






</div>

<center>
<button type="submit" class="btn btn-success" id="createStore">Submit</button>
<button type="submit" class="btn btn-danger">Cancel</button>
</center>
</form>




</div>
<script>
$(document).ready(function() {
	
	$('#datepickerDate').mask('99-99-9999');
});
</script>
@stop