@extends('layout.admin')
@section('content')
<div id="content-wrapper">
<div class="row">
<div class="col-lg-12">
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li><a href="#">Home</a></li>
<li class="active"><span>Users</span></li>
</ol>
</div>
</div>

<div class="row">
<div class="col-lg-12">
<div class="main-box clearfix">
<header class="main-box-header clearfix">
<h2 class="pull-left">Users List</h2>
<div class="filter-block pull-right">

</div>
</header>
<div class="main-box-body clearfix">
<div class="table-responsive">
<table class="table" id="dataTables-example">
<thead>
<tr>
<th>Name</th>
<th><span>Email</span></th>
<th><span>Registered At</span></th>
<th style="display:none" ><span>Action</span></th>
</tr>
</thead>
<tbody>
@foreach($stores as $store)
<tr>
<td >
{{ $store->username }}
</td>
<td >
{{ $store->email }}
</td>
<td >
{{ $store->created_at }}
</td>
<td style="display:none">
<span >
<a href="{{ URL::to('admin/viewuser').'/'.$store->id }}" class="table-link" disabled='disabled'>
<span class="fa-stack">
<i class="fa fa-square fa-stack-2x"></i>
<i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
</span>
</a>

</span>
</td>

</tr>

@endforeach
</tbody>
</table>
</div>
<ul class="pagination pull-right">
<li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
<li><a href="#">1</a></li>
<li><a href="#">2</a></li>
<li><a href="#">3</a></li>
<li><a href="#">4</a></li>
<li><a href="#">5</a></li>
<li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
</ul>
</div>
</div>
</div>
</div>


</div>
</div>


<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>



<script>
$(document).ready(function() {
$( "#dialog" ).dialog({
    autoOpen: false,
    width: 400,
    buttons: [
        {
            text: "Yes",
            click: function() {
                $( this ).dialog( "close" );
                var delStoreId = localStorage.getItem("delTip");
                window.location.href  = $.cookie('ghDomain')+'/admin/delTip/'+delStoreId;
                
            }
        },
        {
            text: "Later",
            click: function() {
                $( this ).dialog( "close" );
            }
        }
    ]
});

// Link to open the dialog
$( ".delTip" ).click(function( event ) {
    localStorage.setItem("delTip", this.id);
    $( "#dialog" ).dialog( "open" );
    event.preventDefault();
});

});
</script>
<!-- ui-dialog -->
<div id="dialog" title="Delete Store">
    <p><b>Do you want to delete this Tip ?</b><br></p>
</div>


@stop