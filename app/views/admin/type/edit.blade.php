@extends('layout.admin')
@section('content')

<script src="{{ asset('/').('public/asset/ajaxupload.3.5.js') }}"></script>
<script src="{{ asset('/').('public/asset/s3_admin_tip.js') }}"></script>

<div id="content-wrapper" style="background-color:white">
<div class="row">
<div class="col-lg-12">
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li><a href="#">Home</a></li>
<li class="active"><span>Type</span></li>
</ol>
</div>
</div>
<div class="row">
<div class="col-lg-6">
<div class="main-box">
<header class="main-box-header clearfix">
<h2>Category Type Information </h2>
</header>
<div class="main-box-body clearfix">
{{ Form::open(array('url' => 'admin/updateType', 'name' => 'addStore', 'id' => 'addStore', 'role' => 'form'))}}
<input type="hidden" value="{{$store->id}}" name="id">
@if(Session::has('message'))
<span class="error-msg">
<h5>{{ Session::get('message') }}</h5>
</span>
@endif
@if(Session::has('s-message'))
<span class="error-msg" style='color:green'>
<h5><b>{{ Session::get('s-message') }}</b></h5>
</span>
@endif
<div class="form-group">

<input type="text" class="form-control" id="name" placeholder="Name" maxlength="25" name="name"   value="{{ $store->name }}">
<span class='error-msg'>{{ $errors->first('name', ':message') }}</span>
</div>








</div>
</div>
</div>
<div class="col-lg-6">
<div class="main-box">

<div class="main-box-body clearfix">


<h2>Category Type Picture</h2>
<div class="col-lg-6">
<div class="form-group" style="">

<br>




<a href="javascript:;" id="tip_pic" style=" font-size:15px; font-weight:bold; padding:3px; text-decoration:none">Browse Files & Upload</a> 

<input type="hidden"id="tip_image" name="image" value="{{ $store->image }}">
<span id="statuss"></span>
<div id="tip_cover"><img src="{{ $store->image }}" style="height:50px;width:50px"></div>
<span id='addStore-profile_image' class='error-msg'>{{ $errors->first('image', ':message') }}</span>	
</div>

</div>



</div>
</div>
</div>






</div>

<center>
<button type="submit" class="btn btn-success" id="createStore">Submit</button>
<button type="submit" class="btn btn-danger">Cancel</button>
</center>
</form>




</div>
<script>
$(document).ready(function() {
	
	$('#datepickerDate').mask('99-99-9999');
});
</script>
@stop