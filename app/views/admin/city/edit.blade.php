@extends('layout.admin')
@section('content')
<div id="content-wrapper" style="background-color:white">
<div class="row">
<div class="col-lg-12">
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li><a href="#">Home</a></li>
<li><a href="#">City</a></li>
<li class="active"><span>Edit City</span></li>
</ol>
</div>
</div>
<div class="row">
<div class="col-lg-6">
<div class="main-box">
<header class="main-box-header clearfix">
<h2>City Information</h2>
</header>
<div class="main-box-body clearfix">
{{ Form::open(array('url' => 'admin/updateCity', 'name' => 'addStore', 'id' => 'addStore', 'role' => 'form'))}}
<input type="hidden" value="{{$store->id}}" name="id">
@if(Session::has('message'))
<span class="error-msg">
<h5>{{ Session::get('message') }}</h5>
</span>
@endif
@if(Session::has('s-message'))
<span class="error-msg" style='color:green'>
<h5><b>{{ Session::get('s-message') }}</b></h5>
</span>
@endif
<div class="form-group">
<input type="text" class="form-control" name="name" value="{{ $store->name}}" maxlength="15" placeholder="City Name">

<span class='error-msg'>{{ $errors->first('name', ':message') }}</span>
</div>

<div class="form-group" style="display:none">
<label for="exampleInputEmail1">Profile Picture</label>
<input type="file" class="form-control" id="exampleInputEmail1" placeholder="Pincode">
</div>







</div>
</div>
</div>
<div class="col-lg-6">
<div class="main-box">
<header class="main-box-header clearfix">
<h2>Other Information</h2>
</header>
<div class="main-box-body clearfix">



<div class="col-lg-6">
<div class="row">
<div class="form-group col-md-12">

<div class="input-group">

<label for="exampleInputEmail1">City Status</label>
<div class="form-group">
<select class="form-control" name="status" id="status">
<option disabled="">Status</option>
<option value="1" <?php if($store->status==1) { echo 'selected'; }?>>Active</option>
<option value="0" <?php if($store->status==0) { echo 'selected'; }?>>Inactive</option>
</select>
<span id='error_cat_status'></span>
</div>
</div>

<span class='error-msg'>{{ $errors->first('status	', ':message') }}</span>
</div>
</div>

</div>



</div>
</div>
</div>






</div>

<center>
<button type="submit" class="btn btn-success" id="createStore">Submit</button>
<button type="submit" class="btn btn-danger">Cancel</button>
</center>
</form>




</div>

@stop