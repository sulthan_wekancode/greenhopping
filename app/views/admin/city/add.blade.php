@extends('layout.admin')
@section('content')
<div id="content-wrapper" style="background-color:white">
<div class="row">
<div class="col-lg-12">
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li><a href="#">Home</a></li>
<li class="active"><span>City</span></li>
</ol>
</div>
</div>
<div class="row">
<div class="col-lg-6">
<div class="main-box">
<header class="main-box-header clearfix">
<h2>Basic Information</h2>
</header>
<div class="main-box-body clearfix">
{{ Form::open(array('url' => 'admin/addCityData', 'name' => 'addStore', 'id' => 'addStore', 'role' => 'form'))}}
@if(Session::has('message'))
<span class="error-msg">
<h5>{{ Session::get('message') }}</h5>
</span>
@endif
@if(Session::has('s-message'))
<span class="success-msg" style='color:green'>
<h5><b>{{ Session::get('s-message') }}</b></h5>
</span>
@endif
<div class="form-group">

<input type="text" class="form-control" name="name" value="{{ Input::old('name') }}" maxlength="15" placeholder="City Name">

<span class='error-msg'>{{ $errors->first('name', ':message') }}</span>
</div>









</div>
</div>
</div>
<div class="col-lg-6">
<div class="main-box">
<header class="main-box-header clearfix">
<h2>Other Information</h2>
</header>
<div class="main-box-body clearfix">



<div class="col-lg-6">
<div class="row">
<div class="form-group col-md-12">

<div class="input-group">

<label for="exampleInputEmail1">City Status</label>
<div class="form-group">
<select class="form-control" name="status" id="status">
<option disabled="">Status</option>
<option value="1">Active</option>
<option value="0">Inactive</option>
</select>
<span id='error_cat_status'></span>
</div>
</div>

<span class='error-msg'>{{ $errors->first('status	', ':message') }}</span>
</div>
</div>

</div>



</div>
</div>
</div>






</div>

<center>
<button type="submit" class="btn btn-success" id="createStore">Submit</button>
<button type="submit" class="btn btn-danger">Cancel</button>
</center>
</form>




</div>

@stop