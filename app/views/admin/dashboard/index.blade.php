@extends('layout.admin')
@section('content')
<div id="content-wrapper">
<div class="row">
<div class="col-lg-12">
<div class="row">
<div class="col-lg-12">
<div id="content-header" class="clearfix">
<div class="pull-left">
<ol class="breadcrumb">
<li><a href="#">Home</a></li>
<li class="active"><span>Dashboard</span></li>
</ol>
<h1>Dashboard</h1>
</div>
<div class="pull-right hidden-xs">
<div class="xs-graph pull-left">
<div class="graph-label">
<b><i class="fa fa-shopping-cart"></i> 838</b> Orders
</div>
<div class="graph-content spark-orders"></div>
</div>
<div class="xs-graph pull-left mrg-l-lg mrg-r-sm">
<div class="graph-label">
<b>&dollar;12.338</b> Revenues
</div>
<div class="graph-content spark-revenues"></div>
</div>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-lg-3 col-sm-6 col-xs-12">
<div class="main-box infographic-box colored green-bg">
<i class="fa fa-money"></i>
<span class="headline">Orders</span>
<span class="value">22,631</span>
</div>
</div>
<div class="col-lg-3 col-sm-6 col-xs-12">
<div class="main-box infographic-box colored red-bg">
<i class="fa fa-user"></i>
<span class="headline">Users</span>
<span class="value">92,421</span>
</div>
</div>
<div class="col-lg-3 col-sm-6 col-xs-12">
<div class="main-box infographic-box colored purple-bg">
<i class="fa fa-building"></i>
<span class="headline">Stores</span>
<span class="value">45</span>
</div>
</div>
<div class="col-lg-3 col-sm-6 col-xs-12">
<div class="main-box infographic-box colored emerald-bg">
<i class="fa fa-money"></i>
<span class="headline">Payments</span>
<span class="value">4,658</span>
</div>
</div>

</div>
<div class="row">
<div class="col-md-12">
<div class="main-box">
<header class="main-box-header clearfix">
<h2 class="pull-left">Sales &amp; Earnings</h2>
</header>
<div class="main-box-body clearfix">
<div class="row">
<div class="col-md-9">
<div id="graph-bar" style="height: 240px; padding: 0px; position: relative;"></div>
</div>
<div class="col-md-3">
<ul class="graph-stats">
<li>
<div class="clearfix">
<div class="title pull-left">
Earnings
</div>
<div class="value pull-right" title="10% down" data-toggle="tooltip">
&dollar;94.382 <i class="fa fa-level-down red"></i>
</div>
</div>
<div class="progress">
<div style="width: 69%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="69" role="progressbar" class="progress-bar">
<span class="sr-only">69% Complete</span>
</div>
</div>
</li>
<li>
<div class="clearfix">
<div class="title pull-left">
Orders
</div>
<div class="value pull-right" title="24% up" data-toggle="tooltip">
3.930 <i class="fa fa-level-up green"></i>
</div>
</div>
<div class="progress">
<div style="width: 42%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="42" role="progressbar" class="progress-bar progress-bar-danger">
<span class="sr-only">42% Complete</span>
</div>
</div>
</li>
<li>
<div class="clearfix">
<div class="title pull-left">
New Clients
</div>
<div class="value pull-right" title="8% up" data-toggle="tooltip">
894 <i class="fa fa-level-up green"></i>
</div>
</div>
<div class="progress">
<div style="width: 78%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="78" role="progressbar" class="progress-bar progress-bar-success">
<span class="sr-only">78% Complete</span>
</div>
</div>
</li>
<li>
<div class="clearfix">
<div class="title pull-left">
Visitors
</div>
<div class="value pull-right" title="17% down" data-toggle="tooltip">
824.418 <i class="fa fa-level-down red"></i>
</div>
</div>
<div class="progress">
<div style="width: 94%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="94" role="progressbar" class="progress-bar progress-bar-warning">
<span class="sr-only">94% Complete</span>
</div>
</div>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-lg-12">
<div class="main-box clearfix">
<header class="main-box-header clearfix">
<h2 class="pull-left">Last orders</h2>
<div class="filter-block pull-right">
<div class="form-group pull-left">
<input type="text" class="form-control" placeholder="Search...">
<i class="fa fa-search search-icon"></i>
</div>
<a href="#" class="btn btn-primary pull-right">
<i class="fa fa-eye fa-lg"></i> View all orders
</a>
</div>
</header>
<div class="main-box-body clearfix">
<div class="table-responsive clearfix">
<table class="table table-hover">
<thead>
<tr>
<th><a href="#"><span>Order ID</span></a></th>
<th><a href="#" class="desc"><span>Date</span></a></th>
<th><a href="#" class="asc"><span>Customer</span></a></th>
<th class="text-center"><span>Status</span></th>
<th class="text-right"><span>Price</span></th>
<th>&nbsp;</th>
</tr>
</thead>
<tbody>
<tr>
<td>
<a href="#">#8002</a>
</td>
<td>
2013/08/08
</td>
<td>
<a href="#">Robert De Niro</a>
</td>
<td class="text-center">
<span class="label label-success">Completed</span>
</td>
<td class="text-right">
&dollar; 825.50
</td>
<td class="text-center" style="width: 15%;">
<a href="#" class="table-link">
<span class="fa-stack">
<i class="fa fa-square fa-stack-2x"></i>
<i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
</span>
</a>
</td>
</tr>
<tr>
<td>
<a href="#">#5832</a>
</td>
<td>
2013/08/08
</td>
<td>
<a href="#">John Wayne</a>
</td>
<td class="text-center">
<span class="label label-warning">On hold</span>
</td>
<td class="text-right">
&dollar; 923.93
</td>
<td class="text-center" style="width: 15%;">
<a href="#" class="table-link">
<span class="fa-stack">
<i class="fa fa-square fa-stack-2x"></i>
<i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
</span>
</a>
</td>
</tr>
<tr>
<td>
<a href="#">#2547</a>
</td>
<td>
2013/08/08
</td>
<td>
<a href="#">Anthony Hopkins</a>
</td>
<td class="text-center">
<span class="label label-info">Pending</span>
</td>
<td class="text-right">
&dollar; 1.625.50
</td>
<td class="text-center" style="width: 15%;">
<a href="#" class="table-link">
<span class="fa-stack">
<i class="fa fa-square fa-stack-2x"></i>
<i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
</span>
</a>
</td>
</tr>
<tr>
<td>
<a href="#">#9274</a>
</td>
<td>
2013/08/08
</td>
<td>
<a href="#">Charles Chaplin</a>
</td>
<td class="text-center">
<span class="label label-danger">Cancelled</span>
</td>
<td class="text-right">
&dollar; 35.34
</td>
<td class="text-center" style="width: 15%;">
<a href="#" class="table-link">
<span class="fa-stack">
<i class="fa fa-square fa-stack-2x"></i>
<i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
</span>
</a>
</td>
</tr>
<tr>
<td>
<a href="#">#8463</a>
</td>
<td>
2013/08/08
</td>
<td>
<a href="#">Gary Cooper</a>
</td>
<td class="text-center">
<span class="label label-success">Completed</span>
</td>
<td class="text-right">
&dollar; 34.199.99
</td>
<td class="text-center" style="width: 15%;">
<a href="#" class="table-link">
<span class="fa-stack">
<i class="fa fa-square fa-stack-2x"></i>
<i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
</span>
</a>
</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>

</div>
</div>
@stop