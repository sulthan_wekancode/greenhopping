@extends('layout.admin')
@section('content')
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=true&libraries=places"></script>


<script src="{{ asset('/').('public/asset/ajaxupload.3.5.js') }}"></script>
<script src="{{ asset('/').('public/asset/s3_admin_store_cover.js') }}"></script>
<script src="{{ asset('/').('public/asset/s3_admin_store_profile.js') }}"></script>



<style>
.error { font-size:12px; font-weight:bold; }
img { padding:4px; margin:3px; }
#files { margin-top:20px; }
</style>
<!-- Country Code Picker -->







<!-- End of Country Code Picker -->



<div id="content-wrapper" style="background-color:white">
<div class="row">
<div class="col-lg-12">
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li><a href="#">Home</a></li>
<li><a href="#">Store</a></li>
<li class="active"><span>Create Store</span></li>




</ol>




</div>
</div>
<div class="row">
<div class="col-lg-6">
<div class="main-box">
<header class="main-box-header clearfix">
<h2>Basic Information</h2>
</header>
{{ Form::open(array('url' => 'admin/addStoreDetails', 'name' => 'addStore', 'id' => 'addStore', 'role' => 'form'))}}
<div class="main-box-body clearfix">
@if(Session::has('message'))
<span class="error-msg">
<h5>{{ Session::get('message') }}</h5>
</span>
@endif
<div class="form-group">
<input type="text" class="form-control" id="name" placeholder="Name" maxlength="25" name="name"   value="{{Input::old('name') }}">
<span id='addStore-name' class='error-msg'>{{ $errors->first('name', ':message') }}</span>
</div>

<script src="{{ asset('/').('public/css/admin/jquery.js') }}"></script>
<script src="{{ asset('/').('public/css/admin/prism.js') }}"></script>




<link rel="stylesheet" href="{{ asset('/').('public/css/admin/prism.css')}}">
<link rel="stylesheet" href="{{ asset('/').('public/css/admin/chosen.css')}}">


<label>Store Type</label>
<div class="form-group">
   <div class="side-by-side clearfix">        
        <div>
          <select id="typeSelect" name="type[]" data-placeholder="Add Category" style="width:350px;" multiple class="chosen-select-no-results" tabindex="12">
          @foreach($types as $type)
          <option value="{{ $type->id }}" 
          <?php 

          //if(isset(Input::old('type')))
            if(Input::old('type'))

          {


          if(in_array($type->id, Input::old('type'))) { echo 'selected="selected"'; }
        }
          ?>


          >{{ $type->name }} </option>
          @endforeach
          </select>
        </div>
      </div>
        
      </div>
<script type="text/javascript">
    var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
  </script>




<div class="form-group">

<textarea class="form-control" name="address" id="address" rows="3" placeholder="Address" maxlength='100'>{{Input::old('address') }}</textarea>

{{ Form::text('latitude', null,['class' => 'unwant','id' => 'latitude','size' => '100x100']) }}
{{ Form::text('longitude', null,['class' => 'unwant','id' => 'longitude','size' => '100x100']) }}


<span id='addStore-address' class='error-msg'>{{ $errors->first('address', ':message') }}</span>
</div>
<div class="form-group">

<input type="text" id="pincode" name="pincode" class="form-control" id="pincode" placeholder="Zipcode" value="{{Input::old('pincode') }}" maxlength="8">
<span id='addStore-pincode' class='error-msg'>{{ $errors->first('pincode', ':message') }}</span>
</div>

<label for="exampleInputEmail1">City</label>
<div class="form-group">
<select class="form-control" name="city" id="status">
<option value="">Select City</option>
@foreach($cities as $city)
<option value="{{ $city->id }}" <?php 

if(Input::old('city'))
{
  if(Input::old('city')==$city->id)
    echo "selected='selected'";
}

?>>{{ $city->name }}</option>
@endforeach
</select>
<span id='addStore-name' class='error-msg'>{{ $errors->first('city', ':message') }}</span>
</div>
<div class="form-group">
<label for="exampleInputEmail1">Profile Picture</label>
<a href="javascript:;" id="profile_pic" style=" font-size:15px; font-weight:bold; padding:3px; text-decoration:none">Browse Files & Upload</a> 
Size : 233px X 1356px
<input type="hidden" name="imgs_profile" value="" id="imgs_profile" >
<input type="hidden" value="{{ Input::old('profile_image') }}" id="profile_image" name="profile_image" >
<br>
<span id="statuss"></span>
<div id="files_profile">
  
  <?php


    if(Input::old('profile_image'))
    {
      ?>
      <img src="{{ Input::old('profile_image') }}" height="50" width="50" class="img-responsive">
      <?php
    }
  ?>

</div>
<span id='addStore-profile_image' class='error-msg'>{{ $errors->first('profile_image', ':message') }}</span>
</div>
<div class="form-group">
<label for="exampleInputEmail1">Cover Picture</label>
<a href="javascript:;" id="cover_pic" style="font-size:15px; font-weight:bold; padding:3px; text-decoration:none">Browse Files & Upload</a> 
Size : 100px X 100px
<input type="hidden" name="imgs_cover" value="" id="imgs_cover">
<input type="hidden" value="{{ Input::old('cover_image') }}" id="cover_image" name="cover_image">
<br>
<span id="statusss"></span>
<div id="files_cover">
  <?php

    if(Input::old('cover_image'))
    {
      ?>
      <img src="{{ Input::old('cover_image') }}" height="50" width="50" class="img-responsive">
      <?php
    }
  ?>
</div>
<span id='addStore-profile_image' class='error-msg'>{{ $errors->first('cover_image', ':message') }}</span>
</div>
<div class="form-group col-md-6">
<?php $time = ['00:00:00' => '12:00 AM','00:30:00' => '12:30 AM','01:00:00' => '01:00 AM','01:30:00' => '01:30 AM','02:00:00' => '02:00 AM','02:30:00' => '02:30 AM','03:00:00' => '03:00 AM','03:30:00' => '03:30 AM','04:00:00' => '04:00 AM','04:30:00' => '04:30 AM','05:00:00' => '05:00 AM','05:30:00' => '05:30 AM','06:00:00' => '06:00 AM','06:30:00' => '06:30 AM','07:00:00' => '07:00 AM','07:30:00' => '07:30 AM','08:00:00' => '08:00 AM','08:30:00' => '08:30 AM','09:00:00' => '09:00 AM','09:30:00' => '09:30 AM','10:00:00' => '10:00 AM','10:30:00' => '10:30 AM','11:00:00' => '11:00 AM','11:30:00' => '11:30 AM','12:00:00' => '12:00 PM','12:30:00' => '12:30 PM','13:00:00' => '01:00 PM','13:30:00' => '01:30 PM','14:00:00' => '02:00 PM','14:03:00' => '02:30 PM','15:00:00' => '03:00 PM','15:30:00' => '03:30 PM','16:00:00' => '04:00 PM','16:30:00' => '04:30 PM','17:00:00' => '05:00 PM','17:30:00' => '05:30 PM','18:00:00' => '06:00 PM','18:30:00' => '06:30 PM','19:00:00' => '07:00 PM','19:30:00' => '07:30 PM','20:00:00' => '08:00 PM','20:30:00' => '08:30 PM','21:00:00' => '09:00 PM','21:30:00' => '09:30 PM','22:00:00' => '10:00 PM','22:30:00' => '10:30 PM','23:00:00' => '11:00 PM','23:30:00' => '11:30 PM']; ?>

<label for="exampleInputEmail1">Opening Time</label>
<select name="open_time">
<?php foreach ($time as $key => $value) { ?>
  <option value="{{ $key }}" <?php if(Input::old('open_time')==$key){ echo 'selected'; }?>>{{  $value }}</option>
<?php } ?>
</select>
<span id='addStore-name' class='error-msg'>{{ $errors->first('open_time', ':message') }}</span>
</div>
<div class="form-group col-md-6">
<label for="exampleInputEmail1">Closing Time</label>
<select name="close_time">
<?php foreach ($time as $key => $value) { ?>
  <option value="{{$key }}" <?php if(Input::old('close_time')==$key){ echo 'selected'; }?>>{{  $value }}</option>
<?php } ?>
</select>
<span id='addStore-name' class='error-msg'>{{ $errors->first('close_time', ':message') }}</span>
</div>



<br>

<label for="exampleInputEmail1">Store Status</label>
<div class="form-group">
<select class="form-control" name="status" id="status">
<option disabled="">Status</option>
<option value="1">Active</option>
<option value="0">Inactive</option>
</select>
<span id='error_cat_status'></span>
</div>




</div>
</div>
</div>
<div class="col-lg-6">
<div class="main-box">
<header class="main-box-header clearfix">
<h2>Contact Information</h2>




</header>
<div class="main-box-body clearfix">

<div class="form-group">
<input type="text" class="form-control" placeholder='Email Address' name="email" id="email"  maxlength="30" value="{{Input::old('email') }}">
<span id='addStore-email' class='error-msg'>{{ $errors->first('email', ':message') }}</span>
</div>

<div class="form-group">

<input type="text" class="form-control" id="website" name="website" placeholder='Website' value="{{Input::old('website') }}" maxlength="40">
<span id='addStore-website' class='error-msg'>{{ $errors->first('website', ':message') }}</span>
</div>


<div class="form-group"> 

<input type="text" class="form-control" id="mobile" name="mobile" placeholder='Mobile Number' value="{{Input::old('mobile') }}" >
<span id='addStore-mobile' class='error-msg'>{{ $errors->first('mobile', ':message') }}</span>
</div>


<div class="form-group">

<input type="text" class="form-control" id="phone" name="phone" placeholder='Phone Number' value="{{Input::old('phone') }}">
<span id='addStore-phone' class='error-msg'>{{ $errors->first('phone', ':message') }}</span>  
</div>

  



</div>
</div>
</div>





</div>

<center>
<button type="submit" class="btn btn-success" id="createStore">Submit</button>
<button type="submit" class="btn btn-danger">Cancel</button>
</center>
{{ Form::close() }}
<div class="row" style="display:none">
<div class="col-lg-12">
<div class="main-box">
<br>
<div class="main-box-body clearfix">
<div class="row">
<div class="col-lg-12">

<center>
<button type="submit" class="btn btn-success">Add Store</button>
</center>
<div  style="display:none">

<div class="onoffswitch">
<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" checked="">
<label class="onoffswitch-label" for="myonoffswitch">
<div class="onoffswitch-inner"></div>

<div class="onoffswitch-switch"></div>

</label>

</div>

</div>




</div>
</div>
</div>
</div>
</div>
</div>



</div>

<script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('address'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var mesg = "address: " + address;


                $.ajax({
           type: "POST",
           url : "googleplace",
           data: {address: address},
           success : function(data){
          console.log(data);
          arr = data.split(',');
          var latitude = arr[0];
          var longitude = arr[1];
          console.log(arr[0]);
          console.log(arr[1]);
          $('#latitude').val(latitude);
        $('#longitude').val(longitude);
          }
        });

          
            });
        });
    </script>
<script>
    $(document).ready(function() {



$('.chosen-container-multi').attr("tabindex", 2);

$('#mobile').mask('(+1) 999-999-9999', {autoclear: false});
$('#phone').mask('(+1) 9999999999', {autoclear: false});
$('#timepicker').mask('99:99:99');
$('#timepicker1').mask('99:99:99');


$('#timepicker').timepicker({
      minuteStep: 5,
      showSeconds: true,
      showMeridian: false,
      disableFocus: false,
      showWidget: true
    }).focus(function() {
      $(this).next().trigger('click');
    });
    $('#timepicker1').timepicker({
      minuteStep: 5,
      showSeconds: true,
      showMeridian: false,
      disableFocus: false,
      showWidget: true
    }).focus(function() {
      $(this).next().trigger('click');
    });


        });
</script>





@if(Session::has('createCat'))

<script>
$(document).ready(function() {
$( "#dialog" ).dialog({
    autoOpen: true,
    width: 400,
    buttons: [
        {
            text: "Yes",
            click: function() {
                $( this ).dialog( "close" );
                

                window.location.href  = $.cookie('ghDomain')+'/admin/product/'+{{ Session::get('id') }};
                
            }
        },
        {
            text: "Later",
            click: function() {
                $( this ).dialog( "close" );
            }
        }
    ]
});

// Link to open the dialog
$( "#dialog-link" ).click(function( event ) {
    $( "#dialog" ).dialog( "open" );
    event.preventDefault();
});

});
</script>
<!-- ui-dialog -->
<div id="dialog" title="Store created Successfully">
    <p>Do you want to add StoreCategories / Juices to the created Store ?</p>
</div>
@endif
<style>
.chosen-choices
{
  width: 142% !important;
}
.chosen-drop
{
  width: 142% !important;
}
</style>
@stop