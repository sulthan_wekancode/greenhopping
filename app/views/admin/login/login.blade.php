<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<title>Green Hopping</title>
 

<link href="{{ asset('/').('public/css/bootstrap/bootstrap.min.css') }}" type="text/css" rel="stylesheet"/>
 
<script src="{{ asset('/').('public/js/demo-rtl.js') }}"></script>


<link href="{{ asset('/').('public/css/libs/font-awesome.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ asset('/').('public/css/libs/nanoscroller.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ asset('/').('public/css/compiled/theme_styles.css') }}" type="text/css" rel="stylesheet"/>

<link href="{{ asset('/').('public/css/css.css?family=Open+Sans:400,600,700,300|Titillium+Web:200,300,400') }}" type="text/css" rel="stylesheet"/>
 

 
<link type="image/x-icon" href="favicon.png" rel="shortcut icon"/>
<!--[if lt IE 9]>
		<script src="public/js/html5shiv.js"></script>
		<script src="public/js/respond.min.js"></script>
	<![endif]-->
</head>
<body id="login-page-full">
<div id="login-full-wrapper">
<div class="container">
<div class="row">
<div class="col-xs-12">
<div id="login-box">
<div id="login-box-holder">
<div class="row">
<div class="col-xs-12">
<header id="login-header">
    <div id="login-logo" style="padding: 0px 0px 0px 0px;background-color:#03a9f4">

<img src="{{ asset('/').('public/img/logo.png') }}" alt=""/ style='height:84px;'>
</div>
</header>
<div id="login-box-inner">
{{ Form::open(array('url' => 'admin/adminAuth', 'files'=> true, 'id' => 'process')) }}

@if(Session::has('message'))
<center class="error-msg">
<h5>{{ Session::get('message') }}</h5>
</center>
@endif
<center>
<span id="adminRegisterFormMessage" class="error-msg"></span>
</center>
<div class="input-group">
<span class="input-group-addon"><i class="fa fa-user"></i></span>

<input class="form-control" type="email" placeholder="Email address" name="email" id="email" value="{{ Input::old('email')}}">
</div>
<div class="error-msg" id="adminLogin-email">{{ $errors->first('email', ':message') }}</div>
<div class="input-group">
<span class="input-group-addon"><i class="fa fa-key"></i></span>
<input type="password" class="form-control" placeholder="Password" name="password" id="password" value="{{ Input::old('password')}}">
</div>
<div class="error-msg" id="adminLogin-password">{{ $errors->first('password', ':message') }}</div>
<div id="remember-me-wrapper">
<div class="row">
<div class="col-xs-6">
<div class="checkbox-nice">
<input type="checkbox" id="remember-me" />
<label for="remember-me">
Remember me
</label>
</div>
</div>
<a href="forgot-password-full.html" id="login-forget-link" class="col-xs-6" style="display:none">
Forgot password?
</a>
</div>
</div>
<div class="row">
<div class="col-xs-12">
    <button type="submit" class="btn btn-success col-xs-12" id="adminLogin">Login</button>
</div>
</div>


</form>
</div>
</div>
</div>
</div>

</div>
</div>
</div>
</div>
</div>

 
<script src="{{ asset('/').('public/js/demo-skin-changer.js') }}"></script>
<script src="{{ asset('/').('public/js/jquery.js') }}"></script>
<script src="{{ asset('/').('public/js/bootstrap.js') }}"></script>
<script src="{{ asset('/').('public/js/jquery.cookie.js') }}"></script>
<script src="{{ asset('/').('public/js/jquery.nanoscroller.min.js') }}"></script>
<script src="{{ asset('/').('public/js/demo.js') }}"></script>
<script src="{{ asset('/').('public/js/scripts.js') }}"></script>
<script src="{{ asset('/').('public/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('/').('public/asset/admin.js') }}"></script>

 
 
</body>

</html>