@extends('layout.admin')
@section('content')
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=true&libraries=places"></script>

<!-- Country Code Picker -->

	

<!-- End of Country Code Picker -->


<div id="content-wrapper" style="background-color:white">
<div class="row">
<div class="col-lg-12">
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li><a href="#">Home</a></li>
<li><a href="#">Juice</a></li>
<li class="active"><span>Create Juice</span></li>
</ol>
</div>
</div>
<div class="row">
<div class="col-lg-6">
<div class="main-box">
<header class="main-box-header clearfix">
<h2>Basic Information</h2>
</header>
{!! Form::open(array('url' => 'admin/addJuiceDetails', 'name' => 'addStore', 'id' => 'addStore', 'role' => 'form'))!!}
<div class="main-box-body clearfix">
@if(Session::has('message'))
<span class="error-msg">
<h5>{{ Session::get('message') }}</h5>
</span>
@endif
<div class="form-group">
<input type="text" class="form-control" id="name" placeholder="Name" maxlength="25" name="name"   value="{!!Request::old('name') !!}">
<span id='addStore-name' class='error-msg'>{{ $errors->first('name', ':message') }}</span>
</div>

<div class="form-group">

<textarea class="form-control" name="description" rows="3" placeholder="Description" maxlength='100'>{!!Request::old('description') !!}</textarea>
<span id='addStore-address' class='error-msg'>{{ $errors->first('address', ':description') }}</span>
</div>
<div class="form-group">
<label for="exampleInputEmail1">Picture</label>
<input type="file" class="form-control" placeholder="Profile Picture" name="image" id="image">
<span id='addStore-profile_image' class='error-msg'>{{ $errors->first('image', ':message') }}</span>
</div>

</div>
</div>
</div>
<div class="col-lg-6">
<div class="main-box">
<header class="main-box-header clearfix">
<h2>Contact Information</h2>




</header>
<div class="main-box-body clearfix">

<div class="form-group">
<input type="text" class="form-control" placeholder='Price' name="price" id="email"  maxlength="30" value="{!!Request::old('price') !!}">
<span id='addStore-email' class='error-msg'>{{ $errors->first('price', ':message') }}</span>
</div>




</div>
</div>
</div>




<input type="hidden" name="image" value="2">


</div>

<center>
<button type="submit" class="btn btn-success" id="createStore">Create Store</button>
<button type="submit" class="btn btn-danger">Cancel</button>
</center>
{!! Form::close() !!}
<div class="row" style="display:none">
<div class="col-lg-12">
<div class="main-box">
<br>
<div class="main-box-body clearfix">
<div class="row">
<div class="col-lg-12">

<center>
<button type="submit" class="btn btn-success">Add Store</button>
</center>
<div  style="display:none">

<div class="onoffswitch">
<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" checked="">
<label class="onoffswitch-label" for="myonoffswitch">
<div class="onoffswitch-inner"></div>

<div class="onoffswitch-switch"></div>

</label>

</div>

</div>




</div>
</div>
</div>
</div>
</div>
</div>



</div>

<script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('address'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var mesg = "address: " + address;
          
            });
        });
    </script>
<script>
    $(document).ready(function() {
        $('#mobile').mask('(+1) 999-999-9999');
$('#phone').mask('(+1) 99999999999');
        });
</script>


@stop