@extends('layout.admin')
@section('content')
<div id="content-wrapper">
<div class="row">
<div class="col-lg-12">
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li><a href="#">Home</a></li>
<li class="active"><span>Store</span></li>
</ol>
</div>
</div>

<div class="row">
<div class="col-lg-12">
<div class="main-box clearfix">
<header class="main-box-header clearfix">
<h2 class="pull-left">Store List</h2>
<div class="filter-block pull-right">

<a href="{{ URL::to('admin/addstore') }}" class="btn btn-primary pull-right">
<i class="fa fa-plus-circle fa-lg"></i> Add Store
</a>
</div>
</header>
<div class="main-box-body clearfix">
<div class="table-responsive">
<table class="table" id="dataTables-example">
<thead>
<tr>
<th><span>Store</span></th>
<th><span>Location</span></th>
<th><span>Status</span></th>
<th><span>Date updated</span></th>
<th>&nbsp;</th>
</tr>
</thead>
<tbody>
@foreach($stores as $store)
<tr>
<td>
{{ $store->name }}
</td>
<td >
{{ $store->address }}
</td>
<td class="text-center">
<?php
if($store->status==1)
{
?>
<span class="label label-success">Active</span>
<?php
}
else
{
?>
<span class="label label-warning">Inactive</span>
<?php
}
?>


</td>
<td class="text-center">
{{ $store->updated_at }}
</td>
<td style="width: 15%;">

<a href="{{ URL::to('admin/listcategory').'/'.$store->id }}" class="table-link">
<span class="fa-stack">
<i class="fa fa-square fa-stack-2x"></i>
<i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
</span>
</a>
<a href="#" class="table-link danger">
<span class="fa-stack">
<i class="fa fa-square fa-stack-2x"></i>
<i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
</span>
</a>
</td>
</tr>
@endforeach
</tbody>
</table>
</div>
<ul class="pagination pull-right">
<li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
<li><a href="#">1</a></li>
<li><a href="#">2</a></li>
<li><a href="#">3</a></li>
<li><a href="#">4</a></li>
<li><a href="#">5</a></li>
<li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
</ul>
</div>
</div>
</div>
</div>


</div>
</div>


<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>


@stop