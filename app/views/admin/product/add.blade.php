@extends('layout.admin')
@section('content')
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=true&libraries=places"></script>
<script src="{{ asset('/').('public/js/jquery.validate.min.js') }}"></script>
<!-- Country Code Picker -->


<script src="{{ asset('/').('public/asset/ajaxupload.3.5.js') }}"></script>
<script src="{{ asset('/').('public/asset/s3_admin_cat.js') }}"></script>
<script src="{{ asset('/').('public/asset/s3_admin_juice.js') }}"></script>

	

<!-- End of Country Code Picker -->



<div id="content-wrapper">
<div class="row">
<div class="col-lg-12">
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li><a href="#">Home</a></li>
<li class="active"><span>Add Product</span></li>
</ol>

</div>
</div>
<div class="row">
<div class="col-lg-12">
<div class="main-box clearfix">

<div class="main-box-body clearfix">
<div class="row" style="margin-top: -20px;">
<br>


<div class="col-lg-6">

<h3>Categories</h3>
<span style='float:none;margin:0px 0px 0px 387px' >

<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" style='background-color:#13923E' id="showAddCat">
Add Category <span class="fa fa-plus"></span>
</button>
<br>

@if(Session::has('catCreated'))
<div class="pageAlerts alert alert-success fade in" style="display:none" >
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<i class="fa fa-check-circle fa-fw fa-lg"></i>
<strong>Category Saved Successfully</strong>
</div>
@endif


<div class="alert alert-success fade in" style="display:none" id="successCat">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<i class="fa fa-check-circle fa-fw fa-lg"></i>
<strong>Category Saved Successfully</strong>
</div>
<div class="alert alert-success fade in" style="display:none" id="showCatUpdate">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<i class="fa fa-check-circle fa-fw fa-lg"></i>
<strong>Category Updated Successfully</strong>
</div>

<div class="alert alert-success fade in" style="display:none" id="showDelCatSection">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<i class="fa fa-check-circle fa-fw fa-lg"></i>
<strong>Category Deleted Successfully</strong>
</div>




</span>

<div class="list-group" id="showAddCategory" style="display:none">
<span class="list-group-item">

<input type="text" class="form-control" id="cat_name" placeholder="Category Name" maxlength="20">
<span id='error_cat_name' class='error-msg'></span>
<br>
<a href="javascript:;" id="cat_pic" style="font-size:15px; font-weight:bold; padding:3px; text-decoration:none">Browse Files & Upload</a> 

<input type="hidden" value="" id="cat_image" name="cat_image">
<span id="statuss"></span>
<div id="cat_cover"></div>

<br>
<span id='error_cat_image' style='color:red'></span>
<div class="form-group">
<select class="form-control" name="cat_status" id="cat_status">
<option disabled="">Status</option>
<option value="1">Enable</option>
<option value="0">Disable</option>
</select>
<span id='error_cat_status'></span>
</div>
<center>
<br>
<button type="button" class="btn btn-success" id="saveCategory">Save Category</button>
<input type="hidden" name="editCatValue" id='editCatValue' value="">
<button type="button" class="btn btn-success" id="updateCategory" style="display:none">Update Category</button>
</center>

</span>
</div>


<br>
<div class="list-group" id='categoryList'>

@foreach ($category as $cat)
<div class="catList list-group-item" id="{{$cat->id}}">
<img src="{{$cat->image}}" height="50px" width="50px">
<span  id='{{ $cat->id }}'>
{{ $cat->name }}
</span>


<span  style='float:right'>	
<a class='listCat' class="table-link" id='{{ $cat->id }}'>
<span class="fa-stack">
<i class="fa fa-square fa-stack-2x"></i>
<i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
</span>
</a>
<a class="table-link">
<span class="editCat fa-stack"  id='{{ $cat->id}}'>
<i class="fa fa-square fa-stack-2x"></i>
<i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
</span>
</a>
<a class="table-link danger" >
<span class="deleteCat fa-stack" style='color:red' id='{{ $cat->id}}'>
<i class="fa fa-square fa-stack-2x"></i>
<i class="fa fa-trash-o fa-stack-1x fa-inverse" ></i>
</span>
</a>
</span>


</div>

@endforeach




</div>
</div>

<div class="col-lg-6" >
<h3>Juices</h3>
<span id='juiceTab' style="display:none">
<span style='float:none;margin:0px 0px 0px 397px' >
<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" style='background-color:#13923E' id="showAddJuice">
Add Juice <span class="fa fa-plus"></span>
</button>
<div class="alert alert-success fade in" style="display:none" id="showJuiceAddSection">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<i class="fa fa-check-circle fa-fw fa-lg"></i>
<strong>Juice Saved Successfully</strong>
</div>
<div class="alert alert-success fade in" style="display:none" id="showDelJuiceSection">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<i class="fa fa-check-circle fa-fw fa-lg"></i>
<strong>Juice Deleted Successfully</strong>
</div>

</span>
<h2><b>Category : </b> <span id='catTitle'></span></h2>
<input type="hidden" id="catTitleId" value="">


<div class="list-group" id="showAddJuiceCol" style="display:none">
<span class="list-group-item">
<input type="text" class="form-control" id="juiceName" placeholder="Juice Name" maxlength="20">
<span id='error-juiceName' class='error-msg'></span>
<br>
<a href="javascript:;" id="juice_pic" style="font-size:15px; font-weight:bold; padding:3px; text-decoration:none">Browse Files & Upload</a> 
<br>
<span id='error-Image' class='error-msg'></span>
<input type="hidden" value="" id="juice_image" name="juice_image">
<span id="statuss"></span>
<div id="juice_cover"></div>
<img src="" height="50" width="50" class="img-responsive" id="juice_Img_Pic" style="display:none">


<textarea class="form-control" id='description' placeholder='Ingredients' maxlength="250"></textarea>
<span id='error-description' class='error-msg'></span>
<input type="text" class="form-control decimal" placeholder='Price' id='price' maxlength="5" > 

<span id='error-price' class='error-msg'></span>
<center>
<div class="form-group">
<select class="form-control" name="juice_status" id="juice_status">
<option disabled="">Status</option>
<option value="1">Enable</option>
<option value="0">Disable</option>
</select>
<span id='error_cat_status'></span>
</div>
<br>
<button type="button" class="btn btn-success" id="saveJuice">Save Juice</button>
<input type="hidden" name="juiceId" value="" id="juiceId">
<button type="button" class="btn btn-success" id="updateJuice" style="display:none">Update Juice</button>
</center>
</span>
</div>

<span id='catListItem'></span>
</span>
</div>

</div>


</div>
</div>
</div>
</div>


</div>
</div>




<script>
$(document).ready(function() {

var ghDomain = $.cookie('ghDomain');


if (localStorage.getItem("showCatAdd") === null) { }
else {	$("#successCat").show().delay(3000).queue(function(n) {
				  $(this).hide(); n();  	});	localStorage.removeItem("showCatAdd"); }
if (localStorage.getItem("showCatUpdate") === null) { }
else {	$("#showCatUpdate").show().delay(3000).queue(function(n) { $(this).hide(); n();	});	localStorage.removeItem("showCatUpdate"); }











$("#showAddCat").click(function(event){
    $('#showAddCategory').show();
});

$("#showAddJuice").click(function(event){
    
    $('.error-msg').html('');
    $('#juiceName').val('');
    $('#description').val('');
    $('#price').val('');
    $('#updateJuice').hide();
    $('#saveJuice').show();
    $('#showAddJuiceCol').show();
    $('#juice_image').val('');
    $('#juice_cover').hide();
    
    

});

//

$(".listCat").click(function(event){
    $('#showAddJuiceCol').hide();
    $('#cat_name').val('');
    $('.error-msg').html('');
    $('#showAddCategory').hide();
    $('.catList').css('background', 'none');
    $('.catList#'+this.id).css('background', '#F5EDED');
    
    
			var cat_id = this.id;
			$.ajax({
                method: "POST",
                url: ghDomain+"/admin/getJuice",
                data: {
                    'cat_id': cat_id,
                }
            })
            .done(function(msg) {
				$('#catListItem').html(msg.html);
				$('#catTitle').html(msg.head);
				$('#catTitleId').val(cat_id);


            });
    		$('#juiceTab').show();
});


$(".editCat").click(function(event){
    
			var cat_id = this.id;
			$.ajax({
                method: "POST",
                url: ghDomain+"/admin/getCatDetails",
                data: {
                    'cat_id': cat_id,
                }
            })
            .done(function(msg) {
				$('#cat_name').val(msg.cat.name);
				$('#editCatValue').val(cat_id);

                $('#cat_cover').html('<img src="'+msg.cat.image+'" height="50" width="50" class="img-responsive">');
                
                $('#cat_image').val(msg.cat.image);
                $('select[name="cat_status"]').val(msg.cat.status);
				$('#saveCategory').hide();
				$('#updateCategory').show();				
				$('#showAddCategory').show();
				return false;
            });
			});

$(".deleteCat").click(function(event){
    
			var cat_id = this.id;
			console.log(cat_id);

			localStorage.setItem("delCat", cat_id);
			$("#dialogs").show();
			$( "#dialogs" ).dialog( "open" );
			    event.preventDefault();
			    return false;
			});

$("#saveCategory").click(function(event){
	event.preventDefault();
    var name = $('#cat_name').val();
    var image = $('#cat_image').val();
    var status = $('#cat_status option:selected').val();
    var id = <?php echo $id ?>;
    console.log(name);
    console.log(image);
    console.log(status);

	if(name=='' || image =='' || status=='')
    //if (name === undefined || image === undefined)
    {
    	console.log('nono');
    	if (name == '')
    	{
    		$('#error_cat_name').html('Category name is required');
    	}
    	if (image == '')
    	{
    		$('#error_cat_image').html('Category image is required');
    	}
        if (image == '')
        {
            $('#error_cat_status').html('Status is required');
        }
    	return false;
    }
    else
    {
    	$('#error_cat_name').html('');
    	$('#error_cat_image').html('');
    }

    $.ajax({
                method: "POST",
                url: ghDomain+"/admin/checkCat",
                data: {
                    "store_id": id,
                    "name": name,
                }
            })
            .done(function(msg) {
                if(msg.success==1)
                {
                    saveCat();            
                }
                else
                {
                    $('#error_cat_name').html("Category name is already taken");
                    return false;  
                }
            });    

  function saveCat()
  {          

    $.ajax({
                method: "POST",
                url: ghDomain+"/admin/addCat",
                data: {
                    'store_id': id,
                    'name': name,
                    'image': image,
                    'status': status,
                }
            })
            .done(function(msg) {
            	if(msg.success==1)
            	{
            		localStorage.setItem("showCatAdd", "1");
            		location.reload();
            		return false;
                

            	}
            	else
            	{
            		$('#cat_error').val('');	
            	}
            });
}
});

$("#updateCategory").click(function(event){
	event.preventDefault();
    var name = $('#cat_name').val();    
    var image = $('#cat_image').val();
    var status = $('#cat_status option:selected').val();
    var id = <?php echo $id ?>;

    console.log(name);
    console.log(image);
    
    $("#error_cat_name").html('');
    $("#error_cat_image").html('');
    $("#error_cat_status").html('');

	if(name=="" || image =="" || status =='')
    {
    	if (name == "")
    	{
    		$('#error_cat_name').html('Category name is required');
    	}
    	if (image == "")
    	{
    		$('#error_cat_image').html('Category image is required');
    	}
        if (image == "")
        {
            $('#error_cat_status').html('Status is required');
        }
    	return false;
    }
    else
    {
    	$("#error_cat_name").html("");
    	$("#error_cat_image").html("");
    }


       $.ajax({
                method: "POST",
                url: ghDomain+"/admin/checkCat",
                data: {
                    "id": $("#editCatValue").val(),
                    "store_id": id,
                    "name": name,
                    "update": "update",
                }
            })
            .done(function(msg) {
                console.log(msg);
                
                if(msg.success==1)
                {
                    updateCate();            
                }
                else
                {
                    $('#error_cat_name').html("Category name is already taken");
                    return false;  
                }
            });    
function updateCate()
{
    var ca_id = $("#editCatValue").val();
console.log(ca_id);
    $.ajax({
                method: "POST",
                url: ghDomain+"/admin/updateCat",
                data: {
                    "id": ca_id,
                    "store_id": id,
                    "status": status,
                    "name": name,
                    "image": image,
                }
            })
            .done(function(msg) {
                if(msg.success==1)
                {
                    localStorage.setItem("showCatUpdate", "1");
                    location.reload();
                    return false;
                }
                else
                {
                    $("#cat_error").val("");    
                }
            });    
}



});

$("#saveJuice").click(function(event){
	event.preventDefault();
    var name = $('#juiceName').val();
    var description = $('#description').val();
    var price = $('#price').val();
    var image = $('#juice_image').val();
    var id = <?php echo $id ?>;
    var cat_id = $('#catTitleId').val();
    var status = $('#juice_status option:selected').val();
    console.log(id);
    console.log(image);
    
    $(".error-msg").html('');
	if(name=='' || description =='' || price =='' || status=='' || image=='')
    //if (name === undefined || image === undefined)
    {
    	
        console.log('nono');
    	if (name == '')
    	{
    		$('#error-juiceName').html('Juice name is required');
    	}
    	if (description == '')
    	{
    		$('#error-description').html('Ingredients is required');
    	}
    	if (price == '')
    	{
    		$('#error-price').html('Price is required');
    	}
        if (image == '')
        {
            $('#error-Image').html('Image is required');
        }
        if (price == '')
        {
            $('#error-status').html('Status is required');
        }
    	
    	return false;
    }
    else
    {
    	$('#error_cat_name').html('');
    	$('#error_cat_image').html('');

    }

    $.ajax({
                method: "POST",
                url: ghDomain+"/admin/checkJuice",
                data: {
                    "store_id": id,
                    "category_id": cat_id,
                    "name": name,
                }
            })
            .done(function(msg) {
                console.log(msg);
                 
                if(msg.success==1)
                {
                    checkJuice();            
                }
                else
                {
                    $('#error-juiceName').html("Juice name is already taken");
                    return false;  
                }
            }); 
function checkJuice()
{
    $.ajax({
                method: "POST",
                url: ghDomain+"/admin/addjuice",
                data: {
                    'store_id': id,
                    'category_id': cat_id,
                    'name': name,
                    'description': description,
                    'image': image,
                    'price': price,
                    'status': status,
                    
                }
            })
            .done(function(msg) {
                if(msg.success==1)
                {
                    localStorage.setItem("showJuiceAddId", cat_id);
                    localStorage.setItem("showJuiceAdd", "1");
                    location.reload();
                    return false;
                

                }
                else
                {
                    $('#cat_error').val('');    
                }
            });

    
}

});



if (localStorage.getItem("showJuiceAdd") === null) { }
else {	$("#showJuiceAddSection").show().delay(3000).queue(function(n) { $(this).hide(); n();	});	localStorage.removeItem("showJuiceAdd"); 
var showJuiceAddId = localStorage.getItem("showJuiceAddId");
localStorage.removeItem("showJuiceAddId");

$("#"+showJuiceAddId+".listCat").trigger("click");
console.log("#"+showJuiceAddId+" .listCat");
}

if (localStorage.getItem("delJuices") === null) { }
else {  $("#showJuiceAddSection").show().delay(3000).queue(function(n) { $(this).hide(); n();   }); localStorage.removeItem("showJuiceAdd"); 
var showJuiceAddId = localStorage.getItem("showJuiceAddId");
$("#"+showJuiceAddId+".listCat").trigger("click");
console.log("#"+showJuiceAddId+" .listCat");
}

if (localStorage.getItem("showDel") === null) { }
else {  $("#showDelCatSection").show().delay(3000).queue(function(n) { $(this).hide(); n();   }); localStorage.removeItem("showDel"); 
}
if (localStorage.getItem("showDelJuice") === null) { }
else {  $("#showDelJuiceSection").show().delay(3000).queue(function(n) { $(this).hide(); n();   }); localStorage.removeItem("showDelJuice"); 
var showJuiceAddId = localStorage.getItem("delJuice_cat");
$("#"+showJuiceAddId+".listCat").trigger("click");
console.log("#"+showJuiceAddId+" .listCat");

}


});




</script>




<script>
$(document).ready(function() {
$( "#dialogs" ).dialog({
    autoOpen: false,
    width: 400,
    buttons: [
        {
            text: "Yes",
            click: function() {
                $( this ).dialog( "close" );
               	cat_id = localStorage.getItem("delCat"); 
                localStorage.setItem("showDel", '1');
                $.ajax({
                method: "POST",
                url: $.cookie('ghDomain')+"/admin/deleteCat",
                data: {
                "cat_id": cat_id,
                }
            })
            .done(function(msg) {
				if(msg.success=="1")
				{
					 location.reload();
				}
				 location.reload();
            });

                
            }
        },
        {
            text: "No",
            click: function() {
                $( this ).dialog( "close" );
            }
        }
    ]
});

});
</script>

<script>
$(document).ready(function() {
$( "#dialog" ).dialog({
    autoOpen: false,
    width: 400,
    buttons: [
        {
            text: "Yes",
            click: function() {
                $( this ).dialog( "close" );
                juice_id = localStorage.getItem("delJuice"); 
                localStorage.setItem("showDelJuice", '1');
                $.ajax({
                method: "POST",
                url: $.cookie('ghDomain')+"/admin/deleteJuice",
                data: {
                "juice_id": juice_id,
                }
            })
            .done(function(msg) {
                if(msg.success=="1")
                {
                     location.reload();
                }
                 location.reload();
            });

                
            }
        },
        {
            text: "No",
            click: function() {
                $( this ).dialog( "close" );
            }
        }
    ]
});

$(".btn-danger").click(function(event){
  $("#email").val('');
  $("#website").val('');
  $("#mobile").val('');
  $("#phone").val('');
});

});
</script>


<!-- ui-dialog -->
<div id="dialogs" title="Delete Category" style="display:none">
    <p><b>Are you sure to delete this Category ?</b></p>
    Deleting Category will delete all the associated Juices in it.
</div>
<div id="dialog" title="Delete Juice" style="display:none">
    <p><b>Are you sure to delete this Juice ?</b></p>
</div>



@stop