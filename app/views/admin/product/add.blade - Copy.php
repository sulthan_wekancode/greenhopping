@extends('layout.admin')
@section('content')
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=true&libraries=places"></script>

<!-- Country Code Picker -->

	

<!-- End of Country Code Picker -->



<div id="content-wrapper">
<div class="row">
<div class="col-lg-12">
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li><a href="#">Home</a></li>
<li class="active"><span>Add Product</span></li>
</ol>

</div>
</div>
<div class="row">
<div class="col-lg-12">
<div class="main-box clearfix">

<div class="main-box-body clearfix">
<div class="row" style="margin-top: -20px;">
<br>


<div class="col-lg-6">

<h3>Categories</h3>
<span style='float:none;margin:0px 0px 0px 397px' >
<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" style='background-color:#13923E' id="showAddCat">
Add Category <span class="fa fa-plus"></span>
</button>
</span>
<br>
<br>
<div class="list-group" id="showAddCategory" style="display:none">
<span class="list-group-item">
<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Category Name">
<input type="file" class="form-control">
<center>
<br>
<button type="button" class="btn btn-success">Add Category</button>
</center>
</span>
</div>


<br>
<div class="list-group" id='categoryList'>

@foreach ($category as $cat)
<a href="#" class="list-group-item">{{ $cat->name }}</a>
@endforeach




</div>
</div>

<div class="col-lg-6">
<h3>Juices</h3>

<span style='float:none;margin:0px 0px 0px 397px' >
<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" style='background-color:#13923E'>
Add Juice <span class="fa fa-plus"></span>
</button>
</span>
<br>
<br>
<div class="list-group">
<span class="list-group-item">
<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Juice Name">
<input type="file" class="form-control">
<textarea class="form-control" placeholder='Ingredients'></textarea>
<input type="text" class="form-control" placeholder='Price'>
<center>
<br>
<button type="button" class="btn btn-success">Add Juice</button>
</center>
</span>
</div>
<br>

<a href="http://localhost/project/greenhopping/admin/addstore" class="btn btn-primary pull-right">
<i class="fa fa-plus-circle fa-lg"></i> Add Juice
</a>

<ul class="list-group">
<li class="list-group-item">
<span class="" style='float:right'>
	
<a href="http://localhost/project/greenhopping/admin/liststore/2" class="table-link">
<span class="fa-stack">
<i class="fa fa-square fa-stack-2x"></i>
<i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
</span>
</a>
<a href="#" class="table-link danger" >
<span class="fa-stack" >
<i class="fa fa-square fa-stack-2x"></i>
<i class="fa fa-trash-o fa-stack-1x fa-inverse" ></i>
</span>
</a>
</span>
Alpha Juice  
<img src="http://www.juiceladycherie.com/Juice/wp-content/uploads/2012/08/juice_glass.png" height="40px" width="40px">
30$ <br>
Ingredients : 
Sugar, Milk, Badam
</li>
<li class="list-group-item">
<span class="" style='float:right'>
	
<a href="http://localhost/project/greenhopping/admin/liststore/2" class="table-link">
<span class="fa-stack">
<i class="fa fa-square fa-stack-2x"></i>
<i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
</span>
</a>
<a href="#" class="table-link danger" >
<span class="fa-stack" >
<i class="fa fa-square fa-stack-2x"></i>
<i class="fa fa-trash-o fa-stack-1x fa-inverse" ></i>
</span>
</a>
</span>
Beta Juice  
<img src="http://pngimg.com/upload/juice_PNG7169.png" height="40px" width="40px">
30$ <br>
Ingredients : 
Sugar, Milk, Badam
</li>

</ul>
</div>

</div>


</div>
</div>
</div>
</div>


</div>
</div>




<script>
$(document).ready(function() {


$("#showAddCat").click(function(event){
    $('#showAddCategory').show();
});

});
</script>








@stop