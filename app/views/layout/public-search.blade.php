<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<title>Green Hopping</title>
 
<link href="{{ asset('/').('public/css/bootstrap/bootstrap.min.css') }}" type="text/css" rel="stylesheet"/>
 
<script src="{{ asset('/').('public/js/demo-rtl.js') }}"></script>
 
 
<link href="{{ asset('/').('public/css/libs/font-awesome.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ asset('/').('public/css/libs/nanoscroller.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ asset('/').('public/css/compiled/theme_styles.css') }}" type="text/css" rel="stylesheet"/>
 

<link href="{{ asset('/').('public/css/libs/daterangepicker.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ asset('/').('public/css/libs/jquery-jvectormap-1.2.2.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ asset('/').('public/css/libs/weather-icons.css') }}" type="text/css" rel="stylesheet"/>


<link type="image/x-icon" href="public/img/favicon.png" rel="shortcut icon"/>
 
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
<!--[if lt IE 9]>
    <script src="public/js/html5shiv.js"></script>
    <script src="public/js/respond.min.js"></script>
  <![endif]-->
</head>
<body >
<div id="theme-wrapper" style="height:100%;">
<header class="navbar" id="header-navbar">
<div class="container">
<a href="{{ URL::to('/') }}" id="logo" class="navbar-brand">
<img src="public/img/logo.png" alt="" class="normal-logo logo-white">
<img src="public/img/logo-black.png" alt="" class="normal-logo logo-black">
<img src="public/img/logo-small.png" alt="" class="small-logo hidden-xs hidden-sm hidden">
</a>
<div class="clearfix">
<button class="navbar-toggle" data-target=".navbar-ex1-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="fa fa-bars"></span>
</button>
<div class="nav-no-collapse navbar-left pull-left hidden-sm hidden-xs">

{{ Form::open(array('method' => 'get', 'url' => 'search', 'name' => 'addStore', 'id' => 'addStore', 'role' => 'form'))}}
<ul class="nav navbar-nav pull-right">
<li class="dropdown hidden-xs">

<select class="form-control" style="margin:7px 0px 0px 0px" name="location">
@foreach($cities as $city)
<option value="{{ $city->id }}">{{ $city->name }}</option>
@endforeach
</select>
</li>
</ul>


</div>
<div class="nav-no-collapse pull-left" id="header-nav">
<ul class="nav navbar-nav pull-left">
<li class="mobile-searching">

<input type="text" name="query" class="form-control" id="exampleInputEmail1" placeholder="Search" style="margin:7px 0px 0px 0px;border:none !important;width:650px">
{{ Form::close() }}
<div class="drowdown-search">
<form role="search">
<div class="form-group">
<input type="text" class="form-control" placeholder="Search...">
<i class="fa fa-search nav-search-icon"></i>
</div>
</form>
</div>
</li>
<li class="dropdown profile-dropdown">
<a id="expandMenu" class="dropdown-toggle" data-toggle="dropdown">
<span class="hidden-xs">
<i class="fa fa-user"></i>
Login</span> <b class="caret"></b>
</a>
<ul class="dropdown-menu dropdown-menu-right" style="min-width: 300px">




<!-- Login starts-->
<div class="col-lg-12" style="background-color:#EFEFEF">
<div class="main-boxallau clearfixallau" style="background-color:#EFEFEF">
<div class="tabs-wrapper profile-tabs" style="background-color:#EFEFEF">
<ul class="nav nav-tabs" style="background-color:#EFEFEF">
<li class="active" style="background-color:#EFEFEF;width:47%;padding:-2px 10px 10px 24px;margin: 0px 0px 0px 5px;">
<a data-toggle="tab" id="showLoginArea" style="color:black;border: 2;
border-bottom: 2px solid #43882C;border-right: 1px solid #43882C;outline: 0;height: 36px;">
<b style="margin:0px 0px 0px 18px">LOGIN</b></a></li>
<li class="" style="background-color:#EFEFEF;width:40%">
<a data-toggle="tab" style="color:black;height:36px;" id="showRegisterArea"><b style="margin:0px 0px 0px 13px">REGISTER</b></a></li>
</ul>
<div class="tab-content">
<div class="tab-pane fade active in" id="loginArea">
<div id="login-box-innerallau" style="background-color:#EFEFEF;margin-top: -6%;">
<form role="form" action="http://cube.adbee.technology/index.html">
<div class="input-group">

<input class="form-control" type="text" placeholder="Email Id" style="border:none;background-color:#EFEFEF;

border-bottom:1px;width:135%;
border: 0;

border-bottom: 1px solid #D0C8C8
outline: 0;
">
</div>
<div class="input-group">

<input type="password" class="form-control" placeholder="Password" style="background-color:#EFEFEF;width:135%;
border: 0;

border-bottom: 1px solid #D0C8C8
outline: 0;
">
</div>

<br>
<div id="remember-me-wrapper">
<div class="row">
<div class="col-xs-6">
<div class="checkbox-nice">
<input type="checkbox" id="remember-me">
<label for="remember-me" style="font-size:11px;margin:0px 0px 0px -6px !important">
Keep me logged in
</label>

</div>
</div>
<a href="forgot-password.html" id="login-forget-link" class="col-xs-6" style="font-size:12px" ;margin:-2px="" 0px="" -20px="">
Forgot Password
</a>

</div>
</div>
<br>
<div class="row">
<div class="col-xs-12">
<button type="submit" class="btn btn-success col-xs-12">Login</button>
</div>
</div>
<br>

</form>
</div>
</div>
<div class="tab-pane fade active in" id="registerArea" style="display:none">
<div id="login-box-innerallau" style="background-color:#EFEFEF;margin-top: -6%;">
<form role="form" action="http://cube.adbee.technology/index.html">
<div class="input-group">

<input class="form-control" type="text" placeholder="Email Id" style="border:none;background-color:#EFEFEF;

border-bottom:1px;width:135%;
border: 0;

border-bottom: 1px solid #D0C8C8
outline: 0;
">
</div>
<div class="input-group">

<input type="password" class="form-control" placeholder="Password" style="background-color:#EFEFEF;width:135%;
border: 0;

border-bottom: 1px solid #D0C8C8
outline: 0;
">
</div>
<div class="input-group">

<input type="password" class="form-control" placeholder="Password" style="background-color:#EFEFEF;width:135%;
border: 0;

border-bottom: 1px solid #D0C8C8
outline: 0;
">
</div>
<div id="remember-me-wrapper">
<div class="row">
<a href="forgot-password.html" id="login-forget-link" class="col-xs-6" style="font-size:12px" ;margin:-2px="" 0px="" -20px="">
Forgot Password
</a>
</div>
</div>
<div class="row">
<div class="col-xs-12">
<button type="submit" class="btn btn-success col-xs-12">Login</button>
</div>
</div>
<br>
<div class="row">
<div class="col-xs-12 col-sm-6">
<button type="submit" class="btn btn-primary col-xs-12 btn-facebook">
<i class="fa fa-facebook"></i> facebook
</button>
</div>
<div class="col-xs-12 col-sm-6">
<button type="submit" class="btn btn-primary col-xs-12 btn-twitter">
<i class="fa fa-twitter"></i> Twitter
</button>
</div>
</div>
</form>
</div>
</div>


</div>
</div>
</div>
</div>

<!-- Login ends -->

</ul>
</li>
<li class="hidden-xxs">
<a class="btn">

</a>
</li>
</ul>
</div>
</div>
</div>
</header>
<!--<div id="page-wrapper" class="container nav-small"> -->


@yield('content')

<footer id="footer-bar" class="row">
<p id="footer-copyright" class="col-xs-12">

<span class="icon" title="WeKanCode" data-toggle="tooltip">
<a href="http://www.wekancode.com" style="text-decoration:none" target="_new">Technology Partner 


<img src="public/img/wekancode-logo.png" height="25px;width:25px">
</a>
&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
</span>
<a href="">About</a>&nbsp;&nbsp;
<a href="">Blog</a>&nbsp;&nbsp;
<a href="">Team</a>&nbsp;&nbsp;
<a href="">Press</a>&nbsp;&nbsp;
<a href="">Contact</a>&nbsp;&nbsp;
</p>

</footer>


</div>
</div>

 
<script src="{{ asset('/').('public/js/demo-skin-changer.js') }}"></script>
<script src="{{ asset('/').('public/js/jquery.js') }}"></script>
<script src="{{ asset('/').('public/js/bootstrap.js') }}"></script>
<script src="{{ asset('/').('public/js/jquery.nanoscroller.min.js') }}"></script>
<script src="{{ asset('/').('public/js/demo.js') }}"></script>

<script src="{{ asset('/').('public/asset/jquery.min.js') }}"></script>
<script src="{{ asset('/').('public/asset/custom.js') }}"></script>

<script src="{{ asset('/').('public/js/moment.min.js') }}"></script>
<script src="{{ asset('/').('public/js/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('/').('public/js/jquery-jvectormap-world-merc-en.js') }}"></script>
<script src="{{ asset('/').('public/js/gdp-data.js') }}"></script>
<script src="{{ asset('/').('public/js/flot/jquery.flot.min.js') }}"></script>
<script src="{{ asset('/').('public/js/flot/jquery.flot.resize.min.js') }}"></script>
<script src="{{ asset('/').('public/js/flot/jquery.flot.time.min.js') }}"></script>
<script src="{{ asset('/').('public/js/flot/jquery.flot.threshold.js') }}"></script>
<script src="{{ asset('/').('public/js/flot/jquery.flot.axislabels.js') }}"></script>
<script src="{{ asset('/').('public/js/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('/').('public/js/skycons.js') }}"></script>


<script src="{{ asset('/').('public/js/pace.min.js') }}"></script>


<script>
  $(document).ready(function() {
    
      //CHARTS
      function gd(year, day, month) {
      return new Date(year, month - 1, day).getTime();
    }
    
    if ($('#graph-bar').length) {
      var data1 = [
          [gd(2015, 1, 1), 838], [gd(2015, 1, 2), 749], [gd(2015, 1, 3), 634], [gd(2015, 1, 4), 1080], [gd(2015, 1, 5), 850], [gd(2015, 1, 6), 465], [gd(2015, 1, 7), 453], [gd(2015, 1, 8), 646], [gd(2015, 1, 9), 738], [gd(2015, 1, 10), 899], [gd(2015, 1, 11), 830], [gd(2015, 1, 12), 789]
      ];
      
      var data2 = [
          [gd(2015, 1, 1), 342], [gd(2015, 1, 2), 721], [gd(2015, 1, 3), 493], [gd(2015, 1, 4), 403], [gd(2015, 1, 5), 657], [gd(2015, 1, 6), 782], [gd(2015, 1, 7), 609], [gd(2015, 1, 8), 543], [gd(2015, 1, 9), 599], [gd(2015, 1, 10), 359], [gd(2015, 1, 11), 783], [gd(2015, 1, 12), 680]
      ];
      
      var series = new Array();

      series.push({
        data: data1,
        bars: {
          show : true,
          barWidth: 24 * 60 * 60 * 12000,
          lineWidth: 1,
          fill: 1,
          align: 'center'
        },
        label: 'Revenues'
      });
      series.push({
        data: data2,
        color: '#e84e40',
        lines: {
          show : true,
          lineWidth: 3,
        },
        points: { 
          fillColor: "#e84e40", 
          fillColor: '#ffffff', 
          pointWidth: 1,
          show: true 
        },
        label: 'Orders'
      });

      $.plot("#graph-bar", series, {
        colors: ['#03a9f4', '#f1c40f', '#2ecc71', '#3498db', '#9b59b6', '#95a5a6'],
        grid: {
          tickColor: "#f2f2f2",
          borderWidth: 0,
          hoverable: true,
          clickable: true
        },
        legend: {
          noColumns: 1,
          labelBoxBorderColor: "#000000",
          position: "ne"       
        },
        shadowSize: 0,
        xaxis: {
          mode: "time",
          tickSize: [1, "month"],
          tickLength: 0,
          // axisLabel: "Date",
          axisLabelUseCanvas: true,
          axisLabelFontSizePixels: 12,
          axisLabelFontFamily: 'Open Sans, sans-serif',
          axisLabelPadding: 10
        }
      });

      var previousPoint = null;
      $("#graph-bar").bind("plothover", function (event, pos, item) {
        if (item) {
          if (previousPoint != item.dataIndex) {

            previousPoint = item.dataIndex;

            $("#flot-tooltip").remove();
            var x = item.datapoint[0],
            y = item.datapoint[1];

            showTooltip(item.pageX, item.pageY, item.series.label, y );
          }
        }
        else {
          $("#flot-tooltip").remove();
          previousPoint = [0,0,0];
        }
      });

      function showTooltip(x, y, label, data) {
        $('<div id="flot-tooltip">' + '<b>' + label + ': </b><i>' + data + '</i>' + '</div>').css({
          top: y + 5,
          left: x + 20
        }).appendTo("body").fadeIn(200);
      }
    }
      
    //WORLD MAP
    $('#world-map').vectorMap({
      map: 'world_merc_en',
      backgroundColor: '#ffffff',
      zoomOnScroll: false,
      regionStyle: {
        initial: {
          fill: '#e1e1e1',
          stroke: 'none',
          "stroke-width": 0,
          "stroke-opacity": 1
        },
        hover: {
          "fill-opacity": 0.8
        },
        selected: {
          fill: '#8dc859'
        },
        selectedHover: {
        }
      },
      markerStyle: {
        initial: {
          fill: '#e84e40',
          stroke: '#e84e40'
        }
      },
      markers: [
        {latLng: [38.35, -121.92], name: 'Los Angeles - 23'},
        {latLng: [39.36, -73.12], name: 'New York - 84'},
        {latLng: [50.49, -0.23], name: 'London - 43'},
        {latLng: [36.29, 138.54], name: 'Tokyo - 33'},
        {latLng: [37.02, 114.13], name: 'Beijing - 91'},
        {latLng: [-32.59, 150.21], name: 'Sydney - 22'},
      ],
      series: {
        regions: [{
          values: gdpData,
          scale: ['#6fc4fe', '#2980b9'],
          normalizeFunction: 'polynomial'
        }]
      },
      onRegionLabelShow: function(e, el, code){
        el.html(el.html()+' ('+gdpData[code]+')');
      }
    });

    /* SPARKLINE - graph in header */
    var orderValues = [10,8,5,7,4,4,3,8,0,7,10,6,5,4,3,6,8,9];

    $('.spark-orders').sparkline(orderValues, {
      type: 'bar', 
      barColor: '#ced9e2',
      height: 25,
      barWidth: 6
    });

    var revenuesValues = [8,3,2,6,4,9,1,10,8,2,5,8,6,9,3,4,2,3,7];

    $('.spark-revenues').sparkline(revenuesValues, {
      type: 'bar', 
      barColor: '#ced9e2',
      height: 25,
      barWidth: 6
    });

    /* ANIMATED WEATHER */
    var skycons = new Skycons({"color": "#03a9f4"});
    // on Android, a nasty hack is needed: {"resizeClear": true}

    // you can add a canvas by it's ID...
    skycons.add("current-weather", Skycons.SNOW);

    // start animation!
    skycons.play();

  });
  </script>
</body>
</html>