<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<title>Green Hopping</title>
 
 
 
<link href="{{ asset('/').('public/css/bootstrap/bootstrap.min.css') }}" type="text/css" rel="stylesheet"/>



<script src="{{ asset('/').('public/js/demo-rtl.js') }}"></script>
 
 
<link href="{{ asset('/').('public/css/libs/font-awesome.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ asset('/').('public/css/libs/nanoscroller.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ asset('/').('public/css/compiled/theme_styles.css') }}" type="text/css" rel="stylesheet"/>


<link href="{{ asset('/').('public/css/libs/jquery-jvectormap-1.2.2.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ asset('/').('public/css/libs/weather-icons.css') }}" type="text/css" rel="stylesheet"/>
 

<script src="{{ asset('/').('public/js/jquery.js') }}"></script>
<script src="{{ asset('/').('public/js/jquery.maskedinput.js') }}"></script>
<script src="{{ asset('/').('public/js/jquery.form.js') }}"></script>


<link href="{{ asset('/').('public/css/libs/datepicker.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ asset('/').('public/css/libs/daterangepicker.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ asset('/').('public/css/libs/bootstrap-timepicker.css') }}" type="text/css" rel="stylesheet"/>




<script src="{{ asset('/').('public/js/jquery.dataTables.min.js') }}"></script>

<link href="{{ asset('/').('public/css/jquery-ui.css') }}" type="text/css" rel="stylesheet"/>
<script src="{{ asset('/').('public/js/jquery-ui.js') }}"></script>

<script src="{{ asset('/').('public/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('/').('public/js/moment.min.js') }}"></script>
<script src="{{ asset('/').('public/js/bootstrap-timepicker.min.js') }}"></script>



<link type="image/x-icon" href="favicon.png" rel="shortcut icon"/>
 
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>



<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
		<script src="js/respond.min.js"></script>
	<![endif]-->

</head>
<body>
<div id="theme-wrapper">
<header class="navbar" id="header-navbar">
<div class="container">
<a href="{{ URL::to('admin/dashboard') }}" id="logo" class="navbar-brand" style="display:none">
<img src="../public/img/logo.png" alt="" class="normal-logo logo-white"/ style="height:10px;width:10px">
<img src="../public/img/logo-black.png" alt="" class="normal-logo logo-black"/>
<img src="../public/img/logo-small.png" alt="" class="small-logo hidden-xs hidden-sm hidden"/>
</a>
<div class="clearfix">
<button class="navbar-toggle" data-target=".navbar-ex1-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="fa fa-bars"></span>
</button>
<div class="nav-no-collapse navbar-left pull-left hidden-sm hidden-xs">
<ul class="nav navbar-nav pull-left">
<li>
<a class="btn" id="make-small-nav">
<i class="fa fa-bars"></i>
</a>
</li>
<li class="dropdown hidden-xs" style="display:none">
<a class="btn dropdown-toggle" data-toggle="dropdown">
<i class="fa fa-bell"></i>
<span class="count">8</span>
</a>
<ul class="dropdown-menu notifications-list">
<li class="pointer">
<div class="pointer-inner">
<div class="arrow"></div>
</div>
</li>
<li class="item-header">You have 6 new notifications</li>
<li class="item">
<a href="#">
<i class="fa fa-comment"></i>
<span class="content">New comment on ‘Awesome P...</span>
<span class="time"><i class="fa fa-clock-o"></i>13 min.</span>
</a>
</li>
<li class="item">
<a href="#">
<i class="fa fa-plus"></i>
<span class="content">New user registration</span>
<span class="time"><i class="fa fa-clock-o"></i>13 min.</span>
</a>
</li>
<li class="item">
<a href="#">
<i class="fa fa-envelope"></i>
<span class="content">New Message from George</span>
<span class="time"><i class="fa fa-clock-o"></i>13 min.</span>
</a>
</li>
<li class="item">
<a href="#">
<i class="fa fa-eye"></i>
<span class="content">New order</span>
<span class="time"><i class="fa fa-clock-o"></i>13 min.</span>
</a>
</li>
<li class="item-footer">
<a href="#">
View all notifications
</a>
</li>
</ul>
</li>
<li class="dropdown hidden-xs" style="display:none">
<a class="btn dropdown-toggle" data-toggle="dropdown">
<i class="fa fa-envelope-o"></i>
<span class="count">16</span>
</a>
<ul class="dropdown-menu notifications-list messages-list">
<li class="pointer">
<div class="pointer-inner">
<div class="arrow"></div>
</div>
</li>
<li class="item first-item">
<a href="#">
<img src="../public/img/samples/messages-photo-1.png" alt=""/>
<span class="content">
<span class="content-headline">
George Clooney
</span>
<span class="content-text">
Look, just because I don't be givin' no man a foot massage don't make it
right for Marsellus to throw...
</span>
</span>
<span class="time"><i class="fa fa-clock-o"></i>13 min.</span>
</a>
</li>
<li class="item">
<a href="#">
<img src="../public/img/samples/messages-photo-2.png" alt=""/>
<span class="content">
<span class="content-headline">
Emma Watson
</span>
<span class="content-text">
Look, just because I don't be givin' no man a foot massage don't make it
right for Marsellus to throw...
</span>
</span>
<span class="time"><i class="fa fa-clock-o"></i>13 min.</span>
</a>
</li>
<li class="item">
<a href="#">
<img src="../public/img/samples/messages-photo-3.png" alt=""/>
<span class="content">
<span class="content-headline">
Robert Downey Jr.
</span>
<span class="content-text">
Look, just because I don't be givin' no man a foot massage don't make it
right for Marsellus to throw...
</span>
</span>
<span class="time"><i class="fa fa-clock-o"></i>13 min.</span>
</a>
</li>
<li class="item-footer">
<a href="#">
View all messages
</a>
</li>
</ul>
</li>


</ul>
</div>
<div class="nav-no-collapse pull-right" id="header-nav">
<ul class="nav navbar-nav pull-right">
<li class="mobile-search" style="display:none">
<a class="btn">
<i class="fa fa-search"></i>
</a>
<div class="drowdown-search" >
<form role="search">
<div class="form-group">
<input type="text" class="form-control" placeholder="Search...">
<i class="fa fa-search nav-search-icon"></i>
</div>
</form>
</div>
</li>
<li class="dropdown profile-dropdown">
<a href="#" class="dropdown-toggle" data-toggle="dropdown">

<span class="hidden-xs"><?php if(Auth::user()->username=='') { echo 'Admin'; } else { echo Auth::user()->username; }?></span> <b class="caret"></b>
</a>
<ul class="dropdown-menu dropdown-menu-right">
<li><a href="{{ URL::to('admin/profile') }}"><i class="fa fa-user"></i>Profile</a></li>
<li><a href="{{ URL::to('admin/logout') }}"><i class="fa fa-power-off"></i>Logout</a></li>
</ul>
</li>

</ul>
</div>
</div>
</div>
</header>
<div id="page-wrapper" class="container">
<div class="row">
<div id="nav-col">
<section id="col-left" class="col-left-nano">
<div id="col-left-inner" class="col-left-nano-content">

<div class="collapse navbar-collapse navbar-ex1-collapse" id="sidebar-nav">
<ul class="nav nav-pills nav-stacked">
<li class="active">
<a href="{{ URL::to('admin/dashboard/') }}" style="display:none">
<i class="fa fa-dashboard"></i>
<span>Dashboard</span>

</a>
</li>
<li>
<a class="dropdown-toggle">
<i class="fa fa-sitemap"></i>
<span>Store Category</span>
<i class="fa fa-angle-right drop-icon"></i>
</a>
<ul class="submenu">
<li>
<a href="{{ URL::to('admin/addtype') }}">
Create Store Category	
</a>
</li>
<li>
<a href="{{ URL::to('admin/listtype') }}">
List Store Category
</a>
</li>
</ul>
</li>

<li>
<a class="dropdown-toggle">
<i class="fa fa-building"></i>
<span>Store</span>
<i class="fa fa-angle-right drop-icon"></i>
</a>
<ul class="submenu">
<li>
<a href="{{ URL::to('admin/addstore') }}">
Create Store
</a>
</li>
<li>
<a href="{{ URL::to('admin/liststore') }}">
List Store
</a>
</li>
</ul>
</li>
<li>
<a class="dropdown-toggle">
<i class="fa fa-globe"></i>
<span>City</span>
<i class="fa fa-angle-right drop-icon"></i>
</a>
<ul class="submenu">
<li>
<a href="{{ URL::to('admin/addcity') }}">
Add City
</a>
</li>
<li>
<a href="{{ URL::to('admin/listcity') }}">
List City
</a>
</li>

</ul>
</li>

<li>
<a href="#" class="dropdown-toggle" style="display:none">
<i class="fa fa-book"></i>
<span>Orders</span>
<i class="fa fa-angle-right drop-icon"></i>
</a>
<ul class="submenu">
<li>
<a href="{{ URL::to('admin/addstore') }}">
List all Orders
</a>
</li>
<li>
<a href="form-dropzone.html">
Generate Orders Transaction
</a>
</li>
</ul>
</li>








<li class="nav-header hidden-sm hidden-xs">
More
</li>


<li>
<a class="dropdown-toggle">
<i class="fa fa-bullhorn"></i>
<span>Tip of Day</span>
<i class="fa fa-angle-right drop-icon"></i>
</a>
<ul class="submenu">
<li>
<a href="{{ URL::to('admin/addtipofday') }}">
Add Tip of Day
</a>
</li>
<li>
<a href="{{ URL::to('admin/listtipofday') }}">
List Tip of Day
</a>
</li>

</ul>
</li>



<li style="display:none">
<a href="#" class="dropdown-toggle">
<i class="fa fa-print"></i>
<span>Report</span>
<i class="fa fa-angle-right drop-icon"></i>
</a>
<ul class="submenu">
<li>
<a href="form-dropzone.html">
Generate Report
</a>
</li>
</ul>
</li>



<li>
<a href="#" class="dropdown-toggle">
<i class="fa fa-user"></i>
<span>Users</span>
<i class="fa fa-angle-right drop-icon"></i>
</a>
<ul class="submenu">
<li>
<a href="{{ URL::to('admin/listuser') }}">
List all Users
</a>
</li>

</ul>
</li>




</ul>
</div>
</div>
</section>
<div id="nav-col-submenu"></div>
</div>
@yield('content')

<footer id="footer-bar" class="row">
<p id="footer-copyright" class="col-xs-12">

<span class="icon" title="WeKanCode" data-toggle="tooltip">
<a href="http://www.wekancode.com" style="text-decoration:none" target="_new">Technology Partner 


<img src="../public/img/wekancode-logo.png" height="25px;width:25px">
</a>
&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
</span>
<a href="">About</a>&nbsp;&nbsp;
<a href="">Blog</a>&nbsp;&nbsp;
<a href="">Team</a>&nbsp;&nbsp;
<a href="">Press</a>&nbsp;&nbsp;
<a href="">Contact</a>&nbsp;&nbsp;
</p>

</footer>
</div>
</div>
</div>
</div>

<script src="{{ asset('/').('public/js/demo-skin-changer.js') }}"></script>

<script src="{{ asset('/').('public/js/jquery.cookie.js') }}"></script>
<script src="{{ asset('/').('public/js/bootstrap.js') }}"></script>
<script src="{{ asset('/').('public/js/jquery.nanoscroller.min.js') }}"></script>
<script src="{{ asset('/').('public/js/demo.js') }}"></script>

<script src="{{ asset('/').('public/js/moment.min.js') }}"></script>
<script src="{{ asset('/').('public/js/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('/').('public/js/jquery-jvectormap-world-merc-en.js') }}"></script>
<script src="{{ asset('/').('public/js/gdp-data.js') }}"></script>
<script src="{{ asset('/').('public/js/flot/jquery.flot.min.js') }}"></script>
<script src="{{ asset('/').('public/js/flot/jquery.flot.resize.min.js') }}"></script>
<script src="{{ asset('/').('public/js/flot/jquery.flot.time.min.js') }}"></script>
<script src="{{ asset('/').('public/js/flot/jquery.flot.threshold.js') }}"></script>
<script src="{{ asset('/').('public/js/flot/jquery.flot.axislabels.js') }}"></script>
<script src="{{ asset('/').('public/js/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('/').('public/js/skycons.js') }}"></script>
<script src="{{ asset('/').('public/js/scripts.js') }}"></script>
<script src="{{ asset('/').('public/js/pace.min.js') }}"></script>


<script src="{{ asset('/').('public/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('/').('public/asset/admin.js') }}"></script>

<script>
	$(document).ready(function() {
		$('#datepickerDate').datepicker({
		  format: 'mm-dd-yyyy'
		});

	    //CHARTS
	    function gd(year, day, month) {
			return new Date(year, month - 1, day).getTime();
		}
		
		if ($('#graph-bar').length) {
			var data1 = [
			    [gd(2015, 1, 1), 838], [gd(2015, 1, 2), 749], [gd(2015, 1, 3), 634], [gd(2015, 1, 4), 1080], [gd(2015, 1, 5), 850], [gd(2015, 1, 6), 465], [gd(2015, 1, 7), 453], [gd(2015, 1, 8), 646], [gd(2015, 1, 9), 738], [gd(2015, 1, 10), 899], [gd(2015, 1, 11), 830], [gd(2015, 1, 12), 789]
			];
			
			var data2 = [
			    [gd(2015, 1, 1), 342], [gd(2015, 1, 2), 721], [gd(2015, 1, 3), 493], [gd(2015, 1, 4), 403], [gd(2015, 1, 5), 657], [gd(2015, 1, 6), 782], [gd(2015, 1, 7), 609], [gd(2015, 1, 8), 543], [gd(2015, 1, 9), 599], [gd(2015, 1, 10), 359], [gd(2015, 1, 11), 783], [gd(2015, 1, 12), 680]
			];
			
			var series = new Array();

			series.push({
				data: data1,
				bars: {
					show : true,
					barWidth: 24 * 60 * 60 * 12000,
					lineWidth: 1,
					fill: 1,
					align: 'center'
				},
				label: 'Revenues'
			});
			series.push({
				data: data2,
				color: '#e84e40',
				lines: {
					show : true,
					lineWidth: 3,
				},
				points: { 
					fillColor: "#e84e40", 
					fillColor: '#ffffff', 
					pointWidth: 1,
					show: true 
				},
				label: 'Orders'
			});

			$.plot("#graph-bar", series, {
				colors: ['#03a9f4', '#f1c40f', '#2ecc71', '#3498db', '#9b59b6', '#95a5a6'],
				grid: {
					tickColor: "#f2f2f2",
					borderWidth: 0,
					hoverable: true,
					clickable: true
				},
				legend: {
					noColumns: 1,
					labelBoxBorderColor: "#000000",
					position: "ne"       
				},
				shadowSize: 0,
				xaxis: {
					mode: "time",
					tickSize: [1, "month"],
					tickLength: 0,
					// axisLabel: "Date",
					axisLabelUseCanvas: true,
					axisLabelFontSizePixels: 12,
					axisLabelFontFamily: 'Open Sans, sans-serif',
					axisLabelPadding: 10
				}
			});

			var previousPoint = null;
			$("#graph-bar").bind("plothover", function (event, pos, item) {
				if (item) {
					if (previousPoint != item.dataIndex) {

						previousPoint = item.dataIndex;

						$("#flot-tooltip").remove();
						var x = item.datapoint[0],
						y = item.datapoint[1];

						showTooltip(item.pageX, item.pageY, item.series.label, y );
					}
				}
				else {
					$("#flot-tooltip").remove();
					previousPoint = [0,0,0];
				}
			});

			function showTooltip(x, y, label, data) {
				$('<div id="flot-tooltip">' + '<b>' + label + ': </b><i>' + data + '</i>' + '</div>').css({
					top: y + 5,
					left: x + 20
				}).appendTo("body").fadeIn(200);
			}
		}
	    
		//WORLD MAP
		$('#world-map').vectorMap({
			map: 'world_merc_en',
			backgroundColor: '#ffffff',
			zoomOnScroll: false,
			regionStyle: {
				initial: {
					fill: '#e1e1e1',
					stroke: 'none',
					"stroke-width": 0,
					"stroke-opacity": 1
				},
				hover: {
					"fill-opacity": 0.8
				},
				selected: {
					fill: '#8dc859'
				},
				selectedHover: {
				}
			},
			markerStyle: {
				initial: {
					fill: '#e84e40',
					stroke: '#e84e40'
				}
			},
			markers: [
				{latLng: [38.35, -121.92], name: 'Los Angeles - 23'},
				{latLng: [39.36, -73.12], name: 'New York - 84'},
				{latLng: [50.49, -0.23], name: 'London - 43'},
				{latLng: [36.29, 138.54], name: 'Tokyo - 33'},
				{latLng: [37.02, 114.13], name: 'Beijing - 91'},
				{latLng: [-32.59, 150.21], name: 'Sydney - 22'},
			],
			series: {
				regions: [{
					values: gdpData,
					scale: ['#6fc4fe', '#2980b9'],
					normalizeFunction: 'polynomial'
				}]
			},
			onRegionLabelShow: function(e, el, code){
				el.html(el.html()+' ('+gdpData[code]+')');
			}
		});

		/* SPARKLINE - graph in header */
		var orderValues = [10,8,5,7,4,4,3,8,0,7,10,6,5,4,3,6,8,9];

		$('.spark-orders').sparkline(orderValues, {
			type: 'bar', 
			barColor: '#ced9e2',
			height: 25,
			barWidth: 6
		});

		var revenuesValues = [8,3,2,6,4,9,1,10,8,2,5,8,6,9,3,4,2,3,7];

		$('.spark-revenues').sparkline(revenuesValues, {
			type: 'bar', 
			barColor: '#ced9e2',
			height: 25,
			barWidth: 6
		});

		/* ANIMATED WEATHER */
		var skycons = new Skycons({"color": "#03a9f4"});
		// on Android, a nasty hack is needed: {"resizeClear": true}

		// you can add a canvas by it's ID...
		skycons.add("current-weather", Skycons.SNOW);

		// start animation!
		skycons.play();

	});
	</script>
	<style>
	.btn-danger
	{
		
	}
	.pagination
	{
		display:none;
	}

	</style>
</body>
</html>