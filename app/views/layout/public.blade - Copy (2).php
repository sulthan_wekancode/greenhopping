<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta name="google-signin-client_id" content="606388516556-27gbda05t739m582c2457hbdga1s3v0u.apps.googleusercontent.com">
<title>Green Hopping</title>
 
<link href="{{ asset('/').('public/css/bootstrap/bootstrap.min.css') }}" type="text/css" rel="stylesheet"/>
 
<script src="{{ asset('/').('public/js/demo-rtl.js') }}"></script>
 
 

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link href="{{ asset('/').('public/css/libs/nanoscroller.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ asset('/').('public/css/compiled/theme_styles.css') }}" type="text/css" rel="stylesheet"/>
 

<link href="{{ asset('/').('public/css/libs/daterangepicker.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ asset('/').('public/css/libs/jquery-jvectormap-1.2.2.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ asset('/').('public/css/libs/weather-icons.css') }}" type="text/css" rel="stylesheet"/>
<script src="{{ asset('/').('public/js/jquery.js') }}"></script>




<link type="image/x-icon" href="public/img/favicon.png" rel="shortcut icon"/>
 
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
<!--[if lt IE 9]>
    <script src="public/js/html5shiv.js"></script>
    <script src="public/js/respond.min.js"></script>
  <![endif]-->
</head>
<body >
<div id="theme-wrapper">
<header class="navbar" id="header-navbar">
<div class="container" style="background-color:#58d5f0">
<a href="{{ URL::to('/') }}" id="logo" class="navbar-brand" style="background-color:#58d5f0">

<img src="{{ asset('/').('public/img/logo.png') }}" alt="" class="normal-logo logo-white"/ style='display:block;    height: 132%;width: 119%;'>
<img src="public/img/logo-black.png" alt="" class="normal-logo logo-black">
<img src="public/img/logo-small.png" alt="" class="small-logo hidden-xs hidden-sm hidden">
</a>
<div class="clearfix" style="background-color:#58d5f0">
<button class="navbar-toggle" data-target=".navbar-ex1-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="fa fa-bars"></span>
</button>
<div class="nav-no-collapse navbar-left pull-left hidden-sm hidden-xs">

{{ Form::open(array('method' => 'get', 'url' => 'search', 'name' => 'addStore', 'id' => 'addStore', 'role' => 'form'))}}
<ul class="nav navbar-nav pull-right">
<li class="dropdown hidden-xs">

<select class="form-control" style="margin:7px 0px 0px 0px" name="location">
@foreach($cities as $city)
<option value="{{ $city->id }}">{{ $city->name }}</option>
@endforeach
</select>
</li>
</ul>


</div>
<div class="nav-no-collapse pull-left" id="header-nav">
<ul class="nav navbar-nav pull-left">
<li class="mobile-searching">

<input type="text" name="query" class="form-control" id="exampleInputEmail1" placeholder="Search" style="margin:7px 0px 0px 0px;border:none !important;width:650px">
{{ Form::close() }}
<div class="drowdown-search">
<form role="search" id="searchForm">
<div class="form-group">
<input type="text" class="form-control" placeholder="Search...">
<i class="fa fa-search nav-search-icon"></i>
</div>
</form>
</div>
</li>
<!-- data-toggle="dropdown" -->
<?php

if (Auth::check())
{
 


?>
<li class="dropdown profile-dropdown">
<a id="expandMenus" class="dropdown-toggle" data-toggle="dropdown">
<span class="hidden-xs">
<i class="fa fa-user"></i>
<span id='userNameNew'>
<?php
if(Auth::user()->name=='')
{
	$nameCount = count(Auth::user()->name);
	if($nameCount>6)
	{
		echo '123456';
	}
	else
	{
		echo 'nope';
	}
	
}
else
{
	echo Auth::user()->name;
}
?>

</span>
</span> <b class="caret"></b>

&nbsp;&nbsp;&nbsp;<i class="bag fa fa-shopping-bag" style='color:green'> 0</i>
</a>
<ul class="dropdown-menu dropdown-menu-right">
<li><a href="{{ URL::to('profile') }}"><i class="fa fa-user"></i>Profile</a></li>
<li><a href="{{ URL::to('logout') }}"><i class="fa fa-power-off"></i>Logout</a></li>
</ul>
</li>
<?php
}
else
{
?>
<li class="dropdown profile-dropdown">
<a id="expandMenu" class="dropdown-toggle" >
<span class="hidden-xs">
<i class="fa fa-user"></i>
Login</span> <b class="caret"></b>
&nbsp;&nbsp;&nbsp;<i class="bag fa fa-shopping-bag" style='color:green'> 0</i>
</a>
<ul class="dropdown-menu dropdown-menu-right account-popup" style="min-width: 300px;height:auto;background-color:#EFEFEF;">




<!-- Login starts-->
<div class="col-lg-12" style="background-color:#EFEFEF">
<div class="main-boxallau clearfixallau" style="background-color:#EFEFEF">
<div class="tabs-wrapper profile-tabs" style="background-color:#EFEFEF">
<ul class="nav nav-tabs" style="background-color:#EFEFEF">
<li class="active" style="background-color:#EFEFEF;width:47%;padding:-2px 10px 10px 24px;margin: 0px 0px 0px 5px;">
<a data-toggle="tab" id="showLogin" style="color:black;border: 2;
background-color:#EFEFEF !important;border-bottom: 2px solid #43882C;border-right: 1px solid #43882C;outline: 0;height: 36px;">
<b style="margin:0px 0px 0px 18px">LOGIN</b></a></li>
<li class="" style="background-color:#EFEFEF !important;width:40%">
<a data-toggle="tab" style="color:black;height:36px;background-color:#EFEFEF !important;" id="showRegister"><b style="margin:0px 0px 0px 13px">REGISTER</b></a></li>
</ul>
<div class="tab-content">
<div class="tab-pane fade active in" id="loginArea">
<div id="login-box-innerallau" style="background-color:#EFEFEF;margin-top: -6%;">
<form role="form" action="#" id="loginForm"  name="loginForm">
<div class="input-group">
<br>
<input class="form-control" id="loginEmail" name="loginEmail" type="text" placeholder="Email Id" style="border:none;background-color:#EFEFEF;

border-bottom:1px;width:135%;
border: 0;

border-bottom: 1px solid #D0C8C8
outline: 0;
">
</div>
<span id='loginEmailValidate'></span>
<span id='errorAreaLogin'></span>
<div class="input-group">

<input type="password" id="loginPassword" name="loginPassword" class="form-control" placeholder="Password" style="background-color:#EFEFEF;width:135%;
border: 0;

border-bottom: 1px solid #D0C8C8
outline: 0;
">
</div>
<span id='loginPasswordValidate'></span>
<br>
<div id="remember-me-wrapper">
<div class="row">
<div class="col-xs-6">
<div class="checkbox-nice">
<input type="checkbox" id="remember-me">
<label for="remember-me" style="font-size:11px;margin:0px 0px 0px -6px !important">
Keep me logged in
</label>

</div>
</div>
<a  id="showForget" class="col-xs-6" style="font-size:12px;display:none" ;margin:-2px="" 0px="" -20px="">
Forgot Password
</a>

</div>
</div>
<br>
<div class="row">
<div class="col-xs-12">
<button type="button" class="btn btn-success col-xs-12" id="doLogin">Login</button>
</div>
</div>
<br>

</form>
</div>
</div>
<div class="tab-pane register fade active in"  id="outputArea" style='display:none'>
</div>
<div class="tab-pane fade active in" id="registerArea" style="display:none">
<div id="login-box-innerallau" style="background-color:#EFEFEF;margin-top: -6%;">
<form role="form" id="registerForm" name="registerForm">
<div class="input-group">
<br>
<input class="form-control" type="text" placeholder="Email Id" style="border:none;background-color:#EFEFEF;

border-bottom:1px;width:135%;
border: 0;

border-bottom: 1px solid #D0C8C8
outline: 0;
"  id="registerEmail" name="registerEmail">
</div>
<span id='registerEmailValidate'></span>
<span id='errorAreaRegister'></span>
<div class="input-group">

<input type="password" class="form-control" placeholder="Password" style="background-color:#EFEFEF;width:135%;
border: 0;

border-bottom: 1px solid #D0C8C8
outline: 0;
" id="registerPassword" name="registerPassword" maxlength="15">
</div>
<span id='registerPasswordValidate'></span>
<div class="input-group">

<input type="password" class="form-control" placeholder="Confirm Password" id="registerPasswordConfirmation" name="registerPasswordConfirmation" maxlength="15" style="background-color:#EFEFEF;width:135%;
border: 0;

border-bottom: 1px solid #D0C8C8
outline: 0;
">
</div>
<span id='registerPasswordConfirmationValidate'></span>
<div class="row">
<div class="col-xs-12">
<button type="submit" class="btn btn-success col-xs-12" id="doRegister">Register</button>
</div>
</div>
<br>
<div class="row">
<div class="col-xs-12 col-sm-6">
<button type="submit" class="btn btn-primary col-xs-12 btn-facebook" style="display:none">
<i class="fa fa-facebook"></i> facebook
</button>
<fb:login-button scope="public_profile,email" onlogin="checkLoginState();">
</fb:login-button>

</div>
<div class="col-xs-12 col-sm-6">
<button type="submit" class="btn btn-primary col-xs-12 btn-google-plus" style="background-color: #D62929;display:none">
<i class="fa fa-google"></i> Google
</button>
<div id="my-signin2"></div>

</div>
</div>
</form>
</div>
</div>

<!-- reset -->
<div class="tab-pane fade active in" id="forgetArea" style="display:none">
<div id="login-box-innerallau" style="background-color:#EFEFEF;margin-top: -6%;">
<form role="form" action="#">
<div class="input-group">
<br>
<input class="form-control" type="text" placeholder="Email Id" style="border:none;background-color:#EFEFEF;

border-bottom:1px;width:135%;
border: 0;

border-bottom: 1px solid #D0C8C8
outline: 0;
">
</div>
<br>


<div class="row">
<div class="col-xs-12">
<button type="submit" class="btn btn-success col-xs-12">Reset</button>
</div>
</div>
<br>

</form>
</div>
</div>


</div>
</div>
</div>
</div>
<!-- Login ends -->
</ul>
</li>
<?php
}
?>
<li class="hidden-xxs">
<a class="btn">

</a>
</li>
</ul>
</div>
</div>
</div>
</header>
<!--<div id="page-wrapper" class="container nav-small"> -->
<div style="background-color:black">

@yield('content')

<footer id="footer-bar" class="row" style='display:none'>
<p id="footer-copyright" class="col-xs-12">

<span class="icon" title="WeKanCode" data-toggle="tooltip">
<a href="http://www.wekancode.com" style="text-decoration:none" target="_new">Technology Partner 


<img src="public/img/wekancode-logo.png" height="25px;width:25px">
</a>
&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
</span>
<a href="">About</a>&nbsp;&nbsp;
<a href="">Blog</a>&nbsp;&nbsp;
<a href="">Team</a>&nbsp;&nbsp;
<a href="">Press</a>&nbsp;&nbsp;
<a href="">Contact</a>&nbsp;&nbsp;
</p>

</footer>

</div>
</div>
</div>

 
<script src="{{ asset('/').('public/js/demo-skin-changer.js') }}"></script>
<script src="{{ asset('/').('public/js/jquery.js') }}"></script>
<script src="{{ asset('/').('public/js/bootstrap.js') }}"></script>
<script src="{{ asset('/').('public/js/jquery.nanoscroller.min.js') }}"></script>
<script src="{{ asset('/').('public/js/demo.js') }}"></script>
<script src="{{ asset('/').('public/js/jquery.cookie.js') }}"></script>
<script src="{{ asset('/').('public/asset/jquery.validate.js') }}"></script>
<script src="{{ asset('/').('public/asset/custom.js') }}"></script>

<script src="{{ asset('/').('public/js/moment.min.js') }}"></script>
<script src="{{ asset('/').('public/js/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('/').('public/js/jquery-jvectormap-world-merc-en.js') }}"></script>
<script src="{{ asset('/').('public/js/gdp-data.js') }}"></script>
<script src="{{ asset('/').('public/js/flot/jquery.flot.min.js') }}"></script>
<script src="{{ asset('/').('public/js/flot/jquery.flot.resize.min.js') }}"></script>
<script src="{{ asset('/').('public/js/flot/jquery.flot.time.min.js') }}"></script>
<script src="{{ asset('/').('public/js/flot/jquery.flot.threshold.js') }}"></script>
<script src="{{ asset('/').('public/js/flot/jquery.flot.axislabels.js') }}"></script>
<script src="{{ asset('/').('public/js/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('/').('public/js/skycons.js') }}"></script>


<script src="{{ asset('/').('public/js/pace.min.js') }}"></script>


<script>
  $(document).ready(function() {
    
      //CHARTS
      function gd(year, day, month) {
      return new Date(year, month - 1, day).getTime();
    }
    
    if ($('#graph-bar').length) {
      var data1 = [
          [gd(2015, 1, 1), 838], [gd(2015, 1, 2), 749], [gd(2015, 1, 3), 634], [gd(2015, 1, 4), 1080], [gd(2015, 1, 5), 850], [gd(2015, 1, 6), 465], [gd(2015, 1, 7), 453], [gd(2015, 1, 8), 646], [gd(2015, 1, 9), 738], [gd(2015, 1, 10), 899], [gd(2015, 1, 11), 830], [gd(2015, 1, 12), 789]
      ];
      
      var data2 = [
          [gd(2015, 1, 1), 342], [gd(2015, 1, 2), 721], [gd(2015, 1, 3), 493], [gd(2015, 1, 4), 403], [gd(2015, 1, 5), 657], [gd(2015, 1, 6), 782], [gd(2015, 1, 7), 609], [gd(2015, 1, 8), 543], [gd(2015, 1, 9), 599], [gd(2015, 1, 10), 359], [gd(2015, 1, 11), 783], [gd(2015, 1, 12), 680]
      ];
      
      var series = new Array();

      series.push({
        data: data1,
        bars: {
          show : true,
          barWidth: 24 * 60 * 60 * 12000,
          lineWidth: 1,
          fill: 1,
          align: 'center'
        },
        label: 'Revenues'
      });
      series.push({
        data: data2,
        color: '#e84e40',
        lines: {
          show : true,
          lineWidth: 3,
        },
        points: { 
          fillColor: "#e84e40", 
          fillColor: '#ffffff', 
          pointWidth: 1,
          show: true 
        },
        label: 'Orders'
      });

      $.plot("#graph-bar", series, {
        colors: ['#03a9f4', '#f1c40f', '#2ecc71', '#3498db', '#9b59b6', '#95a5a6'],
        grid: {
          tickColor: "#f2f2f2",
          borderWidth: 0,
          hoverable: true,
          clickable: true
        },
        legend: {
          noColumns: 1,
          labelBoxBorderColor: "#000000",
          position: "ne"       
        },
        shadowSize: 0,
        xaxis: {
          mode: "time",
          tickSize: [1, "month"],
          tickLength: 0,
          // axisLabel: "Date",
          axisLabelUseCanvas: true,
          axisLabelFontSizePixels: 12,
          axisLabelFontFamily: 'Open Sans, sans-serif',
          axisLabelPadding: 10
        }
      });

      var previousPoint = null;
      $("#graph-bar").bind("plothover", function (event, pos, item) {
        if (item) {
          if (previousPoint != item.dataIndex) {

            previousPoint = item.dataIndex;

            $("#flot-tooltip").remove();
            var x = item.datapoint[0],
            y = item.datapoint[1];

            showTooltip(item.pageX, item.pageY, item.series.label, y );
          }
        }
        else {
          $("#flot-tooltip").remove();
          previousPoint = [0,0,0];
        }
      });

      function showTooltip(x, y, label, data) {
        $('<div id="flot-tooltip">' + '<b>' + label + ': </b><i>' + data + '</i>' + '</div>').css({
          top: y + 5,
          left: x + 20
        }).appendTo("body").fadeIn(200);
      }
    }
      
    //WORLD MAP
    $('#world-map').vectorMap({
      map: 'world_merc_en',
      backgroundColor: '#ffffff',
      zoomOnScroll: false,
      regionStyle: {
        initial: {
          fill: '#e1e1e1',
          stroke: 'none',
          "stroke-width": 0,
          "stroke-opacity": 1
        },
        hover: {
          "fill-opacity": 0.8
        },
        selected: {
          fill: '#8dc859'
        },
        selectedHover: {
        }
      },
      markerStyle: {
        initial: {
          fill: '#e84e40',
          stroke: '#e84e40'
        }
      },
      markers: [
        {latLng: [38.35, -121.92], name: 'Los Angeles - 23'},
        {latLng: [39.36, -73.12], name: 'New York - 84'},
        {latLng: [50.49, -0.23], name: 'London - 43'},
        {latLng: [36.29, 138.54], name: 'Tokyo - 33'},
        {latLng: [37.02, 114.13], name: 'Beijing - 91'},
        {latLng: [-32.59, 150.21], name: 'Sydney - 22'},
      ],
      series: {
        regions: [{
          values: gdpData,
          scale: ['#6fc4fe', '#2980b9'],
          normalizeFunction: 'polynomial'
        }]
      },
      onRegionLabelShow: function(e, el, code){
        el.html(el.html()+' ('+gdpData[code]+')');
      }
    });

    /* SPARKLINE - graph in header */
    var orderValues = [10,8,5,7,4,4,3,8,0,7,10,6,5,4,3,6,8,9];

    $('.spark-orders').sparkline(orderValues, {
      type: 'bar', 
      barColor: '#ced9e2',
      height: 25,
      barWidth: 6
    });

    var revenuesValues = [8,3,2,6,4,9,1,10,8,2,5,8,6,9,3,4,2,3,7];

    $('.spark-revenues').sparkline(revenuesValues, {
      type: 'bar', 
      barColor: '#ced9e2',
      height: 25,
      barWidth: 6
    });

    /* ANIMATED WEATHER */
    var skycons = new Skycons({"color": "#03a9f4"});
    // on Android, a nasty hack is needed: {"resizeClear": true}

    // you can add a canvas by it's ID...
    skycons.add("current-weather", Skycons.SNOW);

    // start animation!
    skycons.play();

  });
  </script>



  <script>

    function statusChangeCallback(response) {
        console.log('statusChangeCallback');
        console.log(response);
        testAPI();
        return false;

         




    }

    function checkLoginState() {
        FB.getLoginStatus(function (response) {
            statusChangeCallback(response);
            
            //return false;
        });
    }

    window.fbAsyncInit = function () {
        FB.init({
            appId: '1487150151594094',
            cookie: true, // enable cookies to allow the server to access 
            // the session
            xfbml: true, // parse social plugins on this page
            version: 'v2.2' // use version 2.2
        });

        FB.getLoginStatus(function (response) {
            console.log(response);
            console.log(response.email);

            //here status is connected
            //statusChangeCallback(response);
        });

    };
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    function testAPI() {
        console.log('Welcome!  Fetching your information.... ');
         var url = '/me?fields=name,email,link';
        FB.api(url, function (response) {
          console.log(response);
          var imgUrl = 'http://graph.facebook.com/'+response.id+'/picture/?type=large';
          localStorage.setItem("fbImg", imgUrl);
          var fbId = response.id;
          var fbName = response.name;
          var fbEmail = response.email;

          

        $.ajax({
            url: 'checkFBLogin',
            type: 'POST',
            data: {facebookId: fbId, username : fbName, email : fbEmail},
            success: function (message)
            {
                console.log(message);        
                console.log('good');
                console.log(message.success);
                
                if (message.success == 1)
                {
                    location.reload();
                }
                if (message.success == 2)
                {
                    location.reload();
                }

                

            }

        });


            

            console.log('endcard');
            return false;




           
        });
    }
</script>
<script>
    function onSuccess(googleUser) {
console.log(googleUser);

googleId = googleUser.El;

googleUserName = googleUser.wc.wc;
googleEmail = googleUser.wc.hg;
gphoto = googleUser.wc.Ph;





$.ajax({
            url: 'checkGoogleLogin',
            type: 'POST',
            data: {googleID: googleId, googleUserName : googleUserName, googleEmail : googleEmail},
            success: function (message)
            {
                console.log(message);        
                console.log('good');
                console.log(message.success);
                
                if (message.success == 1)
                {
                    location.reload();
                }
                if (message.success == 2)
                {
                    location.reload();
                }

                

            }

        });

      //console.log('Logged in as: ' + googleUser.getBasicProfile().getName());
    }
    function onFailure(error) {
      console.log(error);
    }
    function renderButton() {
      gapi.signin2.render('my-signin2', {
        'scope': 'https://www.googleapis.com/auth/plus.login',
        'width': 200,
        'height': 50,
        'longtitle': true,
        'theme': 'dark',
        'onsuccess': onSuccess,
        'onfailure': onFailure
      });
    }
  </script>
  <script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>

</body>
</html>