<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Admin extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	protected $fillable = ['name', 'email', 'password'];

		public function setpasswordAttribute($value)
    {
        $this->attributes['password'] = \Hash::make($value);
    }

    public static $loginAdmin = array(
        'email' => array('required','email'), 
        'password' => 'required', 
			);

    public static $rulespwd = array('OldPassword' => 'required|pwdvalidation',
        'NewPassword' => 'required|confirmed|alphaNum|min:5|max:10',
        'NewPassword_confirmation' => 'required',
        );
    public static $userAdd	 = array(
        'username' => array('required'), 
        'email' => array('required'), 
        
        'mobile' => array('required'), 
        'phone' => array('required'), 
        
			);

    


}
