$(document).ready(function() {



$(document).click(function(e) {
     if (!(e.target.id === 'showLogin' || e.target.id === 'showRegister')) {
        console.log('clicked');
        
        //$(".dropdown-menu").hide();
     }
     else
     {
        console.log('clicked outide');
     }
});

$("#expandMenu").click(function(){
    console.log('try');
    $(".dropdown").toggleClass("open");
});


// $(document).click(function(e) {
//     if(!$(e.target).closest("#expandMenu").length) {
//         if($('.account-popup').is(':visible') && !$(e.target).closest('.account-popup').length) {
//            $('.account-popup').hide();
//            console.log('open');
//         }
//     }
// });

// $("#expandMenu").click(function(e) {
//    e.stopPropagation();
//    $(".account-popup").slideToggle();
// });


$('.myMenuLink').click(function () {

$("#addressCapChanger").html('Add a new address');
$('#newAddress')[0].reset();
$('#newAddress').trigger("reset");
$(".updateAddressItem").hide();
$(".updateAddress").show();

    console.log('super');
    var WH = $(window).height();  
  var SH = $('body')[0].scrollHeight;
  $('html, body').stop().animate({scrollTop: SH-WH}, 1000);
  
  console.log( SH+' '+WH ); // TEST


});

var url      = window.location.href;     // Returns full URL
console.log(url);

if ( url.indexOf("localhost") > -1 ) {
var mode ='local';
} else {
var mode ='live';
}


if(mode=='local')
{
localStorage.setItem("ghWebsite", 'http://localhost/greenhopping');        
var ghDomain = 'http://localhost/greenhopping';
$.cookie('ghDomain', ghDomain);    
}
else
{
localStorage.setItem("ghWebsite", 'http://52.91.246.152');        
var ghDomain = 'http://52.91.246.152';
$.cookie('ghDomain', ghDomain);
}





$(".editAddress").click(function(){
var WH = $(window).height();  
var SH = $('body')[0].scrollHeight;
$('html, body').stop().animate({scrollTop: SH-WH}, 1000);
});


$("#showLogin").click(function(){
    $("#registerArea").hide();
    $("#loginArea").show();
    //$("#showLogin").css({'background':'#2980b9'});
    //$("#showRegister").css({'background':'none'});
    $("#forgetArea").hide()    
    $("#outputArea").hide()    
    
});

$("#showRegister").click(function(){
    $("#loginArea").hide();
    $("#registerArea").show();
    //$("#showRegister").css({'background':'#2980b9'});
    $("#showRegister").css({'border-bottom-color':'rgb(67, 136, 44)'});
    //$("#showLogin").css({'border-bottom-color':'none'});
    $("#showLogin").css({'background':'none'});
    $("#forgetArea").hide();
    $("#outputArea").hide()    
});

$("#showForget").click(function(){
    $("#loginArea").hide();
    $("#forgetArea").show();
    $("#outputArea").hide();  
    $("#registerArea").hide();
     
});

//Profile Action

$("#showAddress").click(function(){
    $("#showAddressArea").show();
    $("#showOrdersArea").hide();
    $("#showCardsArea").hide();

    $(".myMenuLink").show();
    
});

$("#showCards").click(function(){
    $("#showAddressArea").hide();
    $("#showCardsArea").show();
    $("#showOrdersArea").hide();
    $(".myMenuLink").hide();
});

$("#showOrders").click(function(){
    $("#showAddressArea").hide();
    $("#showCardsArea").hide();
    $("#showOrdersArea").show();
    $(".myMenuLink").hide();
});

//Login, Register and Reset Action

$("#doLogin").click(function(event){

    if (!$("#loginForm").valid()) {
            return false;
        }
event.preventDefault();
    $.ajax({
    url: ghDomain+'/loginUser',
    type : 'POST',  
     data: {email : $("#loginEmail").val(), password : $("#loginPassword").val(), },
    success: function(message)
    {
    if(message.success==1)
        {
            
            // Create storage and redirect
            console.log('ok');
            console.log(message.userData);
            localStorage.setItem("ghUserId", message.userData.id);        
            localStorage.setItem("ghUserName", message.userData.username);
            localStorage.setItem("ghUserEmail", message.userData.email);
            window.location.href = ''; 
        }
        else if(message.success==2)
        {
            $('#errorAreaLogin').html('Account Not Activated');
        } 
        else if(message.success==3)
        {
            $('#errorAreaLogin').html('Invalid Password');
        } 
        else
        {
            $('#errorAreaLogin').html('Email Address Not Found');
        }
    }
    });    
});

$("#doRegister").click(function(event){
    event.preventDefault();

    if (!$("#registerForm").valid()) {
            return false;
        }
    $.ajax({
    url: 'registerUser',
    type : 'POST',  
     data: {email : $("#registerEmail").val(), password : $("#registerPassword").val() },
    success: function(message)
    {
    if(message.success==1)
        {
    $('#registerForm')[0].reset();
    $('#registerForm').trigger("reset");
    $("#registerArea").hide();
    $("#outputArea").html('<div class="alert alert-success"><i class="fa fa-check-circle fa-fw fa-lg"></i><strong>Successfully Registered !</strong> Please click on the Confirmation Link that is sent to your Mail.</div>');   
    $("#outputArea").show();   
        }
        else
        {

        $('#errorAreaRegister').html('The email has already taken');
        } 
    }
    });    
});

$("#doReset").click(function(){
    if (!$("#resetForm").valid()) {
            return false;
        }
    $.ajax({
    url: 'resetEmail',
    type : 'POST',  
     data: {email : $("#resetEmail").val()},
    success: function(message)
    {
    if(message.success==1)
        {
            // Create storage and redirect
    
    $("#resetEmailValidate").html('');
    $('#resetForm')[0].reset();
    $('#resetForm').trigger("reset");
    $("#forgetArea").hide();
    $("#outputArea").html('<div class="alert alert-success"><i class="fa fa-check-circle fa-fw fa-lg"></i><strong>Reset Link Sent Successfully to your Mail.</div>');   
    $("#outputArea").show();    
        }
        else
        {
            $('#resetEmailValidate').html('Email Not Found');
        } 
       
    }
    });    
});

    

$(".changePassword").click(function(){
    if (!$("#newPasswordForm").valid()) {
            return false;
        }
    $.ajax({
    url: 'updatePassword',
    type : 'POST',  
     data: {password : $("#password").val(), token : $("#token").val()},
    success: function(message)
    {
    if(message.success==1)
        {
            // Create storage and redirect
    
   
    $("#newPasswordForm").html('<div class="alert alert-success"><i class="fa fa-check-circle fa-fw fa-lg"></i><strong>Succesfully Updated Password.</div>');   
    
        }
        else
        {
            $("#newPasswordForm").html('<div class="alert alert-success"><i class="fa fa-check-circle fa-fw fa-lg"></i><strong>Server Error.</div>');   
        } 
       
    }
    });    
});

//Validation



$("#loginForm").validate({
        rules: {
            loginEmail: {
                minlength: 5,
                required: true,
                email: true
            },
            loginPassword: {
                required: true,
                minlength: 5,
                maxlength: 100
            }
        },
        errorPlacement: function (error, element) {
            console.log('er');
            var name = $(element).attr("name");
            error.appendTo($("#"+name+"Validate"));
            console.log($("#" + name + "Validate"));
        },
    });

$("#resetForm").validate({
        rules: {
            resetEmail: {
                minlength: 5,
                required: true,
                email: true
            }
        },
        errorPlacement: function (error, element) {
            console.log('er');
            var name = $(element).attr("name");
            error.appendTo($("#"+name+"Validate"));
            console.log($("#" + name + "Validate"));
        },
    });

$("#newPasswordForm").validate({
        rules: {
            password: {
                minlength: 5,
                maxlength: 30,
                required: true,
                
            },
            password_confirm: {
                minlength: 5,
                maxlength: 30,
                required: true,
                equalTo : "#password"
            }
        },
        errorPlacement: function (error, element) {
            console.log('er');
            var name = $(element).attr("name");
            error.appendTo($("#"+name+"Validate"));
            console.log($("#" + name + "Validate"));
        },
    });

$("#registerForm").validate({
        rules: {
            registerEmail: {
                minlength: 5,
                required: true,
                email: true
            },
            registerPassword: {
                required: true,
                minlength: 5,
                maxlength: 100
            },
            registerPasswordConfirmation: {
                required: true,
                minlength: 5,
                equalTo : "#registerPassword"
            }
        },
        errorPlacement: function (error, element) {
            console.log('er');
            var name = $(element).attr("name");
            error.appendTo($("#"+name+"Validate"));
            console.log($("#" + name + "Validate"));
        },
    });

    if (localStorage.getItem("ghUserId") === null) 
    {

    }
    else
    {
        var ghUserName = localStorage.getItem("ghUserName");
        //var ghUserEmail = localStorage.getItem("ghUserEmail");
        
        if(ghUserName=='')
        {
            console.log('first');
            $('.ghUserName').html(ghUserEmail);
            $('#ghUserName').html(ghUserEmail);
            $('#ghUserNameProfile').html(ghUserEmail);

        }
        else
        {
            $('.ghUserName').html(ghUserName);
            $('#ghUserName').html(ghUserName);
            $('#ghUserName').val(ghUserName);
            $('#ghUserNameProfile').html(ghUserName);
            //$('#fullNames').val(ghUserName);
            
        }   
        //$('#ghUserEmail').val(ghUserEmail);
    }

    
    
    

    $(".updateAddresss").click(function(){

event.preventDefault();
        if (!$("#newAddress").valid()) {
            
            console.log('11');
            return false;
        }
    
var id = $("#userId").val();
var fullName = $("#fullName").val();
var address = $("#addressVal").val();
var city = $("#city").val();
var state = $("#state").val();
var pincode = $("#pincode").val();
var country = $("#country").val();



$('#newAddress')[0].reset();
$('html, body').stop().animate({scrollTop: 0}, 1000);
$("#profileUpdatedSuccessEdit").hide();         
$("#profileUpdatedSuccessEdit").html('<div class="alert alert-success fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><i class="fa fa-check-circle fa-fw fa-lg"></i>Address Added Successfully</div>');        
$("#profileUpdatedSuccessEdit").show();
setTimeout(function(){ $("#profileUpdatedSuccessEdit").html('');  $("#profileUpdatedSuccessEdit").hide();  }, 2000);



    $.ajax({
    url: 'updateAddress',
    type : 'POST',  
     data: {id : id, fullName : fullName, address : address, city : city, state : state, pincode : pincode, country : country },
    success: function(message)
    {
    console.log(message);
    console.log(message.success);
    if(message.success==1)
        {
    

        $.ajax({
    url: "getProfileData/"+id,
    type : "GET",  
     data: {  },
    success: function(message)
    {

    
    
    if(message.success==1)
        {
   
   
        $("#userAddressData").html(message.address);       
        $(".updateAddressItem").hide();
        $(".updateAddress").show();
        
        
    $("#availableCity").html(message.city);        


        }
        else
        {

$("#availableCity").html(message.city);        
        }
        
    }
    });

    $("#profileUpdateProfile").html('Profile Updated Successfully');        

        }
        else
        {
            $('#profileUpdateProfile').html('Error in Update');
        }
    }
    });
});


$(".updateAddressItem").click(function(){
event.preventDefault();
        if (!$("#newAddress").valid()) {            
            console.log('11');
            return false;
        }

var addr_id = $("#addr_id").val();
var id = localStorage.getItem("ghUserId")  
var fullName = $("#fullName").val();
var address = $("#addressVal").val();
var city = $("#city").val();
var state = $("#state").val();
var pincode = $("#pincode").val();
var country = $("#country").val();
    $.ajax({
    url: 'updateAddressEdit',
    type : 'POST',  
     data: {addr_id : addr_id, id : id, fullName : fullName, address : address, city : city, state : state, pincode : pincode, country : country },
    success: function(message)
    {

$("#addressCapChanger").html('Add a New Address');
$(".updateAddressItem").html('Add a New Address');
$("#addressCapChanger").html('Add a New Address');


    console.log(message);
    console.log(message.success);
    if(message.success==1)
        {
    

        $.ajax({
    url: "getProfileData/"+id,
    type : "GET",  
     data: {  },
    success: function(message)
    {

    
    
    if(message.success==1)
        {
   

var WH = $(window).height();  
  
  $('html, body').stop().animate({scrollTop: 0}, 1000);

        $('#newAddress')[0].reset();
        $("#userAddressData").html(message.address);       
        $("#profileUpdatedSuccessEdit").hide();         
$("#profileUpdatedSuccessEdit").html('<div class="alert alert-success fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><i class="fa fa-check-circle fa-fw fa-lg"></i>Address Updated Successfully</div>');        
$("#profileUpdatedSuccessEdit").show();
setTimeout(function(){ $("#profileUpdatedSuccessEdit").html('');  $("#profileUpdatedSuccessEdit").hide();  }, 2000);

        
    $("#availableCity").html(message.city);        

        }
        else
        {

$("#availableCity").html(message.city);        
        }
        
    }
    });

    $("#profileUpdateProfile").html('Profile Updated Successfully');        

        }
        else
        {
            $('#profileUpdateProfile').html('Error in Update');
        }
    }
    });
});










   $("#updateProfileData").click(function(){
console.log('vali');
if (!$("#newAddressNew").valid()) {
            return false;
        }

var name = $('#fullNames').val();




var id = localStorage.getItem("ghUserId")  
//var name = $("#fullNames").val();

var location = $( "#userLocation option:selected" ).attr("id");

    $.ajax({
    url: 'updateProfile',
    type : 'POST',  
     data: {id : id, name : name, location : location },
    success: function(message)
    {
    console.log(message.success)
    if(message.success==1)
        {
            localStorage.setItem("ghUserName", name);   
            $('#userNameNew').html(name);
            
            $("#profileUpdatedSuccess").hide();         
            $("#profileUpdatedSuccess").html('<div class="alert alert-success fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><i class="fa fa-check-circle fa-fw fa-lg"></i>Profile Updated Successfully</div>');        
            
            $(".ghUserName").html(name);    
            $("#ghUserNameProfile").html(name);
            //$("#profileUpdatedSuccess").slideToggle();
            $("#profileUpdatedSuccess").show();
            setTimeout(function(){ $("#profileUpdatedSuccess").html('');  $("#profileUpdatedSuccess").hide();  }, 1500);

        }
        else
        {
            $("#profileUpdatedSuccess").html('<div class="alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><i class="fa fa-times-circle fa-fw fa-lg"></i>Error in Profile Update</div>');        
        }
        
    }
    });



});


$(".DefaultupdateAddress").click(function(){


if (!$("#newAddress").valid()) {
            return false;
        }

var id = localStorage.getItem("ghUserId")  
var fullName = $("#fullName").val();
var address = $("#addressVal").val();
var city = $("#city").val();
var state = $("#state").val();
var pincode = $("#pincode").val();
var country = $("#country").val();
var defaults = 'yes';
    $.ajax({
    url: 'updateAddress',
    type : 'POST',  
     data: {id : id, fullName : fullName, address : address, city : city, state : state, pincode : pincode, country : country, defaults : defaults },
    success: function(message)
    {
    console.log(message);
    console.log(message.success);
    if(message.success==1)
        {
    

        $.ajax({
    url: "getProfileData/"+id,
    type : "GET",  
     data: {  },
    success: function(message)
    {

    
    
    if(message.success==1)
        {
        $('#newAddress')[0].reset();
        $("#userAddressData").html(message.address);       
        
        
    $("#availableCity").html(message.city);        

        }
        else
        {

$("#availableCity").html(message.city);        
        }
        
    }
    });

    $("#profileUpdateProfile").html('Profile Updated Successfully');        

        }
        else
        {
            $('#profileUpdateProfile').html('Error in Update');
        }
    }
    });
});

jQuery.validator.addMethod("lettersonly", function(value, element) {
  return this.optional(element) || /^[a-z' ]+$/i.test(value);
}, "Letters only please"); 




$("#newAddress").validate({
        rules: {
            fullName: {
                minlength: 2,
                maxlength: 30,
                required: true,
                lettersonly: true
                
            },
            addressVal: {
                required: true,
                minlength: 5,
                maxlength: 100
            },
            city: {
                required: true,
                minlength: 2,
                maxlength: 100
            },
            state: {
                required: true,
                minlength: 2,
                maxlength: 100
            },
            pincode: {
                required: true,
                minlength: 3,
                maxlength: 100
            },
            country: {
                required: true,
                minlength: 2,
                maxlength: 100
            }
        },
        errorPlacement: function (error, element) {
            console.log('er');
            var name = $(element).attr("name");
            error.appendTo($("#"+name+"Validate"));
            console.log($("#" + name + "Validate"));
        },
    });


$("#newAddressNew").validate({
        rules: {
            fullNames: {
                minlength: 2,
                maxlength: 30,
                required: true,
                lettersonly: true
                
            }        },
        errorPlacement: function (error, element) {
            console.log('er');
            var name = $(element).attr("name");
            error.appendTo($("#"+name+"Validate"));
            console.log($("#" + name + "Validate"));
        },
    });


    



});