$(document).ready(function() {

//$('#mobile').mask('(+1) 999-999-9999');
//$('#phone').mask('(+1) 99999999999');
$('#price').mask('99.99');
$('.ui-dialog-titlebar-close').html('<sup>x</sup>');

var url      = window.location.href;     // Returns full URL
console.log(url);

if ( url.indexOf("localhost") > -1 ) {
var mode ='local';
} else {
var mode ='live';
}



//var mode ='local';
if(mode=='local')
{
	var ghDomain = 'http://localhost/greenhopping';
}
else
{
	var ghDomain = 'http://52.91.246.152';
}
$.cookie('ghDomain', ghDomain);

$('.btn-danger').html('Clear');


$(".btn-danger").click(function(event){
    $('#addStore').find("input[type=text],input[type=email], textarea").val("");
    console.log("clear");
    event.preventDefault();
});

//Checking Authentication of Admin

/**
            $.ajax({
                method: "POST",
                url: "admin/authCheck",
                data: {
                    '_token': _token,
                    'ghUser': $("#email").val(),
                }
            })
            .done(function(msg) {
                if(msg.success==1)
                {
                    window.location.href = ghDomain+'/admin/dashboard';
                }
                else
                {
                    if(msg.success==2)
                    {
                        $('#adminLogin-password').html(msg.message);
                    }
                    else
                    {
                        $('#adminRegisterFormMessage').html(msg.message);
                    }
                    
                }
            });
**/
//Initialize
var _token = $("input[name=_token]").val();

$("#adminLogin").click(function(event){
    loginAdmin();
});


//Triggers

$("#createStore").click(function(event){
    createStore();
});




// Functions

function loginAdmin() {
    if (!$("#adminLoginForm").valid()) {
        event.preventDefault();
            return false;
        }
        else
        {
            ("#adminLoginForm").submit();
        }
        return false;
}

// Create Store
function createStore(){
    ("#addStore").submit();
    return false
          if (!$("#addStore").valid()) {
        event.preventDefault();
            return false;
        }
        else
        {
            ("#addStore").submit();
        }
        return false;
    }

// Validations
    //Admin Login
    $("#adminLoginForm").validate({
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#adminLogin-"+name));
        },
        rules: {
            email: {
                minlength: 5,
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 5,
                maxlength: 100
            }
        }
    });
    //Add Store
    $("#addStores").validate({
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            console.log(name);
            error.appendTo($("#addStore-"+name));
        },
        rules: {
            name: {
                minlength: 2,
                maxlength: 25,
                required: true,
            },
            email: {
                minlength: 6,
                maxlength: 35,
                required: true,
                
            },
            address: {
                minlength: 6,
                maxlength: 35,
                required: true,
            },
            pincode: {
                minlength: 4,
                maxlength: 10,
                number: true,
                required: true,
            }
        }
    });




});