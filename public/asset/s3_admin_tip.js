$(document).ready(function() {

var ghDomain = $.cookie('ghDomain')

    var btnUpload=jQuery('#tip_pic');
    var status=jQuery('#statuss');
    new AjaxUpload(btnUpload, {
      action: ghDomain+'/s3/s3_admin_tip.php',
      name: 'uploadfile',
      onSubmit: function(file, ext){
         if (! (ext && /^(jpg|jpeg|gif|png)$/.test(ext))){ 
                    // extension is not allowed 
          status.text('Only JPG or GIF or PNG files are allowed');
          return false;
        }
        status.text('Uploading...');
      },
      onComplete: function(file, response){
        //On completion clear the status
        status.text('');
        //split the response
        var resp = response.split('-');
        //Add uploaded file to list
        if(resp[0] == 1){
          //store image
          var img = resp[1];
          var apic = jQuery('#cover_image').val();
          //uploaded images are stored in hidden text box for future reference
          jQuery('#tip_image').val(img+','+apic);
          jQuery('#tip_cover').html('<img src="'+img+'" height="50" width="50" class="img-responsive">');
          $('#tip_image').val(img);
        } else {
          jQuery('#tip_cover').text(resp[1]).addClass('error');
        }
      }
    });

});