$(document).ready(function() {

getLocation();

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else { 

    }
}

function showPosition(position) {
$.cookie('latitude', position.coords.latitude);    
$.cookie('longitude', position.coords.longitude);    


}


var url      = window.location.href;     // Returns full URL
console.log(url);

if ( url.indexOf("localhost") > -1 ) {
var mode ='local';
} else {
var mode ='live';
}


if(mode=='local')
{
var publicUrl = 'http://localhost:8080/greenhopping';
localStorage.setItem("ghWebsite", publicUrl);        
var ghDomain = publicUrl;
$.cookie('ghDomain', ghDomain);    
}
else
{
var publicUrl = 'http://52.91.246.152';
localStorage.setItem("ghWebsite", publicUrl);        
var ghDomain = publicUrl;
$.cookie('ghDomain', ghDomain);
}


$("#showPopup").click(function(){
	$(".err").html('');
    console.log('try');
    $("#showLogin").css({'border-bottom-color':'green'});
    $("#showRegister").css({'border-bottom-color':'#EFEFEF'});
    $("#popupArea").toggleClass("open");
});
$("#openSettings").click(function(){
	$(".err").html('');
    console.log('open settings');
    $("#settingsMenu").toggleClass("open");
});

$(document).on("click","#myloginbutton",function(){
	$("#myModal").modal({
		show:true,
	});
});

$("#likeStore").click(function(){
    console.log('likeStore')

     $.ajax({
    url: '../likeStore',
    type : 'POST',  
     data: {user_id :  $("#user_id").val() , store_id : $("#store_id").val()},
    success: function(message)
    {
    if(message.success==1)
        {
           $("#likeStore").attr("src","../public/static/like.png"); 
           $("#totalLike").html(message.totalLike);
        }
        else
        {
            $("#likeStore").attr("src","../public/static/unlike.png");    
            $("#totalLike").html(message.totalLike);
        } 

       
    }
    });    


});




$("#showLogin").click(function(){
    console.log("showLogin");
	$(".err").html('');
    $("#registerArea").hide();
    $("#loginArea").show();
    //$("#showLogin").css({'border-bottom-color':'#2px solid #43882C'});
    //$("#showRegister").css({'border-bottom-color':'red'});
    $("#showLogin").css({'border-bottom-color':'green'});
    $("#showRegister").css({'border-bottom-color':'#EFEFEF'});
    //$("#showRegister").css({'background':'none'});
    $("#forgetArea").hide()    
    $("#outputArea").hide()    
    
});



$("#showRegister").click(function(){
    console.log("showRegister");
	$(".err").html('');
    $("#loginArea").hide();
    $("#registerArea").show();
    $("#showRegister").css({'border-bottom-color':'green'});
    $("#showLogin").css({'border-bottom-color':'#EFEFEF'});
    //$("#showRegister").css({'background':'#2980b9'});
    //$("#showRegister").css({'border-bottom-color':'rgb(67, 136, 44)'});
    //$("#showLogin").css({'border-bottom-color':'red'});
    
    $("#forgetArea").hide();
    $("#outputArea").hide()    
});

$("#showForget").click(function(){
	$(".err").html('');
    $("#loginArea").hide();
    $("#forgetArea").show();
    $("#outputArea").hide();  
    $("#registerArea").hide();
     
});

//Profile Action

$("#showAddress").click(function(){
    $("#showAddressArea").show();
    $("#showOrdersArea").hide();
    $("#showCardsArea").hide();

    $(".myMenuLink").show();
    
});

$("#showCards").click(function(){
    $("#showAddressArea").hide();
    $("#showCardsArea").show();
    $("#showOrdersArea").hide();
    $(".myMenuLink").hide();
});

$("#showOrders").click(function(){
    $("#showAddressArea").hide();
    $("#showCardsArea").hide();
    $("#showOrdersArea").show();
    $(".myMenuLink").hide();
});

$("#changePassword").click(function(){
if (!$("#changePasswordForm").valid()) {
            return false;
        }
event.preventDefault();

$.ajax({
    url: ghDomain+'/changepasswordprocess',
    type : 'POST',  
     data: {OldPassword : $("#OldPassword").val(), NewPassword : $("#NewPassword").val(), NewPassword_confirmation : $("#NewPassword_confirmation").val()  },
    success: function(message)
    {
        $(".error").html('');
    if(message.success==0)
    {
    $.each(message.error, function(k, v) {
    $("#"+k+"Validate").html(v);
    console.log(k)
    });
    }
    else
    {
        $('#changePasswordForm')[0].reset();
        $('#changePasswordForm').trigger("reset");

        

        $("#profileUpdatedSuccess").hide();         
            $("#profileUpdatedSuccess").html('<div class="alert alert-success fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><i class="fa fa-check-circle fa-fw fa-lg"></i>Password Updated Successfully</div>');        
            
            //$("#profileUpdatedSuccess").slideToggle();
            $("#profileUpdatedSuccess").show();
            setTimeout(function(){ $("#profileUpdatedSuccess").html('');  $("#profileUpdatedSuccess").hide();  }, 1500);





    }

    

    return false;

    
    }
    
});
});

$("#changePasswordForm").validate({
        rules: {
            password: {
                minlength: 5,
                maxlength: 15,
                required: true,
            },
            new_password: {
                minlength: 5,
                maxlength: 15,
                required: true,
            },
            confirm_password: {
                minlength: 5,
                maxlength: 15,
                required: true,
            }
        },
        errorPlacement: function (error, element) {
            console.log('er');
            var name = $(element).attr("name");
            error.appendTo($("#"+name+"Validate"));
            console.log($("#" + name + "Validate"));
        },
    });




$("#doLogin").click(function(event){

    if (!$("#loginForm").valid()) {
            return false;
        }
event.preventDefault();
    $.ajax({
    url: ghDomain+'/loginUser',
    type : 'POST',  
     data: {email : $("#loginEmail").val(), password : $("#loginPassword").val(), },
    success: function(message)
    {
    if(message.success==1)
        {
            
            // Create storage and redirect
            console.log('ok');
            console.log(message.userData);
            localStorage.setItem("ghUserId", message.userData.id);        
            localStorage.setItem("ghUserName", message.userData.username);
            localStorage.setItem("ghUserEmail", message.userData.email);
            
            window.location.href = ghDomain; 
        }
        else if(message.success==2)
        {
            $('#errorAreaLogin').html('Account Not Activated');
        } 
        else if(message.success==3)
        {
            $('#errorAreaLogin').html('Invalid Password');
        } 
        else if(message.success==0)
        {
            $('#errorAreaLogin').html(message.error.email);
        } 
        else if(message.success==4)
        {
            $('#errorAreaLogin').html('User Not Found');
        } 
        else
        {
            $('#errorAreaLogin').html('Email Address Not Found');
        }
    }
    });    
});

$("#doRegister").click(function(event){
    event.preventDefault();

    if (!$("#registerForm").valid()) {
            return false;
        }
    $.ajax({
    url: 'registerUser',
    type : 'POST',  
     data: {username : $("#registerUserName").val(), email : $("#registerEmail").val(), password : $("#registerPassword").val() },
    success: function(message)
    {
    if(message.success==1)
        {
    $('#registerForm')[0].reset();
    $('#registerForm').trigger("reset");
    $("#registerArea").hide();
    $("#showSocial").hide();
    $("#msgArea").html('<div class="alert alert-success"><i class="fa fa-check-circle fa-fw fa-lg"></i><strong>Successfully Registered !</strong> Please click on the Confirmation Link that is sent to your Mail.</div>');   
    $("#msgArea").show();   
        }
        else
        {

        $('#errorAreaRegister').html('The email has already taken');
        } 
    }
    });    
});

$("#doReset").click(function(){
    event.preventDefault();

    if (!$("#resetForm").valid()) {
            return false;
        }
    $.ajax({
    url: 'resetEmail',
    type : 'POST',  
     data: {email : $("#resetEmail").val()},
    success: function(message)
    {
    if(message.success==1)
        {
            // Create storage and redirect
    
    $("#resetEmailValidate").html('');
    $('#resetForm')[0].reset();
    $('#resetForm').trigger("reset");
    $("#forgetArea").hide();
    $("#outputArea").html('<div class="alert alert-success"><i class="fa fa-check-circle fa-fw fa-lg"></i><strong>Reset Link Sent Successfully to your Mail.</div>');   
    $("#outputArea").show();    
        }
        else
        {
            $('#resetEmailValidate').html('Email Not Found');
        } 
       
    }
    });    
});

    

$(".changePassword").click(function(){
    if (!$("#newPasswordForm").valid()) {
            return false;
        }
    $.ajax({
    url: 'updatePassword',
    type : 'POST',  
     data: {password : $("#password").val(), token : $("#token").val()},
    success: function(message)
    {
    if(message.success==1)
        {
            // Create storage and redirect
    
   
    $("#newPasswordForm").html('<div class="alert alert-success"><i class="fa fa-check-circle fa-fw fa-lg"></i><strong>Succesfully Updated Password.</div>');   
    
        }
        else
        {
            $("#newPasswordForm").html('<div class="alert alert-success"><i class="fa fa-check-circle fa-fw fa-lg"></i><strong>Server Error.</div>');   
        } 
       
    }
    });    
});

//Validation



$("#loginForm").validate({
        rules: {
            loginEmail: {
                minlength: 5,
                required: true,
                email: true
            },
            loginPassword: {
                required: true,
                minlength: 5,
                maxlength: 100
            }
        },
        errorPlacement: function (error, element) {
            console.log('er');
            var name = $(element).attr("name");
            error.appendTo($("#"+name+"Validate"));
            console.log($("#" + name + "Validate"));
        },
    });

$("#resetForm").validate({
        rules: {
            resetEmail: {
                minlength: 5,
                required: true,
                email: true
            }
        },
        errorPlacement: function (error, element) {
            console.log('er');
            var name = $(element).attr("name");
            error.appendTo($("#"+name+"Validate"));
            console.log($("#" + name + "Validate"));
        },
    });

$("#newPasswordForm").validate({
        rules: {
            password: {
                minlength: 5,
                maxlength: 30,
                required: true,
                
            },
            password_confirm: {
                minlength: 5,
                maxlength: 30,
                required: true,
                equalTo : "#password"
            }
        },
        errorPlacement: function (error, element) {
            console.log('er');
            var name = $(element).attr("name");
            error.appendTo($("#"+name+"Validate"));
            console.log($("#" + name + "Validate"));
        },
    });

$("#registerForm").validate({
        rules: {
            registerUserName: {
                minlength: 5,
                maxlength: 20,
                required: true,
            },
            registerEmail: {
                minlength: 5,
                required: true,
                email: true
            },
            registerPassword: {
                required: true,
                minlength: 5,
                maxlength: 100
            },
            registerPasswordConfirmation: {
                required: true,
                minlength: 5,
                equalTo : "#registerPassword"
            }
        },
        errorPlacement: function (error, element) {
            console.log('er');
            var name = $(element).attr("name");
            error.appendTo($("#"+name+"Validate"));
            console.log($("#" + name + "Validate"));
        },
    });

    if (localStorage.getItem("ghUserId") === null) 
    {

    }
    else
    {
        var ghUserName = localStorage.getItem("ghUserName");
        //var ghUserEmail = localStorage.getItem("ghUserEmail");
        
        if(ghUserName=='')
        {
            
            
            
            

        }
        else
        {
            $('.ghUserName').html(ghUserName);
            $('#ghUserName').html(ghUserName);
            $('#ghUserName').val(ghUserName);
            $('#ghUserNameProfile').html(ghUserName);
            //$('#fullNames').val(ghUserName);
            
        }   
        //$('#ghUserEmail').val(ghUserEmail);
    }

    
    


    $(".updateAddress").click(function(){
        
        if (!$("#newAddress").valid()) {
            
            console.log('11');
            return false;
        }
        


var id = $("#userId").val();
var fullName = $("#fullName").val();
var address = $("#addressVal").val();
var city = $("#city").val();
var state = $("#state").val();
var pincode = $("#pincode").val();
var country = $("#country").val();


$('#newAddress')[0].reset();
$('html, body').stop().animate({scrollTop: 0}, 1000);
//$("#profileUpdatedSuccessEdit").hide();         
//$("#profileUpdatedSuccessEdit").html('<div class="alert alert-success fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><i class="fa fa-check-circle fa-fw fa-lg"></i>Address Added Successfully</div>');        
//$("#profileUpdatedSuccessEdit").show();
//setTimeout(function(){ $("#profileUpdatedSuccessEdit").html('');  $("#profileUpdatedSuccessEdit").hide();  }, 2000);



    $.ajax({
    url: 'updateAddress',
    type : 'POST',  
     data: {id : id, fullName : fullName, address : address, city : city, state : state, pincode : pincode, country : country },
    success: function(message)
    {
    console.log(message);
    console.log(message.success);
    if(message.success==1)
        {
    

        $.ajax({
    url: "getProfileData/"+id,
    type : "GET",  
     data: {  },
    success: function(message)
    {

    
    
    if(message.success==1)
        {
   
   
        $("#userAddressData").html(message.address);       
        $(".updateAddressItem").hide();
        $(".updateAddress").show();
        
        
    $("#availableCity").html(message.city);        


        }
        else
        {

$("#availableCity").html(message.city);        
        }
        
    }
    });

    $("#profileUpdateProfile").html('Profile Updated Successfully');        

        }
        else
        {
            $('#profileUpdateProfile').html('Error in Update');
        }
    }
    });
});


$(".updateAddressItem").click(function(){
        
        if (!$("#newAddress").valid()) {
            
            console.log('11');
            return false;
        }
        



var addr_id = $("#addr_id").val();
var id = $("#userId").val();
var fullName = $("#fullName").val();
var address = $("#addressVal").val();
var city = $("#city").val();
var state = $("#state").val();
var pincode = $("#pincode").val();
var country = $("#country").val();
    $.ajax({
    url: 'updateAddressEdit',
    type : 'POST',  
     data: {addr_id : addr_id, id : id, fullName : fullName, address : address, city : city, state : state, pincode : pincode, country : country },
    success: function(message)
    {

$("#addressCapChanger").html('Add a New Address');
$(".updateAddressItem").html('Add a New Address');
$("#addressCapChanger").html('Add a New Address');


    console.log(message);
    console.log(message.success);
    if(message.success==1)
        {
    

        $.ajax({
    url: "getProfileData/"+id,
    type : "GET",  
     data: {  },
    success: function(message)
    {

    
    
    if(message.success==1)
        {
   

var WH = $(window).height();  
  
  $('html, body').stop().animate({scrollTop: 0}, 1000);

        $('#newAddress')[0].reset();
        $("#userAddressData").html(message.address);       
        $("#profileUpdatedSuccessEdit").hide();         
$("#profileUpdatedSuccessEdit").html('<div class="alert alert-success fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><i class="fa fa-check-circle fa-fw fa-lg"></i>Address Edited Successfully</div>');        
$("#profileUpdatedSuccessEdit").show();
setTimeout(function(){ $("#profileUpdatedSuccessEdit").html('');  $("#profileUpdatedSuccessEdit").hide();  }, 2000);

        
    $("#availableCity").html(message.city);        

        }
        else
        {

$("#availableCity").html(message.city);        
        }
        
    }
    });

    $("#profileUpdateProfile").html('Profile Updated Successfully');        

        }
        else
        {
            $('#profileUpdateProfile').html('Error in Update');
        }
    }
    });
});










   $("#updateProfileData").click(function(){
console.log('vali');
if (!$("#newAddressNew").valid()) {
            return false;
        }

var name = $('#fullNames').val();




var id = $("#userId").val();
//var name = $("#fullNames").val();

var location = $( "#userLocation option:selected" ).attr("id");

    $.ajax({
    url: 'updateProfile',
    type : 'POST',  
     data: {id : id, name : name, location : location },
    success: function(message)
    {
    console.log(message.success)
    if(message.success==1)
        {
            localStorage.setItem("ghUserName", name);   
            var s = name.slice(0, 10);
            console.log(s);
            $(".profileName").html(name);
            $('#userNameNew').html(s+'...');
            
            $("#profileUpdatedSuccess").hide();         
            $("#profileUpdatedSuccess").html('<div class="alert alert-success fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><i class="fa fa-check-circle fa-fw fa-lg"></i>Profile Updated Successfully</div>');        
            
            $(".ghUserName").html(name);    
            $("#ghUserNameProfile").html(name);
            //$("#profileUpdatedSuccess").slideToggle();
            $("#profileUpdatedSuccess").show();
            setTimeout(function(){ $("#profileUpdatedSuccess").html('');  $("#profileUpdatedSuccess").hide();  }, 1500);

        }
        else
        {
            $("#profileUpdatedSuccess").html('<div class="alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><i class="fa fa-times-circle fa-fw fa-lg"></i>Error in Profile Update</div>');        
        }
        
    }
    });



});


$(".DefaultupdateAddress").click(function(){


if (!$("#newAddress").valid()) {
            return false;
        }

var id = $("#userId").val();
var fullName = $("#fullName").val();
var address = $("#addressVal").val();
var city = $("#city").val();
var state = $("#state").val();
var pincode = $("#pincode").val();
var country = $("#country").val();
var defaults = 'yes';
    $.ajax({
    url: 'updateAddress',
    type : 'POST',  
     data: {id : id, fullName : fullName, address : address, city : city, state : state, pincode : pincode, country : country, defaults : defaults },
    success: function(message)
    {
    console.log(message);
    console.log(message.success);
    if(message.success==1)
        {
    

        $.ajax({
    url: "getProfileData/"+id,
    type : "GET",  
     data: {  },
    success: function(message)
    {

    
    
    if(message.success==1)
        {
            var WH = $(window).height();  
  
  $('html, body').stop().animate({scrollTop: 0}, 1000);
  
        $('#newAddress')[0].reset();
        $("#userAddressData").html(message.address);       
        
        
    $("#availableCity").html(message.city);        

        }
        else
        {

$("#availableCity").html(message.city);        
        }
        
    }
    });

    $("#profileUpdateProfile").html('Profile Updated Successfully');        

        }
        else
        {
            $('#profileUpdateProfile').html('Error in Update');
        }
    }
    });
});

jQuery.validator.addMethod("lettersonly", function(value, element) {
  return this.optional(element) || /^[a-z' ]+$/i.test(value);
}, "Letters only please"); 




$("#newAddress").validate({
        rules: {
            fullName: {
                minlength: 2,
                maxlength: 30,
                required: true,
                lettersonly: true
                
            },
            addressVal: {
                required: true,
                minlength: 5,
                maxlength: 100
            },
            city: {
                required: true,
                minlength: 2,
                maxlength: 100
            },
            state: {
                required: true,
                minlength: 2,
                maxlength: 100
            },
            pincode: {
                required: true,
                minlength: 3,
                maxlength: 100
            },
            country: {
                required: true,
                minlength: 2,
                maxlength: 100
            }
        },
        errorPlacement: function (error, element) {
            console.log('er');
            var name = $(element).attr("name");
            error.appendTo($("#"+name+"Validate"));
            console.log($("#" + name + "Validate"));
        },
    });


$("#newAddressNew").validate({
        rules: {
            fullNames: {
                minlength: 2,
                maxlength: 30,
                required: true,
                lettersonly: true
                
            }        },
        errorPlacement: function (error, element) {
            console.log('er');
            var name = $(element).attr("name");
            error.appendTo($("#"+name+"Validate"));
            console.log($("#" + name + "Validate"));
        },
    });


    



});